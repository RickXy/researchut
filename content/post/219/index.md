{
    "title": "Gitolite and Gitweb",
    "date": "2015-05-13T04:29:27-04:00",
    "lastmod": "2015-05-13T04:29:27-04:00",
    "draft": "false",
    "tags": [
        "git",
        "gitolite3",
        "gitweb"
    ],
    "categories": [
        "Debian-Blog"
    ]
}

This article is for self, so that I don't again forget the specifics. The last
time I did the same setup, it wasn't very important in terms of security.
gitolite(3) + gitweb can give an impressive git tool with very simple user
acls. After you setup gitolite, ensure that the umask value in gitolite is
approriate, i.e. the gitolite group has r-x privilege. This is needed for the
web view. Add your apache user to the gitolite group. With the umask changes,
and the group association, apache's user will now be able to read gitolite
repos.  
  
Now, imagine a repo setting like the following:

    
    
    repo virtualbox
        RW+     =   admin
        R   =   gitweb

This allows 'R'ead for gitweb. But by Unix ACLs, now even www-data will have
'RX' on all (the ones created after the UMASK) the repositories.

    
    
    rrs@chutzpah:~$ sudo ls -l /var/lib/gitolite3/repositories/
    [sudo] password for rrs:
    total 20
    drwxr-x--- 7 gitolite3 gitolite3 4096 May 12 17:13 foo.git
    drwx------ 8 gitolite3 gitolite3 4096 May 13 12:06 gitolite-admin.git
    drwxr-x--- 7 gitolite3 gitolite3 4096 May 13 12:06 linux.git
    drwx------ 7 gitolite3 gitolite3 4096 May 12 16:38 testing.git
    drwxr-x--- 7 gitolite3 gitolite3 4096 May 12 17:20 virtualbox.git
    13:10 ♒♒♒   ☺    
    

But just www-data. No other users. Because for 'O', there is no 'rwx'. And
below shows gitolite's ACL in picture...

    
    
    test@chutzpah:~$ git clone gitolite3@chutzpah:virtualbox
    Cloning into 'virtualbox'...
    Enter passphrase for key '/home/test/.ssh/id_rsa':
    FATAL: R any virtualbox test DENIED by fallthru
    (or you mis-spelled the reponame)
    fatal: Could not read from remote repository.
    
    Please make sure you have the correct access rights
    and the repository exists.

