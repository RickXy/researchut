{
    "title": "Micro DD meetup",
    "date": "2015-07-20T07:26:35-04:00",
    "lastmod": "2015-07-20T07:26:35-04:00",
    "draft": "false",
    "tags": [
        "linux containers",
        "lxc",
        "systemd",
        "systemd-nspawn",
        "machinectl"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Technology"
    ],
    "url": "/blog/linux-containers-with-systemd"
}

A couple of us DDs met here on the weekend. It is always a fun time, being
part of these meetings. We talked briefly about the status of Cross
Compilation in Debian, on the tools that simplify the process.

Next we touched upon licensing, discussing the benefits of particular licenses
(BSD, Apache, GPL) from the point of view of the consumer. The consumer being
an individual just wanting to use/improve software, to a consumer who's
building a (free / non-free) product on top of it. I think the overall
conclusion was that there are 2 major licenses at a high level: Ones those
allow you take the code and not give back, and the others which allow you to
take code only if you are ready to share the enhancements back and forward.

Next we briefly touched upon systemd. Given that I recently spent a good
amount of time talking to the systemd maintainer while fixing bugs in my
software, it was natural for me to steer that topic. At the end, more people
are now enthused to learn the paradigm shift.

The other topic where we spent time was on Containers. It is impressive to see
how quick, and how many, products have now spun out of cgroups. The topic
moved to cgroups, thanks to systemd, one of the prime consumers of cgroups.
While demonstrating the functionalities of Linux Containers (LXC), I realized
that systemd has a tool in place to serve the same use case.

So, once back home, I spent some time figuring out the possibility to replace
my lxc setup, with that of systemd-nspawn. Apart from a [minor
bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=792882), almost
everything else seems to work find with _systemd-nspawn_.

So, following is the config detail of my container, as used in lxc. And to
replace lxc, I need to fill is almost all of it with systemd-nspawn.

    
    
    rrs@learner:~$ sudo cat /var/lib/lxc/deb-template/config
    # Template used to create this container: /usr/share/lxc/templates/lxc-debian
    # Parameters passed to the template:
    # For additional config options, please look at lxc.container.conf(5)
    
    # CPU
    lxc.cgroup.cpuset.cpus = 0,1
    lxc.cgroup.cpu.shares = 1234
    
    # Mem
    lxc.cgroup.memory.limit_in_bytes = 2000M
    lxc.cgroup.memory.soft_limit_in_bytes = 1500M
    
    # Network
    lxc.network.type = veth
    lxc.network.hwaddr = 00:16:3e:0c:c5:d4
    lxc.network.flags = up
    lxc.network.link = lxcbr0
    
    # Root file system
    lxc.rootfs = /var/lib/lxc/deb-template/rootfs
    
    # Common configuration
    lxc.include = /usr/share/lxc/config/debian.common.conf
    
    # Container specific configuration
    lxc.mount = /var/lib/lxc/deb-template/fstab
    lxc.utsname = deb-template
    lxc.arch = amd64
    
    # For apt
    lxc.mount.entry = /var/cache/apt/archives var/cache/apt/archives none defaults,bind 0 0
    lxc.mount.entry = /var/tmp/lxc var/tmp/lxc none defaults,bind 0 0
    2015-07-20 / 16:28:58 ♒♒♒  ☺    
    



The equivalent of the above, in systemd-nspawn is:

    
    
    sudo systemd-nspawn -n -b --machine deb-template --network-bridge=lxcbr0 --bind /var/cache/apt/archives/

The only missing bit is CPU and Memory, which I'm yet to try, is documented as
doable with the _systemctl --property=_ interface

    
    
          --property=
               Set a unit property on the scope unit to register for the machine. This only
               applies if the machine is run in its own scope unit, i.e. if --keep-unit is not
               used. Takes unit property assignments in the same format as systemctl
               set-property. This is useful to set memory limits and similar for machines.
    

With all this in place, using containers under systemd is a breeze.

    
    
    rrs@learner:~/Community/Packaging/multipath-tools (experimental)$ sudo machinectl list
    MACHINE      CLASS     SERVICE
    deb-template container nspawn
    
    1 machines listed.
    2015-07-20 / 16:44:07 ♒♒♒  ☺    
    rrs@learner:~/Community/Packaging/multipath-tools (experimental)$ sudo machinectl status deb-template
    deb-template
               Since: Mon 2015-07-20 16:13:58 IST; 30min ago
              Leader: 9064 (systemd)
             Service: nspawn; class container
                Root: /var/lib/lxc/deb-template/rootfs
               Iface: lxcbr0
             Address: 172.16.10.12
                      fe80::706d:e4ff:fec5:bfc8%3
                  OS: Debian GNU/Linux stretch/sid
                Unit: machine-deb\x2dtemplate.scope
                      ├─9064 /lib/systemd/systemd --system --deserialize 14
                      └─system.slice
                        ├─systemd-journald.service
                        │ └─9092 /lib/systemd/systemd-journald
                        ├─ssh.service
                        │ └─9160 /usr/sbin/sshd -D
                        └─console-getty.service
                          ├─9166 /bin/login --     
                          ├─9171 -bash
                          └─9226 dhclient host0
    
    Jul 20 16:13:58 learner systemd[1]: Started Container deb-template.
    Jul 20 16:13:58 learner systemd[1]: Starting Container deb-template.
    2015-07-20 / 16:44:15 ♒♒♒  ☺    
    

