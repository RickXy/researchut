{
    "title": "laptop-mode-tools",
    "date": "2011-02-15T10:41:23-05:00",
    "lastmod": "2013-01-31T13:38:22-05:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools",
        "udev",
        "pm-utils",
        "acpid"
    ],
    "categories": [
        "Debian-Blog",
        "General",
        "Programming"
    ]
}

I just released **laptop-mode-tools** version 1.56.

This release adds many goodies. We now have a better calling application to
rely upon, **udev**. The linux kernel generates many events based on
conditions. udev is a framework that acts on those events and can call-in
applications based on conditions. This is something I wanted to do for laptop-
mode-tools.

 **Power State**

First was power state. The power_supply subsystem can sense changes and
generate events. We simply act on the **change** action here and call in
laptop-mode-tools because we want laptop-mode-tools run on both **ON_AC** and
**ON_BAT**

 **Suspend/Resume**

Suspend/Resume was my bigger concern. pm-utils no more likes us. In fact it
now has direct Conflict: defined in the Package Managers. For laptop-mode-
tools, on suspend, there was not much to care about. But I did want to ensure
that we act on resume because:

  * User could suspend when **ON_AC**
  * Go back home
  * Resume when **ON_BAT**

The **machinecheck** subsystem helped here. I hope my assumption that this
event in only seen during suspend/resume stands true.

 **Multi Events and Locking**

Acting on events is good but can be challenging because the kernel can
generate a large number of events and we have a danger of ending up spawning
just too many instances. **flock** came to the rescue there.



So overall, by depending on events, we are better now. We can cut down on many
dependencies. Especially the **Power Management Daemon** dependency because
different arches had diffrerent managers.

pm-utils conflict is disappointing especially for the fact that that conflict
is inherited in Debian too. I hope the pm-utils maintainer
[reconsiders](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=612710).
Uninstalling pm-utils has the challenge that the DEs lose the suspend
functionality. Does anyone know how the DEs call them? Can a minor hacky
workaround be provided?

