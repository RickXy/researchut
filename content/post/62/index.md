{
    "title": "Mobiking Revisited",
    "date": "2007-10-19T06:22:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "General",
        "Fun"
    ]
}

It has been around 5 yrs when I suffered a broken collar bone in a road
accident on my bike.

My take since then had been to avoid biking in Indian City Roads. But I
finally gave up on it because I realised how much I really missed avoiding it.

So here I am, back to biking.

I wanted to start up again with a bike which would be:

  * Steady on the roads.
  * Would definitely have a sporty look.
  * Would be Mr. Unshakable and Mr. Reliable

Amongst the Indian bikes in the market today, the best I could find to my
requirements was the [Bajaj Pulsar 200
DTS-i.](http://www.bajajauto.com/pulsar/index.htm)

Not the best Sport Bike, but definitely is one of the best available in the
Indian market.

PS: I really liked the rear wide tyres which provide extra grip on the roads.

Happy Biking!

![](http://www.researchut.com/blog/images/dsc00994-blog.jpg)

![](http://www.researchut.com/blog/images/dsc00993-blog.jpg)

