{
    "title": "Fixing broken videos",
    "date": "2015-10-30T13:10:07-04:00",
    "lastmod": "2015-10-30T13:10:07-04:00",
    "draft": "false",
    "tags": [
        "video",
        "gpac",
        "data recovery",
        "MP4Box"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/recover-damaged-video"
}

Post iPhone launch, in this new age, it is common to carry a device capable of
capturing high definition images and videos. Most people today carry a high
end device capable of capturing video/audio, in the form of a Mobile Phone.
This has made us raise our expectation that we should capture most moments, if
not all.

But as is with everything, sometimes things ditch you right at the moment when
you did not want it to. In new age phones, scenarios usually are: you
capturing a video and the actual data not written to the disk (battery drain,
cpu overheat, software bug: Whatever). Such was my case from the year 2012,
where one of the videos was corrupt. Corrupt as in, **A/V not in sync**.

Now everytime I solve a real world problem using Free Software tools, I feel
the need to express my gratitude. It also serves as a note for me (and maybe
others too), for future, if I run into similar problems. ;-)

So as it turns out (in layman terms), a video clip is nothing but 2 streams:
Video and Audio. Now most video run at 30 frames per second. And the audio
stream synchronizes with the video frames. Sorry, I did not read up beyond
that, as I was able to fix my problem.
![cheeky](http://www.researchut.com/sites/all/libraries/ckeditor/plugins/smiley/images/tounge_smile.gif)![wink](http://www.researchut.com/sites/all/libraries/ckeditor/plugins/smiley/images/wink_smile.gif)

So, my thought was that the video stream is not corrupt. It just is rendering
at a slower frame rate. So, there were 2 problems. 1: Video rendering slower
than the usual 30 fps rate. 2: As a result of 1, audio not being in sync with
video.

Thanks to gpac tools, even a novice, I was able to quickly separate the
streams. And then re-encode the video stream into 30 fps. And then merge the
audio + video stream together. And the result: Just awesome.

In below steps, Strip the video which results in a raw H264 data file. Similar
for audio. And then just re-encode the streams, specifying the framerate.

    
    
    _MP4Box -add 20121002_164608.mp4#video -raw 1 -new test1_
    
     test1_track1.h264
    
    _MP4Box -add 20121002_164608.mp4#audio -raw 2 -new test1_audio_
    
     test1_audio_track2.aac
    
    _MP4Box -add test1_track1.h264:fps=30 -add test1_audio_track2.aac -new test1_audio_30fps.mp4_

