{
    "title": "Glory of Eternal Uma-7 Manifestations (Part-III)",
    "date": "2016-01-10T12:11:46-05:00",
    "lastmod": "2016-01-10T12:11:46-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism",
        "religion",
        "Uma"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/glory-uma-7"
}

[![adi-shakti-tridevi](/sites/default/files/adi-shakti-
tridevi.jpg)](https://vibhormahajan.files.wordpress.com/2012/10/adi-shakti-
tridevi.jpg)

**Shatakshi Durga Tale**

A great trouble maker demon named Durgama was once the scourge of all the
faithful's, the gods, sages, nobles and peace loving people of the three
worlds. Great was his physical power and master of Vedas he had become. He had
made a long penance and had asked for Vedas as a boon from propitiated Brahma.
He had studied them all and had become all knowing. Durgama found out that the
gods got empowered by the religious acts of yajna, havana, penance, charity,
fasts and worship regimes. As he became the ruler of all the worlds he banned
all those religious acts to debilitate the gods. The gods were no more getting
empowered and their divinity was diminishing.

The gods got together and prayed to primordial Devi power Uma to redeem them.
In the prayer the gods revealed how evil Durgama was squeezing divinity out of
them and tormenting them. How woe begotten had become their existence! The
demon had monopolised the knowledge of Vedas. The water was not available and
without it no religious ritual was possible. No rain could fall as water god
and rain god had been rendered powerless by the demon Durgama. They no more
received empowering oblations as no yajna and havana was being performed. They
prayed to Mahamaya Uma to deliver them from Durgama.

Mahamaya materialised in the form of one hundred eyed Shatakshi. In her many
hands she held bow, arrows, lotus, tubers, vegetable, corn and weapons. Her
one hundred eyes shed tears of sympathy for the parched lands and the
thirsting creatures. Her tears flowed to become streams to provide water. The
people were blessed with plentiful bounties of food grains, vegetables and
tubers. The land again greened up. The people could harvest the crops again
with water aplenty. All the tanks, lakes, pools and wells were full of water.

[![shatakshi_devi__the_goddess_with_innumerable_eyes_wg34](/sites/default/files/shatakshi_devi__the_goddess_with_innumerable_eyes_wg34.jpg)](https://vibhormahajan.files.wordpress.com/2012/10/shatakshi_devi__the_goddess_with_innumerable_eyes_wg34.jpg)

Then, Mahamaya Uma asked the praying deities and the gods to seek boons from
her. She was beseeched and retrieve the Vedas from his possession. The human
priest also made the same request as the demons were missing the knowledge of
Vedas.

This manifestation of Mahamaya Uma was named  _' Shatakshi'_ for having
hundred eyes and 'Shakambari' for providing food and vegetables to the people.

Meanwhile, Durgama was getting uneasy about the situation. He was feeling the
vibes that his enemy gods were again getting empowered and were gaining
strength. He could not understand what was causing it. He decided to raid the
celestial domain once again to destroy whatever was causing the revival of the
gods. But he could not reach the capital city of gods Amravati because his way
was barred by the incarnation of Mahamaya Uma. She stood there with her divine
wheel and other weapons ready to face the foe. The host of gods stood behind
her. Durgama was not alone. He was backed up by a huge force of demon
warriors.

Seeing this Devi produced a host of her alter forms in Kali, Tara,
Chinnamastaka, Shri Vidya, Bhuvaneshwari, Bhairavi, Bagala, Matangi, Tripura
Sundari and Rauravi. These ten alter forms further generated their own hosts.
Thus, a great goddess force launched itself on the demon host. The demons were
cut down like fodder. A mighty blow from her own self felled the mighty demon
Durgama. For this victory over demon Durgama, the Devi incarnation also got
the name of  _' Durga'_ which later became an epithet or adjective noun
meaning  _' Fierce and indomitable battler '_.

For slaying demons she is known also as  _' Bhīma Devi'_. The consort of Shiva
manifestation of Mahamaya Uma came to be known as  _' Parvati'_ because she
dwelled in mountains (Parvat), Himalaya being her parental adobe and Kailasha
mountain as her marital home. Devi got the name of  _' Bhramarambha'_when she
sent black bees (Bhramars) to kill Aruna.

 **" JAI MATA DI"**



 **Copyright (C) Vibhor Mahajan**

