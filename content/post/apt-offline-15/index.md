{
    "title": "apt-offline 1.5",
    "date": "2014-09-15T14:17:12-04:00",
    "lastmod": "2014-09-15T14:17:12-04:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "Offline APT Package Manager"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/apt-offline-15"
}

I am very pleased to announce the release of apt-offline, version 1.5.

In version 1.4, the offline bug report functionality had to be dropped. In
version 1.5, it is back again. apt-offline now uses the new Debian native BTS
library. Thanks to its developers, this library is much more slim and neat.
The only catch is that it depends on the SOAPpy library which currently is not
stock in Python. If you run apt-offline of Debian, you may not have to worry
as I will add a Recommends on that package. For users using it on Microsoft
Windows, please ensure that you have the SOAPpy library installed. It is
available on pypi.

The old bundled magic library has been replaced with the version of python
magic library that Debian ships. This library is derived from the file package
and is portable on almost all Unixes. For Debian users, there will be a
Recommends on it too.

There were also a bunch of old, outstanding, and annoying bugs that have been
fixed in this release. For a full list of changes, please refer to the git
logs.

With this release, apt-offline should be in good shape for the Jessie release.

apt-offline is available on Alioth @ https://alioth.debian.org/projects/apt-
offline/

