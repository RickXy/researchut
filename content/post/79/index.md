{
    "title": "knetstats for Debian Etch/Sid",
    "date": "2006-11-12T06:02:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

Debian is a great OS Distribution. It itself is a great community. But there
are times when Debian is very frustrating also. Like many of the packages go
on a very slow pace.

 Truly speaking, there isn't much a person can ask the Debian community given
the amount of contribution it makes.

I was looking for knetstats package but couldn't find much help. The ITP
against knetstats is more than 100 days but still not entered into Debian. And
on the knetstats page, the debian package mentioned is outdated.

 This was a great opportunity for me to try out Debian packaging. I had done
read the guides earlier but never paid much attention to building a package.

So hence here is knetstats 1.6.1 for Debian Etch/Sid. I've tested it
installing it onto another machine and it works without any problems. There
are some lintian warnings but they are safe to ignore. I'll fix those soon.

 rrs@geeKISSexy:/var/tmp/knetstat $ lintian knetstats_1.6.1-1_i386.deb  
W: knetstats: binary-without-manpage knetstats  
W: knetstats: menu-item-needs-tag-has-unknown-value kde
/usr/share/menu/knetstats:2  
W: knetstats: binary-or-shlib-defines-rpath ./usr/bin/knetstats
/usr/lib:/usr/share/qt3/lib

Here's the link to the package. Please mail me if you encounter any problems
or if it "Just Works"

[knetstats_1.6.1-1_i386.deb](http://www.researchut.com/blog/images/knetstats_1.6.1-1_i386.deb)

