{
    "title": "Basis B1",
    "date": "2014-04-22T15:32:32-04:00",
    "lastmod": "2014-11-12T03:22:19-05:00",
    "draft": "false",
    "tags": [
        "Basis B1"
    ],
    "categories": [
        "Debian-Blog",
        "Technology"
    ],
    "url": "/blog/basis-b1"
}



![](/sites/default/files/Basis%20IMG_20140423_004242.jpg)

Starting yesterday, I am a happy user of the [Basis B1 (Carbon Edition) Smart
Watch](http://www.mybasis.com)

The company recently announced being acquired by Intel. Overall I like the
watch. The price is steep, but if you care of a watch like that, you may as
well try Basis. In case you want to go through the details, there's a

pretty comprehensive review
[here](http://www.dcrainmaker.com/2013/07/basis-b1-review.html).

Since I've been wearing it for just over 24hrs, there's not much data to
showcase a trend. But the device was impressively precise in monitoring my
sleep.



![](/sites/default/files/Screenshot_2014-04-23-00-52-20.png)

Pain points - For now, sync is the core of the pains. You need either a Mac or
a Windows PC. I have a Windows 7 VM with USB Passthru, but that doesn't work.
There's also an option to sync over mobile (iOS and Android). That again does
not work for my Chinese Mobile Handset running MIUI.

