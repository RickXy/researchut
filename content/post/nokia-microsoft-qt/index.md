{
    "title": "Microsoft, Nokia and Qt",
    "date": "2011-03-07T13:08:57-05:00",
    "lastmod": "2013-01-31T13:36:33-05:00",
    "draft": "false",
    "tags": [
        "Nokia",
        "Microsoft",
        "Qt",
        "KDE"
    ],
    "categories": [
        "Debian-Blog",
        "General",
        "KDE"
    ],
    "url": "/blog/nokia-microsoft-qt"
}

Nokia has [announced](http://blog.qt.nokia.com/2011/03/07/nokia-and-digia-
working-together/) that it is selling off its Qt business. This needed to
happen given the change in the company's strategy. Microsoft is a software
vendor providing Mobile OS, with Nokia being one of its consumers. For Nokia,
to have a tier 1 alliance with Microsoft, it would have been important for the
company to show positive gesture. With this move, Microsoft and Nokia will
strengthen their partnership. This move will focus on tighter integration in
between the 2 company's portfolios.

Nokia mentioned in the announcement that it'd further continue Qt core
development. I don't see what value that has to bring to the company. It
either has to do with the company making some money with whatever IP it
acquired in Trolltech, or else, it'd be just a phased wise approach in selling
off the Qt business. Also it could be Nokia's **Plan B** , in case the
**Micro-Noki** team fails to build the ecosystem that it is aspiring for.

As for Qt, we'll have to see. Given that it is GPL, we don't have too much to
worry (hopefully).

