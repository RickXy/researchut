{
    "title": "NFS - High Stakes",
    "date": "2008-10-25T15:01:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "computer gaming"
    ],
    "categories": [
        "Fun"
    ]
}

For all NFS Old Timers...

Many must be aware of the older Need For Speed games that EA produced. One
among them was **High Stakes**. IMHO, the uniqueness of High Stakes was its
**Damage Mode** feature which could be very well felt in the game. The more
the car's _**engine/steering/suspension**_ got damaged, the more difficult it
was to handle the car then. I could never find the same game play in the later
versions of EA's NFS.

Looks like I wasn't the only one that admired High Stakes. Thanks to people on
the internet, even though EA has completely discontinued High Stakes, High
Stakes is still available. And it has been hacked upon to run on Windows XP.
It is a good old 1999 game and I am still enjoying it now (2008).

