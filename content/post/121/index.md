{
    "title": "pypt-offline",
    "date": "2005-07-04T04:54:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "offline package manager"
    ],
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

My first contribution to [FSF](http://www.fsf.org). [pypt-
offline](http://www.researchut.com/repository/pypt-offline.html "Offline
Package Manager") is an offline package manager for
[Debian](http://www.debian.org) and its derivatives.

