{
    "title": "The 'F' Word",
    "date": "2010-06-01T19:37:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Psyche",
        "Rant"
    ]
}

The blog entry is about the **_F_** word.

What the fuck? You don't know what the **_F_** word is. Sigh!!!

Mea Culpa! Not your fault. It is quite offensive, no? More offensive than _BC,
MC, BsdK, LkB_ ?These are some of the usual terms used in our local derived
languages in all its locally derived richness.

The word Fuck, many times, either from people with authority (HR) or the
receiver, is termed and reacted to as a very offensive term. Usage of the word
fuck is kind of blasphemy. In organizations, its usage can lead to harassments
(to all those ultra fuckin' dumbs) and terminations. Sigh!!

Well! **_Yuck!_** to all those, who think that way.

 ** _Fuck_** is the most neutral word to express all kinds of feelings, be it
_anger, agression, disappointment, love, sex, celebration_ or anything else
you can think of.

  * Fuck you man
  * I'm gonna fuckin' kill you.
  * Fuck! It is painful.
  * It was fuckin' amazing
  * Let's fuck

See. How offensive it is. Now let's localize it with its equivalents.

  * BC, Saala
  * BC/MC, I'll kill you
  * BC/MC, it is painful
  * G phat gayi. It is so painful
  * MC. Maza aa gaya

See. How nice it is. Not offensive at all. One can always argue that those
words are not required to be used in the local languages, rather it is
offensive terms not preferred. But be honest and ask yourself if you haven't
used them when expressing yourself.

So when someone uses the **_F_** word, there isn't really a good reason to
make those fucked up faces and feel offensive. The person is just making use
of the  best expressive neutral word to express himself.

At least, this way, while expressing himself, he still is sparing families and
friends. So go ahead and make use of this wonderful **_F_** word and educate
people on its endless possible usage. Or else if you still don't get it, I got
_2_ words: **Fuck You**.

