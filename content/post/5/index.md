{
    "title": "Icedove",
    "date": "2010-11-07T12:46:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "KDE",
        "kmail",
        "knode",
        "Icedove",
        "Akregator"
    ],
    "categories": [
        "Tools",
        "KDE",
        "Rant"
    ]
}

My recent experience with Mozilla ThuderBird, IceDove.

With KDE 4.x, the KDE team took a radical step of ripping apart most of the
stuff and rethinking many of the designs. Quite a bold move. Many people
appreciated KDE's efforts to start afresh while others moved away from KDE.
For some reasons, I decided to stick to KDE. Maybe it was because of the
awesome flexibility KDE provided provides to customize the DE to one's
personal taste. Or maybe because I was too used to the KDE way of doing
things. I stayed with KDE while 4.0 was released and stayed with it up till
very recently.

I started to lose my patience with the PIM Suite. I know the PIM team is also
going to some very radical changes which will bring very innovative stuff
later. But, at the moment, the KDE PIM suite is very broken. Broken not in the
first impress, but broken when you regularly use it. It leaks memory like
anything and keeps doing lots and lots of I/O. I hope some day the KDE team
decides that they do need a **core team** , a core team that could take care
of important tasks, making sure that the imporatant tasks are **Continuously
Usable**. It takes time to earn the reputation but it takes a lot lesser time
to lose it.

Anyways, having been a KDE (PIM) user for long, I had been bearing the PIM
torments up until, I recently saw a colleague using IceDove. There was a time
when the Mozilla suite was in itself terribly slow. But things seem to have
changed a lot. Both, the browser and the PIM suite, have improved a lot lot in
terms of functionality and performance. Performance is very important. What
good is a feature if its performance is terrible and it hinders the usability.
Some of the things that really impressed me were:

  * Indexing
  * Organization

**Indexing** : Nepomuk might be good one day but that day is yet to be seen. I
patiently wait for that day to come. Well, don't have much choice. Have been
patiently waiting for it since KDE 4.0 was released. KDE has great ideas with
Nepomuk which is good. But realisticly, what all do you want to see indexed ?
There has to be a realistic line drawn. The browsers already have indexing for
the history. Amarok, the player, also does indexing. It can tell you when your
last favorite song was played. For pictures, I have the awesome KPhotoAlbum
that cannot be beaten by anything. But above all, the most important thing to
index is your conversation. Those emails that you send daily. And indexing is
no good if you can't find the information you need, later. IceDove has filled
that place. It does an excellent indexing (in terms of performance) and
presents a very user intuitive way to narrowing it down to, when looking for a
particular information.

**Organization:** The other great thing about KDE PIM is its ability to break
down its applications into small parts and glue them together into a new, well
integrated application. Yes, Kontact. It  is used to be the best PIM
application. Used common libraries to make the suite more efficient than the
rest. But all this was **used to be**. Today, kpart itself might be Kontact's
problem. We have different applications glued together that if built with a
common design, could have benefited a lot more. Take for example: kmail, knode
and akregator. They are all very important parts of the PIM suite. Yet all
three are different. The only thing common amonst them is is that they are
available from the same Kontact shell. kmail has a different navigation.
akregator has very nice aggregated folders but the same cannot be available in
knode and kmail. knode, while still okay, has been rotting for some time. So
it was time to see how IceDove performed when testing it up against this Use
Case. The good thing about IceDove is is that it has one single uniform
interface to most of the PIM needs. I can use the same window and the same
interface for all my rss feeds, my emails and my leafnode newsgroups. That has
a big benefit in itself. I have only 1 integrated interface to look at and
only 1 interactive method to learn. There aren't different keystrokes for
different applications. All is one in IceDove.

And I think one of the main reasons for Mozilla's success is its plugin
architecture. It is very difficult to satisfy everyone's needs. In the same
way, it is very difficult for one group of developer's to be able to innovate
differently. This is where Mozilla rocked. They provided a solid foundation
with basic standard interface and let new fresh minds to do the rest of the
innovation. Turns out it has worked well.

So, with my PIM needs satisfied, I thought KDE was serving as nothing but just
a mere shell. So, now was the right time to do the thing I always thought of
doing. Switch to GNOME. GNOME looks elegant at first look but that is it. I
wanted to take a screenshot of an application to report a bug. I fired up
PrntSc key to let the screen capture utility pop-up. It did not have the
opiton to select just the application window. Hmmm! Time to return back to KDE
and use the Mozilla PIM suite and hope the KDE PIM team learns and does the
right thing.

