{
    "title": "Debugging Techniques",
    "date": "2006-06-13T14:31:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Programming"
    ]
}

_**Abnormal Program Termination**_ are not something just seen on Microsoft
Windows. I hope most users will have seen some kind of program error in
GNU/Linux and *BSD platforms also.

Abnormal Program Termination can happen for multiple reasons of which
**Programming Errors** are one of the major reasons. ~~Badly~~ Carelessly
written programs often end up having such bugs.

Here's a sample C program which has generic programming errors because of
which the compiler doesn't catch it but the program does do unexpected
behavior.

#include <stdio.h>  
#include <unistd.h>  
  
int divide (int x, int y);  
  
int main(void)  
{  
    int x, y = 0;  
  
    printf ("Enter divident:\n");  
    scanf ("%d", &x);  
  
    printf ("Enter divisor:\n");  
    scanf ("%d", &y);  
  
    printf("The result is %d\n", divide(x, y));  
    return 0;  
}  
  
int divide (int x, int y)  
{  
    return x / y;  
}

The problem with the above mentioned code is that it has no checkings for the
integers. This program will give an **Arithmetic Division By Zero Error.**
Upon execution of such programs, most Unixes during termination of the program
dump the core.

A core dump is noting but the state of the memory when the abnormal behavior
occured. This comes out to be very handly to debug nasty bugs which creep away
from the compiler.

Assuming you execute this program and get the core dump, you can easily debug
and find the bug with **gdb**.

rrs@learner:~ $ gdb test coreGNU gdb 6.4-debian  
Copyright 2005 Free Software Foundation, Inc.  
GDB is free software, covered by the GNU General Public License, and you are  
welcome to change it and/or distribute copies of it under certain conditions.  
Type "show copying" to see the conditions.  
There is absolutely no warranty for GDB. Type "show warranty" for details.  
This GDB was configured as "i486-linux-gnu"...Using host libthread_db library
"/lib/tls/i686/cmov/libthread_db.so.1".  
  
Core was generated by `./test'.  
Program terminated with signal 8, Arithmetic exception.  
  
warning: Can't read pathname for load map: Input/output error.  
Reading symbols from /lib/tls/i686/cmov/libc.so.6...done.  
Loaded symbols for /lib/tls/i686/cmov/libc.so.6  
Reading symbols from /lib/ld-linux.so.2...done.  
Loaded symbols for /lib/ld-linux.so.2  
#0 0x08048463 in divide (x=9, y=0) at test.c:22  
22 return x / y;  
(gdb)

If you look at the output of gdb you can clearly see what kind of error led to
the unexpected behavior.

(gdb) bt  
#0 0x08048463 in divide (x=9, y=0) at test.c:22  
#1 0x08048437 in main () at test.c:16

You can do a backtrace and figure out the complete flow of the program andd
see where exactly the error occured.

In this case the bug is in the divide() where y is passed the value 0 which
has led to an **Arithmetic Exception.**

Analyzing core dumps is very helpful and fun while debugging applications.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)

HTH,

 **Ritesh Raj Sarraf**

