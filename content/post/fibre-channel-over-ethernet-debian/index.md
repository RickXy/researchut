{
    "title": "Fibre Channel over Ethernet - FCoE",
    "date": "2011-05-21T13:31:28-04:00",
    "lastmod": "2013-01-31T13:51:27-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "fcoe",
        "fc"
    ],
    "categories": [
        "Debian-Pages",
        "Debian-Blog",
        "Technology"
    ],
    "url": "/blog/fibre-channel-over-ethernet-debian"
}

With the final package, [fcoe-utils](http://packages.qa.debian.org/f/fcoe-
utils.html), clearing the NEW packages queue, the Open-FCoE project's **[Fibre
Channel over
Ethernet](http://en.wikipedia.org/wiki/Fibre_Channel_over_Ethernet)** stack is
now in the Debian archive. I had anticipated to have access to FCoE hardware
by the time the packaging would complete but that didn 't work out. It has
been delayed, hence the packages are in experimental. If you have access to
FCoE hardware, please test and provide your feedback.

