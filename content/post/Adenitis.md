{
    "title": "Adenitis",
    "date": "2018-11-14T18:40:06+05:30",
    "lastmod": "2018-11-14T18:40:06+05:30",
    "draft": "false",
    "tags": [
        "Ayurveda",
        "Adenitis"
    ],
    "categories": [
        "Ayurveda",
        "General"
    ],
    "url": "/post/Adenitis"
}

# Adenitis

## What is Adenitis?

Adenitis refers to the inflammation of lymph glands present in our body. Lymph nodes are present everywhere in human body. The main symptoms of adenitis are pain and swelling. Adenoids of different body parts are given different names depending upon their location. Inflammation from one part may reach to other parts of the body. Mainly bacteria or virus is responsible for producing the infection. When inflammation is not diagnosed at an early stage it may further develop and grow into a tumour. Generally, on conventional system surgery is done to remove the tumour of the affected part of the body. But this is not a real cure but inflammation can occur repeatedly.


## Symptoms of Adenitis

There are common symptoms of adenitis that are associated with different types of lymph node inflammation. Some of the common and important symptoms of lymph node inflammation are given here:

 * There is tenderness and hardness of the affected lymph nodes. A hard nodule is formed where the lymph node get inflamed. There is a cluster of cells at the site of inflammation which feels as if a tumour has formed at the place.
 * Fever, tiredness, weakness, cough, soreness is present along with other symptoms. Chilliness is felt all over the body and cough is present with soreness of the throat.
 * The main cause of lymph node inflammation is virus or bacteria and people with lymph node inflammation will give you a history of infection with any such microorganism.
 * The next region becomes tender to touch and there is presence of a hard lump. Fluid may exudates from the affected area due to inflammation.


## Herbal Remedies

 * Divya Gilloy Sattva is a wonderful herbal remedy for relieving pain and inflammation of the lymph glands. It is herbal remedy that provides nourishment to the lymph glands and prevents their pain and swelling.
 * Divya Tamra bhasm is another helpful remedy that prevents growth of tumour in the lymph glands. It is prepared by using the natural tamra bhasm. It helps to prevent the spread of infection to neighbouring parts of the lymph glands.
 * Divya Kanchanar guggulu: This herbal remedy help in the cure of adenitis or inflammation of the lymph glands. This relieves pain and reduces swelling. It is one of the best remedy for the treatment of lymph gland inflammation.
 * Divya Vriddhivaadhika vati: This herbal remedy provides nourishment to the body and prevents any type of inflammation. It gives best results when combined with other herbal remedies. When it is taken regularly it prevents the formation of cancer causing cells and reduces inflammation.


## Home Remedies

There are numerous home remedies useful to prevent the growth of tumours of lymph glands. Some of the useful home remedies for adenitis are given below and these may be taken regularly to prevent any inflammation of the lymph glands:

 * Omega 3 fatty acids are useful for people who are prone to develop the growth of lymph glands. They should regularly take omega 3 fatty acids in their diet to nourish the cells properly and to prevent the growth of tumour cells.
 * Another important vitamin which is helpful in preventing the growth of tumours is vitamin D. Regular intake of vitamin D helps in preventing the over growth of lymph glands.
 * Green tea is also useful as it gives relief from fever and inflammation of the lymph glands. It relieves the soreness of the throat and also prevents coughing.
 * It is recommended to take balanced diet which is rich in different types of nutrients required by our body in the balanced amount.
 * Garlic is also useful for reducing inflammation as it is known to have anti-inflammatory properties. Regular intake of garlic in food helps to boost up the immunity and quickly relieves the inflammation of the lymph glands.
 * Turmeric is another home remedy that has anti-inflammatory properties and help in reducing inflammation in any part of the body. It also helps in relieving pain.
 * Foods rich in anti-oxidants should be eaten because anti-oxidants helps to remove free radicals from the body and prevents inflammatory diseases.


## Prevention of adenitis

 * Adenitis or tumour growth can be easily prevented by making changes in the lifestyle. One should do regular exercise or yoga asana to remain healthy and for preventing growth of abnormal cells.
 * It is recommended to drink more water because it helps in eliminating the waste products from the body.
 * Drinking alcohol should be avoided.
 * Smoking should be avoided.
 * Meals should be eaten at regular intervals and eating late at night should be avoided. It may cause digestion problems and leads to formation of abnormal cells in the body.
 * Fried foods should be avoided as more fats may cause inflammation of the different body parts.

