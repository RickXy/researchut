{
    "title": "Laptop Mode Tools - 1.68",
    "date": "2015-08-27T13:39:15-04:00",
    "lastmod": "2015-08-27T13:41:13-04:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools",
        "LMT",
        "systemd"
    ],
    "categories": [
        "Debian-Blog",
        "Programming",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-1_68"
}

I am please to announce the release of Laptop Mode Tools, version 1.68.

This release is mainly focused on integration with the newer init system,
systemd. Without the help from the awesome Debian systemd maintainers, this
would not have been possible. Thank you folks.

While the focus now is on systemd, LMT will still support the older SysV Init.

With this new release, there are some new files: _laptop-mode.service, laptop-
mode.timer and lmt-poll.service_. All the files should be documented well
enough for users. _lmt-poll.service_ is the equivalent of the module _battery-
level-polling_ , should you need it.

Filtered git log:

    
    
    1.68 - Thu Aug 27 22:36:43 IST 2015
    
        * Fix all instances for BATTERY_LEVEL_POLLING
    
        * Group kill the polling daemon so that its child process get the same signal
    
        * Release the descriptor explicitly
    
        * Add identifier about who's our parent
    
        * Narrow down our power_supply subsystem event check condition
    
        * Fine tune the .service file
    
        * On my ultrabook, AC as reported as ACAD
    
        * Enhance lmt-udev to better work with systemd
    
        * Add a timer based polling for LMT. It is the equivalent of battery-polling-daemon,
    
          using systemd
    
        * Disable battery level polling by default, because most systems will have systemd running
    
        * Add documentation reference in systemd files

The tarball is available @ <http://samwel.tk/laptop_mode/tools/downloads
/laptop-mode-tools_1.68.tar.gz>

The md5 checksum for the tarball is 15edf643990e08deaebebf66b128b270  


