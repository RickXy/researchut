{
    "title": "Laptop Mode Tools 1.66",
    "date": "2014-09-27T05:09:30-04:00",
    "lastmod": "2014-11-12T03:01:24-05:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-1-66"
}

I am pleased to announce the release of [**Laptop Mode
Tools**](http://samwel.tk/laptop_mode) at version **1.66**.

This release fixes an important bug in the way Laptop Mode Tools is invoked.
Users, now when disable it in the config file, the tool will be disabled.
Thanks to **bendlas** @github for narrowing it down. The GUI configuration
tool has been improved, thanks to **Juan**. And there is a new power saving
module for users with ATI Radeon cards. Thanks to **M. Ziebell** for
submitting the patch.

Laptop Mode Tools development can be tracked @
[GitHub](https://github.com/rickysarraf/laptop-mode-tools/)

![](/sites/default/files/LMT New GUI.jpg)

