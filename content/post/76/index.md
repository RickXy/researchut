{
    "title": "Data Synchronization in KDE",
    "date": "2006-12-25T08:25:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Computing"
    ]
}

Synchronization is one of the common problems I face with my multiple
machines. This [article](http://www.researchut.com/docs/kde-resources.html
"Data Synchronization in KDE") is about how KDE help me in keeping my data
synchronized across machines without any problem.

