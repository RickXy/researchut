{
    "title": "VirtualBox",
    "date": "2011-02-17T16:09:06-05:00",
    "lastmod": "2013-01-31T13:37:54-05:00",
    "draft": "false",
    "tags": [
        "VirtualBox",
        "Windows 7"
    ],
    "categories": [
        "Debian-Blog",
        "Virtualization"
    ],
    "url": "/blog/virtualbox-native-partition"
}

I recently ended up acquiring a Windows 7 Professional license. While I rarely
have a need for Windows, that one rare instance is important. Many of the
bank's web interfaces are tightly dependent on IE. I do have a copy of
CrossOver but that doesn't help much. Most times, IE crashes when trying to
render pages. Must be having some OLE dependency.

So I convinced myself to install Windows7. For my banking needs and also to
catch-up on how that platform has been doing. From my **limited** usage, my
current impression has been that   Windows 7 is a great revive. [passwordless]
sudo is conceptually implemented into Windows 7. I'm not sure how good that
would be given that most users would have the tendency to just click on 'Yes'
without paying attention to the pop-up. Anyways.....I installed Windows 7 onto
one of my spare partitions to get the full experience (than doing it in a VM
image).

Reboot is pain... This is what made me look at possibilities of using my
installed copy of Windows 7 into a VM configuration. I've been using KVM for
things here and there but its UI kept me away. Even with virt-manager's
existence over a year or two, I think they still haven't reached what
**qemulator** was early. (Just realized that qemulator now is re-branded
VirtualBricks). I also had had looked at VirtualBox sometime before, so now
was the right time to try it again. And it impressed..........



 **Requirement**

  * Use my installed OS Image from the physical partition
  * Well integrated User Interface

 **Req1**

VirutalBox just rocked. There's pretty [decent
documentation](http://www.virtualbox.org/manual/ch09.html#rawdisk) available
on their website covering all aspect of the product.
[This](http://www.rajatarya.com/website/taming-windows-virtualbox-vm) blog
entry is also a good starting point for Req1. To use the physical partition as
the Guest Image for the VM, the following command is required.

 **VBoxManage internalcommands createrawvmdk -filename /path/to/file.vmdk
-rawdisk /dev/sda -partitions 1,2,3 -relative -register**

The docs have all the details but I'll mention the noteworthy parts. This is
an advanced command not available in the UI. We register our disk/partitions
to the vmdk to be sued with the VM. The **-partitions 1,2,3** is important.
This basically defines the config as,  "Attach my disk /dev/sda with
partitions sda1, sda2 and sda3 to the file vmdk". This will ensure that the
guest VM sees all the partitions natively. This is important because without
the full disk outlook grub would go confused. Also this is important to ensure
that Windows 7  senses no hardware change when switching boots in between
Virtual and Native. The **-register** option is to set this setting permanent
across reboots on the Host OS.

Windows 7 does not have **Hardware Profiles** anymore (or to the best of my
knowledge). What that means is that Windows 7, on every switch, would assume a
possible corruption and play safe by asking you to pop-in the Repaid CD. This
is PITA. To overcome this pain, it is important to layout the image settings
as I 've mentioned above so that in either of the modes, Windows 7 feels that
it is the same disk. This ends bringing a very risky situation. With Debian
running: You have rw access to your 2nd and 3rd partition. And then when you
fire-up the VM, **if** by mistake grub boots into Linux instead of Windows,
kiss your data goodbye. Well, not 100% true, but don't get there.

A better workaround is this:

1) Add your disk image (that points to the physical partition) to your VM
config

2) Add the gurb-rescue-cdrom.iso provided by the grub-rescue-pc package

3) Be very sure that the **only bootable device** selected is the CDROM device
i.e. grub-rescue-cdrom.iso

Do this and there'll be no room for error. We simply bypass our native boot
loader and instead boot off the grub rescue iso. Setting the correct partition
persistent is left as an excercise for the reader.

![](https://lh6.googleusercontent.com/__etqz-
yeP4s/TV2MjgmmbSI/AAAAAAAABSk/qKeUfUUrQ8U/s640/vb7.png)

Okay!! For completeness, this is how it should look.



 **Req 2**

With Req1 achieved, I was pretty happy. But Req2 results got me impressed. For
your eyes.......

![VirtualBox](https://lh5.googleusercontent.com/__etqz-
yeP4s/TV2GQLkzhbI/AAAAAAAABSY/mK9vAD2dwGg/s600/vb6.png)

A pretty neat summary of my Guest VM. You can see most of what it supports.

  * USB
  * PulseAudio
  * Shared Folder
  * SATA Controller
  * VT
  * 2D/3D (Experimental)



![Maximized View](https://lh5.googleusercontent.com/__etqz-
yeP4s/TV2GQeO2vkI/AAAAAAAABSg/W_EDkJ4kCHc/s600/vb4.png)

This one's a Maximized Windowed View of the Guest VM running.



![](https://lh3.googleusercontent.com/__etqz-
yeP4s/TV2GQOQNsoI/AAAAAAAABSc/GPaIcJ6f_dk/s640/vb5.png)



But what impressed me the most is the **Seamless Mode** that VirtualBox
provides. In the above screenshot, if you notice closely you 'll realize that
the desktop displayed is that of KDE's. This is very cool when I use the IE
browser because IE is rendered as almost a regular native application. With
Guest Tools installation, there's no Keyboard Capture/Release. Your Host's
mouse is tightly integrated and will act on events when inside the Guest VM's
display.

Not every thing can be described in words. You need to use it really
experience its features.



An awesome product. And the best - GPL. It comes packaged in 2 editions:
Proprietary (that adds proprietary plugins: iSCSI, RDP etc) and OSE Open
Source Edition (which recently saw the USB feature added to it).

