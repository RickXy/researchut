{
    "title": "Customizing Gallery ver 1",
    "date": "2005-12-04T19:26:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools"
    ]
}

Customizing Gallery ver 1 is as simple as adding your HTML code into
respective **wrapper.header.html** and **wrapper.footer.html** files at the
end, after all the PHP code, in the html_wrap directory.

Simple, Isn't it!

