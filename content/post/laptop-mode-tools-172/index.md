{
    "title": "Laptop Mode Tools 1.71",
    "date": "2017-01-12T03:54:02-05:00",
    "lastmod": "2017-01-12T07:57:01-05:00",
    "draft": "false",
    "tags": [
        "Laptop Mode Tools",
        "power saving"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-172"
}

I am pleased to announce the 1.71 release of Laptop Mode Tools. This release
includes some new modules, some bug fixes, and there are some efficiency
improvements too. Many thanks to our users; most changes in this release are
contributions from our users.

A filtered list of changes in mentioned below. For the full log, please refer
to the git repository.

Source tarball, Feodra/SUSE RPM Packages available at:  
<https://github.com/rickysarraf/laptop-mode-tools/releases>

Debian packages will be available soon in Unstable.

Homepage: <https://github.com/rickysarraf/laptop-mode-tools/wiki>  
Mailing List: <https://groups.google.com/d/forum/laptop-mode-tools>



    
    
    1.71 - Thu Jan 12 13:30:50 IST 2017
        * Fix incorrect import of os.putenv
        * Merge pull request #74 from Coucouf/fix-os-putenv
        * Fix documentation on where we read battery capacity from
        * cpuhotplug: allow disabling specific cpus
        * Merge pull request #78 from aartamonau/cpuhotplug
        * runtime-pm: refactor listed_by_id()
        * wireless-power: Use iw and fallback to iwconfig if it not available
        * Prefer available AC supply information over battery state to determine ON_AC
        * On startup, we want to force the full execution of LMT.
        * Device hotplugs need a forced execution for LMT to apply the proper settings
        * runtime-pm: Refactor list_by_type()
        * kbd-backlight: New module to control keyboard backlight brightness
        * Include Transmit power saving in wireless cards
        * Don't run in a subshell
        * Try harder to check battery charge
        * New module: vgaswitcheroo
        * Revive bluetooth module. Use rfkill primarily. Also don't unload (incomplete list of) kernel modules
    



### **What is Laptop Mode Tools**

    
    
    Description: Tools for Power Savings based on battery/AC status
     Laptop mode is a Linux kernel feature that allows your laptop to save
     considerable power, by allowing the hard drive to spin down for longer
     periods of time. This package contains the userland scripts that are
     needed to enable laptop mode.
     .
     It includes support for automatically enabling laptop mode when the
     computer is working on batteries. It also supports various other power
     management features, such as starting and stopping daemons depending on
     power mode, automatically hibernating if battery levels are too low, and
     adjusting terminal blanking and X11 screen blanking
     .
     laptop-mode-tools uses the Linux kernel's Laptop Mode feature and thus
     is also used on Desktops and Servers to conserve power
    

