{
    "title": "Compaq Presario 2203 running Debian",
    "date": "2005-06-11T01:25:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "Compaq",
        "Presario",
        "2203"
    ],
    "categories": [
        "Debian-Pages",
        "Computing"
    ]
}

Useful [documentation](http://www.researchut.com/docs/mynotebook.html "Compaq
Presario 2203AL On Debian") about my notebook, Compaq Presario 2203AL, running
Debian

