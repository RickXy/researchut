{
    "title": "My APT Repositoy",
    "date": "2007-04-20T20:22:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages"
    ]
}

I've created an APT repository of my own for Debian because the package
inclusion policy in Debian (for good reasons) is very stringent.

So this public apt repository is nothing but a placeholder for packages which
I am not able to push to Debian.

Currently it only has fusecompress

Add the following lines to your /etc/apt/sources.list file for automatic
installation/upgradation of packages.

**# RESEARCHUT - APT Repository  
deb http://www.researchut.com/packages/ sid main contrib non-free  
deb-src http://www.researchut.com/packages/ sid main contrib non-free**

