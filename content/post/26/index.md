{
    "title": "Another Debian Merit",
    "date": "2009-02-19T17:51:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages"
    ]
}

So there was a recent thread on the Destop Architect's forum, where Dan Kegel
discussed the idea of silent upgrades (for Security, at least).

I for one, am not very fond of the idea. Updates/Upgrades (Security or
Features) are something which should be done with some Human Intervention. But
still it is not an idea to be completely ruled out. Free Software is about
choice. There definitely are use cases where Silent Updates look a good
choice. With a carefully crafted policy, this could really help achieve the
ultimate goal that Dan was looking after. Closing down vulnerabilities asap.

One of the concerns I had was that a mere upgrade is not what can protect
vulnerabilites. Standard Package Managers will do the job of updating the
vulnerable libraries with the newer patched ones. But they don't come into
effect untill re-loaded. Take the browser as an example. It is vulnerable. You
silently got it updated by your distro's package manager. But still it is
vulnerable. For the patches to be effective, the browser needs a re-start.

In Debian, you could attain Silent Upgrades with some settings defined in apt.

> apt ships a /etc/cron.daily/apt script.

> So with the following in place:

> APT {

>     Get {

>         Assume-Yes "true"; Upgrade "true";

>     }

> }

>  

So, with the above settings in place, one can achieve Silent Updates. This
ofcourse comes with an assumption that you are not on a Rolling Release of
Debian.

But my focus was about getting those patched libraries/binaries effective. So
here's the cooled feature of Debian, which has been there for years. apt/dpkg
allows you the debconf interface. And there you can set the priority of
messages/questions that you'd like to see. So assume that your remote machine,
that you access through ssh, has a sshd vulnerability:

> Debian updates the sshd package  
>  It'll ask you (Depending of debconf priority), whether it should restart
the sshd daemon

And thus you get a patched sshd package, installed, with the patch effective.

I haven't tried but I believe, if you don't install debconf, dpkg/apt will go
with the default action that is defined in the package by itself.

So with all this in place, Debian has a very nice approach to package
management. I haven't come across a similar interface in any other non-Debian
derived distribution.

