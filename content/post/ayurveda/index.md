{
    "title": "Ayurveda",
    "date": "2017-02-25T14:05:54-05:00",
    "lastmod": "2017-02-25T14:24:30-05:00",
    "draft": "false",
    "tags": [
        "ayurveda"
    ],
    "categories": [
        "General"
    ],
    "url": "/ayurveda"
}

**To quote the 1st Google result:**

    
    
    the traditional Hindu system of medicine (incorporated in Atharva Veda, the last of the four Vedas), which is based on the idea of balance in bodily systems and uses diet, herbal treatment, and yogic breathing.

**To quote WIkipedia:**

    
    
    _Ayurveda_  names three elemental substances, the doshas (called Vata, Pitta and Kapha), and states that a balance of the doshas results in health, while imbalance results in disease.  _Ayurveda_  has eight canonical components, which are derived from classical Sanskrit literature.

**Am I an expert of the topic ?**

    
    
    Nope. The intent is to document whatever is suitable, relevant, legitimate and useful on the topic of Ayurveda. Please use your own judgement when following the recommendations in this section.
    
    

**Attributions**

    
    
    **http://www.swamibabaramdevmedicines.com/**

