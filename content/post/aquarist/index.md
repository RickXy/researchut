{
    "title": "Snails you need",
    "date": "2011-02-26T09:13:08-05:00",
    "lastmod": "2013-01-31T13:36:48-05:00",
    "draft": "false",
    "tags": [
        "snail",
        "aquarium",
        "cleanliness",
        "aquarist"
    ],
    "categories": [
        "General",
        "Fun"
    ],
    "url": "/blog/aquarist"
}

Fishkeeping is a very common hobby and an easy one to start. What's
challenging is is to be able to continue the hobby. Very often most people
give up on fish keeping.

Reasons being:

  * Water - Gets contaminated too soon.
  * Fish - All of a sudden they start dying and you'd hate to see that happen
  * Cleanliness - Sometimes, it just becomes a burden cleaning the tank

All the points above are inter-related. If you can crack 1-2 of them, you
might have a better experience with your fishkeeping hobby.

I've been an aquarist for just about 2 years. I've been a victim of all the 3
above bulleted problems. But now, I have kinda worked it out and haven't had
any trouble.

 **Tips:** You'll find plenty of online information on do's and don'ts on this
topic. Here's my thought derived from them and with my 2 years of observation.

  * Never empty the tank in trying to clean it. That just disrupts the eco-system of the tank and leads to, all of a sudden, of all fishes dying. Freshen like, half of the tank's water. That is enough.
  * What is important is to clean the bottom of the tank. That's where all the waste gets accumulated. The best solution to it is to use a suction pump. Nothing fancy - a simple one will do. The suction pump does a pretty good job of sucking out all the waste from the bottom of the tank and also sucks of some of the waste from the gravels.
  * Fish. Never over-populate your tank. Too many problems. Not enought space for the fish. Too much of waste they'd generate. And too frequently you'd be required to clean your tank
  * Snails - My experience has been that snails are a must to your aquarium. They do the awesomest job of keeping your tank's bottom and the sides clean. I have like 5 snails in my tank and my tank is cleaned up by them round the clock. I love this slug which works for me all the time without demanding any wage. On the internet, I've read many people mentioning the challenges they've had when snails have sneaked in into their tanks. That they increase in numbers very quickly and that it becomes a nightmare of cleaning them out. Fortunately, for me that never occured. For whatevery reasons, that I don't know, my snails have never copulated (or what that species' equivalent is). So I've not had to deal with that situation. If you can figure out the reason OR if you can deal with that situation, I highly recommend having some snails in your tank. They keep your tank very clean and help you do what you actually want i.e. **watch your tank**.

