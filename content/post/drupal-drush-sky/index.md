{
    "title": "Drupal maintenance with Drush",
    "date": "2015-05-14T09:10:14-04:00",
    "lastmod": "2015-05-14T09:20:07-04:00",
    "draft": "false",
    "tags": [
        "drupal",
        "drush",
        "sky",
        "adaptive themes"
    ],
    "categories": [
        "Debian-Blog"
    ],
    "url": "/blog/drupal-drush-sky"
}

Another of my articles for self. Writing it down on the website is much better
than pushing it on a 3rd party social site. Your data is yours.

My site runs on Drupal. Given I'm not a web designer, it is not my core area.
Thus I've always wanted to have minimal engagement with it. My practices have
paid me well so far. And I should thank all the free tools that help do that.
I like to keep a running snapshot of my website on my local laptop, to keep it
handy when trying anything new. This means that the setup has to be almost
identical to what is running remotely.

Thanks to Drush, managing Drupal is very easy. It allows me to easily try to
out changes and push them from **dev = > staging => live** withtout too much
effort. It also helps me control the environment well. And since the whole
transport is over SSH, no separate exceptions are required.

For long, the theme on my site had some issues. The taxonomy terms did not
have proper spacing. See [bug](https://www.drupal.org/node/1856814) for
details.

![](/sites/default/files/Drupal-Sky-Theme-Before.png)

With the [fix](https://www.drupal.org/node/1856814#comment-9921244), this
transformed into:

![](/sites/default/files/Drupal-Sky-Theme-After.png)



I wish Drush had support for revision control. Or maybe it already has, and I
need to check ? Bug Fixes and Customizations would have been well recorded
with a revision control system.



