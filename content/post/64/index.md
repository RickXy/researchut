{
    "title": "ANSI Color Codes",
    "date": "2007-08-10T08:00:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Computing"
    ]
}

So in the search of finding a simple solution for printing colored text in
python, it took me some time to dig up. Most solutions people have done were
using curses or some modules. My ultimate intention has always been to cut
down on the dependency on the **3rd party modules**.

I think the **ANSI Color Codes** would be good enough for my requirements to
print colored text on an ANSI compliant terminal.

The **ANSI Terminal Specification** gives programs the ability to change the
text color or background color.  
An ansi code begins with the ESC character **[^** (ascii 27) followed by a
number (or 2 or more separated by a semicolon) and a letter.  
  
In the case of colour codes, the trailing letter is  " **m** "...  
  
So as an example, we have **ESC[31m** ... this will change the foreground
colour to red.  
  
The codes are as follows:

_**For Foreground Colors**_ _  
1m      -     Hicolour (bold) mode  
4m     -     Underline (doesn't seem to work)  
5m     -     BLINK!!  
8m     -     Hidden (same colour as bg)  
30m    -    Black  
31m    -    Red  
32m    -    Green  
33m    -    Yellow  
34m    -    Blue  
35m    -    Magenta  
36m    -    Cyan  
37m    -    White_

_**For Background Colors**_

_40m     -    Change Background to Black  
41m    -    Red  
42m    -    Green  
43m    -    Yellow  
44m    -    Blue  
45m    -    Magenta  
46m    -    Cyan  
47m    -    White  
_

_7m      -     Change to Black text on a White bg  
0m     -     Turn off all attributes._  
  
Now for example, say I wanted blinking, yellow text on a magenta background...
I'd type ESC[45;33;5m

This isn't a complete list of features provided by the ANSI Codes. There are
many more....

