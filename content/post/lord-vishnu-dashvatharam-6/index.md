{
    "title": "Lord Vishnu – Dashavatharam (Part – VI)",
    "date": "2016-02-25T04:53:55-05:00",
    "lastmod": "2016-02-25T04:53:55-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "buddha",
        "siddhartha"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/lord-vishnu-dashvatharam-6"
}

## Lord Vishnu – Dashavatharam (Part – VI)



[![22](/sites/default/files/Vibhor/22.jpg)](/sites/default/files/Vibhor/22.jpg)

With the departure of Lord Krishna, the Kali yuga set in. In this age, the
true devotion to Vedas was replaced by empty rituals. To enlighten the world,
Lord Vishnu descended to Earth as Lord Buddha, the enlightened one.

Lord Buddha was born as the crown prince of the Kapilavastu to King
Suddhodana and Queen Maya. He was named Siddhartha, meaning “All thing
fulfilled”. On his birth the wise men made predictions – “O King,” they said,
“the signs of the Prince’s birth are most favourable. Your son will grow up to
be even greater than you are now!”

Later an old sage named Asita, who lived by himself in the distant forests
visited the palace to see the new born. The King and Queen were Surprised that
Asita would leave his forest home and appear at their palace. The king was
very happy and excited, he brought the baby to sage Asita. For a long time the
holy man gazed at the infant and then said – “Oh dear king, If your son
decides to stay with you and become a king, he will be the greatest king in
history. He rule a vast realm and bring his people much peace and happiness.
But if he decides not to become a king, his future will be even greater! He
will become a great teacher, showing all people how to live with peace and
love in their hearts. Seeing the sadness in the world he will leave your
palace and discover a way to end all suffering. Then he will teach this way to
whoever will listen.”

[![04](/sites/default/files/Vibhor/04.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/04.jpg)

Siddhartha was destined to lead a luxurious life as a prince and had three
palaces, for three different seasonal occupations, especially built for him.
King Suddhodana’s wish for Siddhartha was to become a great king and so he
shielded him from religious teachings or any knowledge  
of human suffering. When he was seven, the Prince began to receive scholarly
instruction from the Institute of Vissavamitr, which was the best education a
Prince could receive. But on the seventh day of his study he had graduated and
become an expert in all forms of knowledge.

When the Prince reached the age of sixteen, the King arranged his marriage to
Princess Yasodhara.

[![07](/sites/default/files/Vibhor/07.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/07.jpg)

Although she was beautiful and he himself was continually surrounded by
beautiful female servants and slaves,Prince Siddhartha was not satisfied
simply with this beauty and felt that there was more to a person’s life than
wealth and beauty. He looked around his palace and he found nothing to satisfy
his quest to seek out the answers to his questions. One day he was traveling
out of the palace walls with his charioteer, Channa. In the city, he found
living beings: a new born child, a diseased man, an old and decaying corpse,
and finally an ascetic.

On that day, after returning to his palace, his son, Prince Rahula, was born.
Siddhartha felt that he, his wife, his new born son, and all people would be
living under the same cycle of beings that he had seen that day. He considered
his own predicament, thought about his talents and potential, and became
determined to set himself, and all others around the world, free from this
cycle of suffering. He, alone, would find the answer.

[![11](/sites/default/files/Vibhor/11.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/11.jpg)

At the age of 29, Siddhartha escaped from his palace, accompanied by Channa,
aboard his horse Kanthaka. He left his royal life to become a mendicant. It is
stated in the scriptures that, “…the horse’s hooves were muffled by the angels
in order to prevent guards from knowing that the Bodhisattva had departed.”
Before his departure, however, Mara (evil) appeared to him to try and stop him
from leaving, to which Siddhartha replied –“You are a wicked tempter. I will
cross endless oceans to pursue my path. I will cross countless pits of fire
regardless of their size. Neither great evil nor great goodness (wealth) will
prohibit me from going.”

[![12](/sites/default/files/Vibhor/12.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/12.jpg)

Siddhartha left Kapilavastu that night and on the banks of the Anoma River cut
his hair and took on the life of an ascetic. Siddhartha then practiced a way
of liberation under the institutes of two hermit teachers – Alara Kalama and
Udaka Ramaputta, but, although he achieved higher levels of meditative
consciousness, he was still not satisfied with his path. And, so, he moved on.

[![13](/sites/default/files/Vibhor/13.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/13.jpg)

Siddhartha then went on to Rajagaha, the capital of Magadha, and began his
ascetic life by begging for alms in the streets. In Rajagaha, Siddhartha
joined five companions headed by Kondanna who was the Brahmin who had stated
that Siddhartha would become a great holy man but not a King. They tried to
find enlightenment through near total deprivation of worldly goods – including
food and practicing self‐mortification. After nearly starving himself to death
by restricting his food he began to look like a skeleton.

[![14](/sites/default/files/Vibhor/14.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/14.jpg)

One day he collapsed in a river while bathing and almost drowned, it was at
this point that he began to reconsider his path. As he lay there, a boat
passed him and he overheard the conversation that two musicians were having
aboard – “If you tighten the string too tight it will snap, but if you loosen
it, it will not play.” And from this, he realized that he would have to take
“Middle Way” to reach enlightenment. True enlightenment could not be reached
by going to either extreme.

After many days, Siddhartha started to have some food as it was needed by his
body. On that day after having a nice bath in the Neranjara River, his body
was as bright as the full moon. Beside him stood a village woman name Sujata
who was thinking of fulfilling a vow of having a son and she had a golden tray
of rice porridge in her hands. After six years of asceticism and concentrating
on mortification, the great ascetic, Siddhartha, had discovered the path of
adequacy – a path of moderation away from the extremes of self‐indulgence and
self‐mortification. He accepted the rice porridge tray from Sujata who tried
to give him food all these six years. After this meal, his mind was free and
fresh – nothing is this world was a worry to him. He turned to the golden tray
and placed the tray into the tide of Neranjara and made a wish that if he
could become enlightened, the tray would float upstream. The tray floated
upstream and sunk, right to the bottom of the river where the other three
trays from the three previous Buddha’s had also made their wish. Naga, the big
serpent, was wakened from the sound of its splash, and said “Goodness, one
more Buddha has come for enlightenment”.

[![16](/sites/default/files/Vibhor/16.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/16.jpg)

Siddhartha turned to sit under a papal tree, now known as the Bodhi tree, and
vowed to sit under the tree until either one of two things happened: his body
would be decayed, dried or left only to tendons like a skeleton, or he would
arise as an enlightened being having found the Truth.

[![20](/sites/default/files/Vibhor/20.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/20.jpg)

He attained complete enlightenment on the sixth lunar month on the same day as
his birth, at the age of 35. From that day on, he was known as “The Buddha”,
“The Awakened One”, or “The Enlightened One”.

[![23](/sites/default/files/Vibhor/23.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/23.jpg)

For the remaining years of his life, Lord Buddha is said to have travelled all
of the regions in and around India – Bangladesh, Bihar, Pakistan, and Nepal –
to teach his doctrine and discipline to extremely diverse groups of people;
from nobles to outcast street sweepers, from mass murderers such as Angulimala
to cannibal ogres such as Alavaka. He also reached out to teach many adherents
of rival philosophies and religions. Lord Buddha founded the community of
Buddhist monks and female monks to continue the dissemination of the Dhamma to
the millions of people who lived in this area even after his Parinirvana or
“Complete Nirvana”. His religion was opened to all races and classes, and it
contained no caste structure.

[![27](/sites/default/files/Vibhor/27.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/27.jpg)

At his death, Lord Buddha told his disciples to follow no leader, but to
follow his teachings.

[![image](/sites/default/files/Vibhor/image.png)](https://vibhormahajan.files.wordpress.com/2012/05/image.png)

**I do not see any harm in the whole world.  
Hence, I do sleep  
With compassion for all living beings”.**

**– Lord Buddha**



Copyright (C) Vibhor Mahajan

