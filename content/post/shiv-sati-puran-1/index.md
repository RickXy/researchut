{
    "title": "The Holy Tale Of Sati – Shiva Puran (Part-I)",
    "date": "2016-01-11T05:05:57-05:00",
    "lastmod": "2016-01-11T05:05:57-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "sati"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/shiv-sati-puran-1"
}

[![mahashivaratri-2009-a](/sites/default/files/mahashivaratri-2009-a.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/mahashivaratri-2009-a.jpg)

Lord Brahma carried the grudge against Lord Shiva for rebuking him in his own
court before others during the Kama episode. He thought -  _what did prosaic
and ascetic like Lord Shiva know the power of passion and the force of
romantic feelings?_  Lord Shiva must experience it and know, he muttered. So,
Lord Brahma urged Kama to work his spell on Lord Shiva with the arrows having
flower heads blessed by him, the creator. To help Kama he created _Vasanta
Sena_ , the spring season of love, romance pollinated air, humming bees, birds
crooning mating calls, blooms etc. But they failed to arouse Lord Shiva.

Dismayed was Lord Brahma. Kama told him that Lord Shiva sat with burning eyes
totally unresponsive to their passion charges. He was scared of getting burnt
to ashes. Lord Brahma sighed unhappily.

The creator sought the help of Lord Vishnu who also wanted Lord Shiva to have
normal love life because his ascetic attitude and stern approach was not
helpful for sustenance. Lord Vishnu advised that Lord Brahma and Daksha may
invoke Lord Shiva's  _Maya_  aspect to seek her help. Accordingly Lord Brahma
asked Daksha to pray to  _Adi-Shakti (Mahamaya)_  of Lord Shiva to take
incarnation as his daughter. In answer to penance of Lord Brahma, Mahamaya
manifested before him. Lord Brahma asked her to help them arouse romantic
sentiment in Lord Shiva to have him desire for a wife. Mahamaya wondered if it
was Lord Shiva's will expressing itself through Lord Brahma. She promised to
try and materialised before Daksha who prayed her to incarnate as his
daughter. She granted his prayer.

Daksha joined his wife Veerni and copulated. In due course of time Mahamaya
arrived as their female child. They named her  _Sati_. From the very birth she
worshipped Lord Shiva, sang prayers and odes to him. When she grew up into her
teens, Lord Brahma went to her and reminded her of her mission of life and the
purpose of incarnation. Daksha also kept encouraging her in her endeavour to
win Lord Shiva.

Sati kept all fasts prescribed for Lord Shiva faithful's. She would perform
all Lord Shiva rites and obeyed the regimes of his worship.  _Nandivrata_  was
one of them. Then, she engaged herself in intense penance to propitiate her
idol, Lord Shiva.

Meanwhile, Lord Vishnu and Lord Brahma went to Kailasha to pray to Lord Shiva
to urge him to become a family man to inspire continuation of creation
process. Lord Shiva said as a divine Yogi, he always remained withdrawn into
deep meditation delinked from outside world. If he took a wife she must be
like a yogini who must only get into heat whenever rarely he felt amorous. The
visiting divinities gradually mentioned about the daughter of Daksha who had
all the qualities he was desiring and her undivided devotion to him.

Lord Shiva knew about the penance of Sati because he was feeling the heat of
her devotion. He materialised before her in his glorious masculine
resplendence. He revealed to her that he was ready to accept her as his wife.
He knew if it came to granting her a boon she would ask him to be her husband.

[![images](/sites/default/files/images.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/images.jpg)

Sati demanded that there union be properly solemnised in the presence of her
parents and all others with due traditions.

Lords Shiva and sati became husband wife in the grand marriage celebration and
rites performed on the 13th day of the ascending moon phase of Chaitra month
under Uttara Phalguni star. It was attended by all the deities, celestials,
holy men and all kinds of weird characters of the spirit world.

[![sati_and_shiva_indian-mythology](/sites/default/files
/sati_and_shiva_indian-
mythology.jpg)](https://vibhormahajan.files.wordpress.com/2012/07
/sati_and_shiva_indian-mythology.jpg)

During the ceremony Lord Brahma committed yet another outrage. As Lord Shiva
and Sati were going around the holy fire for seven rounds (Saptapadi) Lord
Brahma caught the sight of incredible pretty feet of Sati. Out of curiosity he
cast a glance at the face of the owner of those feet. The facial beauty of
Sati so sexually titillated Lord Brahma that his male organ could not help
shedding four drops of semen. Lord Shiva stared at the red face of Lord Brahma
sensing what he had done. He raised his trident to punish Lord Brahma.

[![shiva](/sites/default/files/shiva.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/shiva.jpg)

At this sudden development all those present at the ceremony got struck with
fear and shock. Most of them were baffled. Daksha and some sages rushed to
Lord Shiva to calm him down who looked in raging fury. Lord Vishnu also ran to
deity supreme. The sight of him appeared to cool down the temper of Lord
Shiva. Meanwhile, Lord Brahma prayed for mercy.

Lord Shiva made Lord Brahma touch his forehead and there appeared a tattoo of
Nandi Bull borne face of Lord Shiva. Lord Brahma was asked to make long
penance. Whenever anyone questioned him about the tattoo, Lord Brahma was to
recount true episode behind it and his own shameful role in it. Every ridicule
would lessen the burden of his sin, Lord Shiva proclaimed.

When the ceremonies were over Lord Shiva felt sorry for Lord Brahma. To
assuage his feelings he asked Lord Brahma to seek a boon. Lord Brahma asked
for that very venue to be made a holy and sacred spot as  _' Kalyana Mandap'_.
Lord Shiva obliged and said any faithful visiting that spot on the holy
wedding day to worship each year shall earn his blessings and get freed of all
mundane wishes.

[![6163150583_16a1acc069_z](/sites/default/files/6163150583_16a1acc069_z.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/6163150583_16a1acc069_z.jpg)

"This episode is known as  _Dakshayani - Kalyana_, the marriage of daughter of
Daksha. Bringing this episode to mind of anyone would gain the grace of deity
supreme and a wife as blessing. A faithful woman hearing the recital of this
_Katha_  will beget a son."

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

