{
    "title": "Lord Vishnu as Vamana and Birth of Goddess Ganga",
    "date": "2016-04-02T11:53:36-04:00",
    "lastmod": "2016-04-02T11:53:36-04:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "vishnu",
        "ganga"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/vishnu-vamana-birth-of-ganga"
}

# Lord Vishnu as Vamana and Birth of Goddess Ganga

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on [MAY
11, 2012](https://vibhormahajan.wordpress.com/2012/05/11/lord-vishnu-as-
vamana-and-birth-of-goddess-ganga/)

[![vishnuvarma](/sites/default/files/Vibhor/vishnuvarma.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/vishnuvarma.jpg)

  
Bali Chakravarthi was one of the greatest kings. Though he was a demon by
birth, he never deviated from the path of Truth and Dharma (religious ethics).
His power and strength kept on increasing as he has been following Dharma.
With the help and advice of his preceptor, King Bali had conquered all the
three worlds, and dethroned Indra from Heaven and occupied Amaravathi, the
capital of Indralok.

Indra got scared and went to Lord Vishnu for help. Lord Vishnu incarnated as
the dwarf Vamana and tricked Bali to grant him as much of his kingdom as he
could measure in 3 steps.

With the first step he covered all of Earth. With the second step he covered
all the heavens. But there was no place for the third step. Then Vamana
questions Bali where he could place his third foot? King Bali realized that he
is short of fulfilling his promise given to Vamana. He surrenders completely
before the Lord, and bends his head where the third foot could be placed to
fulfill his promise, pushing Bali to the underworld.

With the second step, when Vamana covered all the heavens, Lord Brahma washed
his feet in his kamandula (Water pot). When water from Kamandula touched the
feet of Lord Vishnu as Vamana, Ganga was born from the water in kamandula.
Ganga was very pretty and danced in the heavens to the delight of all.

One time however, when a gust of wind blew off the only cloth in which the
Rishi Durvasa was wrapped, Ganga laughed too much. The rishi cursed Ganga to
become reincarnated as a river in which all humans would seek purification.

