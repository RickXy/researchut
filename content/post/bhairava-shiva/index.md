{
    "title": "Bhairava – A Fierce Manifestation of Lord Shiva",
    "date": "2016-01-13T06:00:18-05:00",
    "lastmod": "2016-01-13T06:00:18-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "shiv",
        "bhairava"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/bhairava-shiva"
}

[![143_15](/sites/default/files/143_15.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/143_15.jpg)

Bhairava is the fierce manifestation of Lord Shiva associated with total
destruction.

He is ornamented with a range of twisted serpents (as earrings), bracelets,
anklets, and sacred thread (Yajnopavita). He wears a tiger skin and a ritual
apron composed of human bones.

Bhairava himself has eight manifestations: Kala Bhairava, Asitanga Bhairava,
Samhara Bhairava, Ruru Bhairava, Krodha Bhairava, Kapala Bhairava, Rudra
Bhirava and Unmatta Bhairava.

  *  **Origin Of Bhairava -**

[![url78_thumb25](/sites/default/files/url78_thumb25.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/url78_thumb25.jpg)

Once while travelling around the universe, Lord Brahma reached the abode of
Lord Vishnu. He saw Lord Vishnu resting on Adi-Anant-Shesha and being attended
by Garuda and other attendants.

 [![lord-vishnu-3r](/sites/default/files/lord-vishnu-
3r.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/lord-vishnu-3r.jpg)

As Lord Brahma entered, Lord Vishnu did not get up to receive him. This act of
Lord Vishnu got him angry. Very soon, verbal dual erupted between them to
determine who is superior. It became so severe that a battle was fought
between them, which continued for very long time.

All the deities became very worried when they saw no sign of battle coming to
an end. They decided to go to Lord Shiva, to seek his help.

[![Lord-Shiva \(1\)](/sites/default/files/lord-
shiva-1.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/lord-
shiva-1.jpg)

When Lord Shiva reached there, he manifested himself in the form of
'Analstamba' (Pillar of fire) between them. Lord Brahma and Lord Vishnu were
very surprised to see the pillar of fire, which reached the sky and penetrated
down the Earth.

Lord Vishnu transformed himself into a boar and went to the 'Patal'
(Underworld) to find the base of that 'Pillar of fire'. But he was
unsuccessful in his attempt and came back. Similarly Lord Brahma transformed
himself into a swan and flew up in the sky to find its limit. While going
through the aerial route he met a withered 'Ketaki' flower, who told Lord
Brahma that he had been present there since the beginning of the creation, but
was unable to know about the origin of that 'Pillar of fire'. Lord Brahma then
sought his help to give a false witness before Lord Vishnu, that he (Lord
Brahma) had been successful in seeing the limit of that pillar of fire. Then,
Lord Brahma told Lord Vishnu that he had seen the limit of that Pillar of
fire, Ketaki flower gave a witness. Lord Vishnu accepted the superiority of
Lord Brahma.

[![rudrashtakam-of-lord-rudra-shiva](/sites/default/files/rudrashtakam-of-
lord-rudra-shiva.jpg)](https://vibhormahajan.files.wordpress.com/2012/06
/rudrashtakam-of-lord-rudra-shiva.jpg)

Lord Shiva became very angry with Lord Brahma. Overflowing with anger, Lord
Shiva opened his third eye and from it manifested 'Bhairava'. Addressing him
as Kala-Bhairava (Lord of Time-Death) as he shone like the God of Death, Lord
Shiva said- "You are called Bhairava because you are of terrifying features
and are capable of supporting the universe. You are called Kala-Bhairava, for
even Time-Death is terrified of you." Lord Shiva ordered him to punish Lord
Brahma, promising him in return eternal suzerainty over his city of Kashi
(Varanasi). In a short period of time, Bhairava ripped off Lord Brahma's
guilty head with the nail of his left thumb. Seeing this, the terrified Lord
Vishnu eulogized Lord Shiva and devotedly recited his sacred hymns, followed
in this by the repentant Lord Brahma.  [![Shiva-comic-
virgin_thumb1](/sites/default/files/shiva-comic-
virgin_thumb1.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/shiva-
comic-virgin_thumb1.jpg)

The severed head immediately stuck to Bhairava's hand, where it remained in
the form of the skull, destined to serve as his insatiable begging-bowl.
Enjoining him to honour Lord Vishnu and Lord Brahma, Lord Shiva then directed
Bhairava to roam the world in this beggarly condition to atone for the sin of
Brahmahatya (Killing of a Brahman).

[![ts](/sites/default/files/ts.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/ts.jpg)

Lord Shiva said - "Show to the world the rite of expiation for removing the
sin of Brahmahatya. Beg for alms by resorting to the penitential rite of the
skull (kapalavrata)." Creating a maiden renowned as Brahmahatya, Lord Shiva
instructed her to relentlessly follow Bhairava everywhere until he reached the
holy city of Kashi to which she would have no access.

Observing the Kapalika rite with skull in hand and pursued by the terrible
Brahmahatya, Bhairava sported freely, laughing, singing and dancing with his
goblin horde (pramathas). As he passed through the Daru forest, the erotic
ascetic arrived at Lord Vishnu's door to seek redemption only to find his
entry barred by the guard, Vishvaksena. Spearing the latter and heaving the
corpse of this Brahman on his shoulder, he pressed before Lord Vishnu with
outstretched begging-bowl. Lord Vishnu split his own forehead-vein but the
out-flowing blood as a suitable offering, but could not fill the skull though
it flowed for eons. When Lord Vishnu then tried to dissuade Brahmahatya from
tormenting Bhairava, the criminal observed that "Beggars are not intoxicated
by the alms they receive as by drinking the wine of worldly honour." Before
leaving joyously to beg elsewhere, Bhairava reciprocated by recognizing Lord
Vishnu as his foremost disciple and acknowledged the latter's status
as"Grantor of boons to all the Gods". On arriving at Kashi, the skull fell on
the ground, freeing Bhairava from his sin and Brahmahatya sank into the
nether-world.

 **" Har Har Mahadev"**



 **Copyright (C) VIbhor Mahajan**

