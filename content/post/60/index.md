{
    "title": "Microsoft Wireless Mouse",
    "date": "2007-11-12T07:11:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Technology"
    ]
}

Today I also bought a [Microsoft Wireless Notebook Optical Mouse
3000](http://www.microsoft.com/hardware/mouseandkeyboard/productdetails.aspx?pid=064).
She is small, sleek and sexy.

And the good thing is that she **Just Mates** with Linux.

Here's what she speaks to her buddy:

input: Microsoft Microsoft Wireless Optical Mouseï¿½ 1.00 as
/class/input/input14  
input: USB HID v1.11 Mouse [Microsoft Microsoft Wireless Optical Mouseï¿½
1.00] on usb-0000:00:1d.7-2.1

