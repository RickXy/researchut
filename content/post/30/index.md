{
    "title": "Operation Modes in a [Linux] Distribution",
    "date": "2009-02-11T01:07:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Computing"
    ]
}

IMO, Linux Distributors should seriously think about Role Based Operations for
their distributions.

Every linux user is going to be using his copy of the distribution in a
different way. But from the distributor's point of view, it should boil down
to:

>  Desktop Users

>  Laptop Users

>  Server Users

Currently, some of the distributions apply this thought only to packages. For
example, Ubuntu rolls out a desktop flavor and a server (generic) flavor of
its kernel package. I think the distributions need to think broad and realize
that NOT One Size Fits All.

Currently, In Debian, the debian installer can detect what kind of hardware
you are installing it onto and come up with a base installation proposal. d-i
should extend this to configuration modes. For example, if the installation is
being done on a laptop, as many as possible services should be configured to
start from inetd and not as an ever running daemon.

Here's an example. I love Deiban.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)
They package the entire set of dictionaries. I love to have a handy copy of
all the dictionaries in just a couple of megs. And I don't have to worry about
the dictionaries being outdated. As soon as a new udpate is out, Debian gets
it.

Here's where I see the problem. The dictionary service is something I use in a
very limited fashion. Maybe once a day? But still, that _once a day_ usage is
important to me.

But this is what my system reports with dictd running:

> rrs@champaran:~$ cat /tmp/dictd.memstat | grep dict  
>     1392k: PID  9665 (/usr/share/dictd/bouvier.dict.dz)  
>      176k: /usr/sbin/dictd 9665  
>     2340k: /usr/share/dictd/bouvier.dict.dz 9665  
>      132k: /usr/share/dictd/bouvier.index 9665  
>      196k: /usr/share/dictd/vera.dict.dz 9665  
>      128k: /usr/share/dictd/vera.index 9665  
>      292k: /usr/share/dictd/foldoc.index 9665  
>     2148k: /usr/share/dictd/foldoc.dict.dz 9665  
>     1064k: /usr/share/dictd/freedict-eng-hin.dict.dz 9665  
>      420k: /usr/share/dictd/freedict-eng-hin.index 9665  
>     1064k: /usr/share/dictd/freedict-hin-eng.dict.dz 9665  
>     1228k: /usr/share/dictd/freedict-hin-eng.index 9665  
>    13212k: /usr/share/dictd/gcide.dict.dz 9665  
>     3860k: /usr/share/dictd/gcide.index 9665  
>      560k: /usr/share/dictd/jargon.dict.dz 9665  
>       44k: /usr/share/dictd/jargon.index 9665  
>    10508k: /usr/share/dictd/moby-thesaurus.dict.dz 9665  
>      532k: /usr/share/dictd/moby-thesaurus.index 9665  
>     9248k: /usr/share/dictd/wn.dict.dz 9665  
>     3004k: /usr/share/dictd/wn.index 9665  
>  \--------  
>   164444k  
>



Wow!!! That's a good chunk of memory that my dictionary service is using even
while I use this service just once a day.



And here's what the output is when I disable my dictd and configure it to run
from inetd.



> rrs@champaran:~$ cat /tmp/inetd.memstat  
>  
>  \--------  
>   109920k

So that brings us to a saving of (164444KiB - 109920KiB) 54524KiB, roughly
around 50 MiB of RAM.
![:-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_01.gif)
Sweet.

Point to note is that this is just one service. There can be many more like
this. Potential candidate that I can think of right now, from the ones that
are installed on my box, are CUPS, Mailserver et cetera.

IMO, it is not just Code Optimization that helps. Some thoughts like these can
also help us in many ways.

A lot of people talk about power savings, just think, how much battery power
can be conserved with these services were running through inet than always
running.



>  

>  

