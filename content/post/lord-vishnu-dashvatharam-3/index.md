{
    "title": "Lord Vishnu – Dashavatharam (Part – III)",
    "date": "2016-02-25T06:16:17-05:00",
    "lastmod": "2016-02-25T06:16:17-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "vishnu"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/lord-vishnu-dashvatharam-3"
}

## **Vamana Avatara  (Dwarf Incarnation)**

[![vishnuvarma_thumb](/sites/default/files/Vibhor/vishnuvarma_thumb1.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/vishnuvarma_thumb1.jpg)

  
Prahlada’s grandson was Bali Chakravarthi and he was one of the greatest
kings. Though he was a demon by birth, he never deviated from the path of
Truth and Dharma (religious ethics). His power and strength kept on increasing
as he has been following Dharma. With the help and advice of his preceptor,
King Bali had conquered all the three worlds, and dethroned Indra from Heaven
and occupied Amaravathi, the capital of Indralok.

Indra got scared and went to Lord Vishnu for help. Lord Vishnu incarnated as
the dwarf Vamana and tricked Bali to grant him as much of his kingdom as he
could measure in 3 steps.

With the first step he covered all of Earth. With the second step he covered
all the heavens. But there was no place for the third step. Then Vamana
questions Bali where he could place his third foot? King Bali realized that he
is short of fulfilling his promise given to Vamana. He surrenders completely
before the Lord, and bends his head where the third foot could be placed to
fulfill his promise, pushing Bali to the underworld.

**6)  Lord Parashuram**

[![1335015774_parsuram1](/sites/default/files/Vibhor/1335015774_parsuram1.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/1335015774_parsuram1.jpg)

The Kshatriyas (Warriors) were the second of the four classes. It was their
job to wear arms and protect the world and rule. The Brahmans (Sages) were the
first of the four classes. It was their job to pray, study the sacred texts
and perform religious rites. But the Kshatriyas became very insolent and began
to oppress the world and the Brahmans. Lord Vishnu was then born as the son of
the sage Jamadagni and his wife Renuka, named Parshuram. Parshu means axe,
hence his name literally means Ram-with-the-axe. He received an axe after
undertaking a terrible penance to please Lord Shiva, from whom he learned the
methods of warfare and other skills. Even though he was born as a Brahmin, he
had Kshatriya (warrior) traits in terms of aggression, warfare and valour.
Since this was the line of the sage Bhrigu, Parashurama was also called
Bhargava. Lord Parashuram’s mission was to protect the Brahmans and teach a
lesson to the Kshatriyas. There was a king named Kartavirya who had received
all sorts of boons from the sage Dattatreya. With these boons, Kartavirya had
a thousand arms; he conquered and ruled over the entire world. One day,
Kartavirya went on a hunt to the forest. He was very tired after the hunt and
was invited by the sage Jamadagni. He had a kamadhenu cow; which means that
the cow produced whatever its owner desired. Jamadagni used the kamadhenu to
treat Kartavirya and all his soldiers to a sumptuous feast. Kartavirya was so
enamoured of the kamadhenu that he asked the sage to give it to him, but
Jamadagni refused. Kartavirya then abducted the cow by force and a war started
between Kartavirya and Lord Parashuram. In this war, Lord Parashuram cut off
Kartavirya’s head with his axe (parashu) and brought the kamadhenu back to the
hermitage. After some time, Lord Parashuram was away when Kartavirya’s sons
arrived at the ashram and killed Jamadagni. On the death of his father, Lord
Parashuram’s anger was aroused. He killed all the Kshatriyas in the world
twenty-one times. On the plains of Kurukshetra, he built five wells which were
filled with the blood of Kshatriyas.

He is a Chiranjeevi (Immortal) who fought the advancing ocean back, thus
saving the lands of Konkan and Malabar. He has been a Guru to Bhishma,
Dronacharya and later also to Karna. He taught Karna the extremely powerful
Brahmastra (a celestial weapon).

Lord Parshuram lives on earth even today as he is immortal. The Kalki Purana
states that Lord Parshuram will be the martial guru of Sri Kalki, the tenth
and final Incarnation of Lord Vishnu. It is he who instructs Kalki to perform
a long penance onto Shiva to receive celestial weaponry



## Copyright (C) Vibhor Mahajan

