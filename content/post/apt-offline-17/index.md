{
    "title": "apt-offline 1.7",
    "date": "2015-11-11T07:29:29-05:00",
    "lastmod": "2015-11-11T07:32:06-05:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "Offline APT Package Manager"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/apt-offline-17"
}

Hello World,

![](/sites/default/files/1280px-The_Rangoli_of_Lights.jpg)

In this part of the world, today is a great day. Today is _**[Diwali - the
festival of lights](https://en.wikipedia.org/wiki/Diwali)**_

On this day, I am very happy to announce the release of **apt-offline** ,
version **1.7**. This release brings in a large number of fixes and is a
recommended update. Thanks to **Bernd Dietzel** for uncovering the shell
injection bug which could be exploited by carefully crafting the signature
file. Since apt-offline could be run as  'root', this one was an important
bug. Also thanks to him for the fix.

During my tests, I also realized that apt-offline's _\--install-src-packages_
implementation had broken over time. _\--install-src-packages_ option can be
useful to users who would like the offline ability to download a [Debian]
**source package** , along with all its **Build Dependencies**.

For further details on many of the other fixes, please refer to the git
repository at the homepage. Packages for Debian (and derivatives) are already
in the queue.

Wishing You and Your Loved Ones a Very Happy Diwali.

