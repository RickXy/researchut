{
    "title": "New GPG Key",
    "date": "2009-08-06T19:06:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "f00a2be6",
        "gpg",
        "gnupg"
    ],
    "categories": [
        "Debian-Pages",
        "General"
    ]
}

Given the recent switch to a stronger key by everyone, here's mine.

> The old key that I'll still keep using till I get the new key signed by a
good amount of people.

>

> **pub    1024D/04F130BC 2003-08-18  
>        Key fingerprint = CF0F EDEF 1052 83D2 62A4  0549 E118 62EA 04F1 30BC  
>  uid                  Ritesh Raj Sarraf <rrs@researchut.com>  
>  uid                  Ritesh Raj Sarraf <riteshsarraf@users.sourceforge.net>  
>  uid                  Ritesh Raj Sarraf (NetApp) <rsarraf@netapp.com>  
>  uid                  [jpeg image of size 52128]  
>  sub   1024g/A876BF8F 2003-08-18  
>  sub   4096R/5D93A273 2009-04-25  
>  sub   4096R/7FBB6077 2009-04-25**

>

>  And here's the new key, that's been duly signed with the above old key.

>

> **pub    4096R/F00A2BE6 2009-08-06  
>        Key fingerprint = 43DE F582 F9E6 7111 CE00  8917 F2F1 1C23 F00A 2BE6  
>  uid                  Ritesh Raj Sarraf <rsarraf@netapp.com>  
>  uid                  Ritesh Raj Sarraf <rrs@researchut.com>  
>  uid                  [jpeg image of size 52128]  
>  sub   4096R/0B488290 2009-08-06**  
>  

