{
    "title": "Google Take Action - Encryption",
    "date": "2015-09-12T07:15:11-04:00",
    "lastmod": "2015-09-12T07:15:11-04:00",
    "draft": "false",
    "tags": [
        "Google",
        "Encryption",
        "Privacy",
        "Cloud"
    ],
    "categories": [
        "Debian-Blog",
        "Rant"
    ],
    "url": "/blog/google-take-action-encryption"
}

So I got an email yesterday from Google's Jess Hemerly, talking about how they
[care about encryption](https://www.google.com/takeaction/issue/encryption/)

To quote  a small snippet of the email:

    
    
    We use locks to keep our homes and our possessions safe offline. But how can we protect our digital things, like photos on a smartphone or email traveling across the web?  **[The answer is encryption.](https://takeaction.withgoogle.com/page/m/1439db80/19cdc98/22666ed/3ed584ff/875135400/VEsE/ "http://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww%2Efacebook%2Ecom%2FGoogleTakeAction%2Fvideos%2Fvb%2E924932914232399%2F939211039471253%2F&utm_medium=email&utm_source=google&utm_content=2+-+The+answer+is+encryption&utm_campaign=20150910encryption1pm&source=20150910encryption1pm")**

To change, it has to start from home. That means, add encryption support to
Gmail. Add encryption to Google Photos. To every service you provide. So that,
if tomorrow, your service is compromised, you don't public our data.

What all the service providers need to realize, is that it is not the tool
that needs encryption, but the data. You could claim that newer Androids are
safer because encryption is enabled by default. But the data that you are
backing up on your cloud, is not encrypted. It only makes sense to have an
end-to-end encryption, or rather, encryption at source.

Had you just stacked the interface to have write() transparently call
encrypt() for any and all data, there'd be no worry about privacy. But wait,
then how'd you munch.

