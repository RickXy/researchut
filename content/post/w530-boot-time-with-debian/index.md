{
    "title": "Debian Boot time",
    "date": "2012-10-20T14:38:13-04:00",
    "lastmod": "2013-01-31T13:43:40-05:00",
    "draft": "false",
    "tags": [
        "systemd",
        "btrfs",
        "boot",
        "w530"
    ],
    "categories": [
        "Debian-Pages",
        "Debian-Blog"
    ],
    "url": "/blog/w530-boot-time-with-debian"
}

In case, the video doesn't show on the page,
http://www.youtube.com/watch?v=cTK3aIZbe2A This blog post is to show-off the
impressive performance I saw with my machine.

I recently switched to a ThinkPad W530 laptop. It is a fairly recent machine
with the following hardware config:

  * Intel(R) Core(TM) i7-3720QM CPU @ 2.60GHz
  * 8 GiB RAM
  * nVIDIA Optimus
  * Samsung SSD Drive

On the software front, I decided to take my chances. Hence:

  * BTRFS File System
  * SystemD Init

The rest is in the video. It is impressive to see how drastically the
experience has changed with this combination.

From my limited time spent exploring the machine, all credit goes to the SSD
technology.

