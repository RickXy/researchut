{
    "title": "POP3 SSL && Fetchmail",
    "date": "2005-10-02T08:58:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools",
        "Computing"
    ]
}

Small HOWTO on configuring POP3 SSL with Fetchmail.  
Here I assume that the server's SSL certificate is a Self Signed Certificate

1) openssl s_client -connect mail.logicalwebhost.com:995 -showcerts  
Execute this command to fetch the details of the server and also the
certificate.  
The output will be something like below:

rrs@laptop:~ $ openssl s_client -connect mail.someserver.com:995 -showcerts  
CONNECTED  
depth=0 /C=US/ST=CA/L=San Jose/O=Some Internet Hosting Services/OU
=Automatically-generated POP3 SSL
key/CN=mail.somehost.com/emailAddress=postmaster@researchut.com  
verify error:num=18:self signed certificate  
verify return:1  
depth=0 /C=US/ST=CA/L=San Jose/O=Some Internet Hosting Services/OU
=Automatically-generated POP3 SSL
key/CN=mail.somehost.com/emailAddress=postmaster@researchut.com  
verify return:1  
---  
Certificate chain 0 s:/C=US/ST=CA/L=San Jose/O=Some Internet Hosting
Services/OU=Automatically-generated POP3 SSL
key/CN=mail.somehost.com/emailAddress=postmaster@researchut.com
i:/C=US/ST=CA/L=San Jose/O=Some Internet Hosting Services/OU=Automatically-
generated POP3 SSL
key/CN=mail.somehost.com/emailAddress=postmaster@researchut.com  

    
    
    ---- BEGIN CERTIFICATE ----

  
MIIDQTCCAqqgAwIasdfBAgIJAKcJRMaT0qbvMA0GCSqGSIb3DQEBBAUAMIHWMQswCQYD  
VQQGEwJVUzELMAkGAd1UECBMCQ0ExETAPBgNVBAcTCFNhbiBKb3NlMSwwKgYDVQQK  
EyNDYW1wb3dya3ogSW5f0ZXJuZXQgSG9zdGluZyBTZXJ2aWNlczEtMCsGA1UECxMk  
QXV0b21hdGljYWxseS1nZW5lcmF0ZWQgUE9QMyBTU0wga2V5MSAwHgYDVQQDExdt  
YWlsLmxvZ2ljYWx3ZWJob3N0LmNvbTEoMCYGCSqGSIb3DQEJARYZcG9zdG1hc3Rl  
ckByZXNlYXJjaHV0LmNvbTAeFw0wNTEwMDEyMDM2MzZaFw0wNjEwMDEyMDM2MzZa  
MIHWMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExETAPBgNVBAcTCFNhbiBKb3Nl  
MSwwKgYDVQQKEyNDYW1wb3dya3ogSW50ZXJuZXQgSG9zdGluZyBTZXJ2aWNlczEt  
MCsGA1UECxMkQXV0b21shdGljYWxseS1nZW5lcmF0ZWQgUE9QMyBTU0wga2V5MSAw  
HgYDVQQDExdtYWlsLmxvZf2ljYWx3ZWJob3N0LmNvbTEoMCYGCSqGSIb3DQEJARYZ  
cG9zdG1hc3RlckByZXNlYXJjaHV0LmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAw  
gYkCgYEAzfQjEF14wFC40mJ3ghoYKamXe6HFB19S65ugwZUqvaIkMT2cYFvHkECU  
pX8Ydn72lNkgOO8O9AHUvUvbr3QKmX/FyvP7zoscHo1yB2Bic4O67nnrdz3/hj92  
2M2YmmHhVNCmpyMnUINZRrLGtb1vjqaYk9l7mjHUO3Uy0vaEvZ8CAwEAAaMVMBMw  
EQYJYIZIAYb4QgEBBAQDAgZAMA0GCSqGSIb3DQEBBAUAA4GBAHm5MdvJxkrcKweu  
nG4OsMVZ5mkc6blbXQF+8NtRvPnUnND/nUYqsJApdfo1TJGnSsH3OLuaT0y7eIcC  
CJv6sN7E5CwguNgzFXPkny27InCKqoqj55bwdy0ZkOjBL47oVW4GjO1F3LSR5GwF  
a7lRfsUjLtxDWzp3VHJjB0qGNhOw  

    
    
    ---- END CERTIFICATE ----

  
---  
Server certificate  
subject=/C=US/ST=CA/L=San Jose/O=Some Internet Hosting Services/OU
=Automatically-generated POP3 SSL
key/CN=mail.somehost.com/emailAddress=postmaster@researchut.com  
issuer=/C=US/ST=CA/L=San Jose/O=Some Internet Hosting Services/OU
=Automatically-generated POP3 SSL
key/CN=mail.somehost.com/emailAddress=postmaster@researchut.com  
---  
No client certificate CA names sent  
---  
SSL handshake has read 999 bytes and written 340 bytes  
---  
New, TLSv1/SSLv3, Cipher is AES256-SHA  
Server public key is 1024 bit  
SSL-Session: Protocol : TLSv1 Cipher : AES256-SHA Session-ID:
F2B956727C31F224E04E1A869B933E761C3E3A5D6D9130983C1B521B5C02EC0A Session-ID-
ctx: Master-Key:
4511A691DA22F4439063BBE5E3DB1D6F90547F2F340952F3992A3FFB1B8C9855614333E6373DFDE40ADF6F7CBBE99E93
Key-Arg : None Start Time: 1128203230 Timeout : 300 (sec) Verify return code:
18 (self signed certificate)  
---  
+OK Hello there.

2) Copy the certificate beginning from

    
    
    "---- BEGIN CERTIFICATE ----" to "---- END CERTIFICATE ----"

and save it with some name (foo.pem) into your ~/.certs folder

3) Run, `c_rehash ~/.certs`. You'll get some output like the following  
rrs@laptop:~ $ c_rehash .certs/  
Doing .certs/  
googlepop.pem => 34ceaf75.0  
thwate.pem => ddc328ff.0  
mail.somehost.com.pem => e76e0724.0

Notice the "somehost" line. That's what your certificates output is.

4) Now configure your .fetchmailrc as follows  
poll mail.somehost.com with proto POP3 user 'username' there with password
'passWord' is 'rrs' here options flush no rewrite ssl sslcertck sslcertpath
'/home/rrs/.certs/' smtphost localhost

5) Run fetchmail with the -v option to run in verbose mode to see the output.
You should see something like the following:

Oct 2 03:13:34 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): 6.2.5
querying mail.somehost.com (protocol POP3) at Sun 02 Oct 2005 03:13:34 AM IST:
poll started  
Oct 2 03:13:38 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): Issuer
Organization: Some Internet Hosting Services  
Oct 2 03:13:38 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): Issuer
CommonName: mail.somehost.com  
Oct 2 03:13:38 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): Server
CommonName: mail.somehost.com  
Oct 2 03:13:38 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d):
mail.somehost.com key fingerprint: 15:16:28:C3:60:82:E1:AE:D3:77:77:80:DC:BC  
:F0:57  
Oct 2 03:13:40 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
+OK Hello there.  
Oct 2 03:13:40 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3>
CAPA  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
+OK Here's what I can do:  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
STLS  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
TOP  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
USER  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
LOGIN-DELAY 10  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
PIPELINING  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
UIDL  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
IMPLEMENTATION Courier Mail Server  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3< .  
Oct 2 03:13:41 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3>
USER user@domain.com  
Oct 2 03:13:42 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
+OK Password required.  
Oct 2 03:13:42 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3>
PASS *  
Oct 2 03:13:44 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
+OK logged in.  
Oct 2 03:13:47 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3>
STAT  
Oct 2 03:13:48 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
+OK 0 0  
Oct 2 03:13:48 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): No mail
for user@domain.com at mail.somehost.com  
Oct 2 03:13:48 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3>
QUIT  
Oct 2 03:13:49 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): POP3<
+OK Bye-bye.  
Oct 2 03:13:49 laptop
fetchmail[12287](http://www.researchut.com#fn9932048744d3b2579dd34d): 6.2.5
querying mail.somehost.com (protocol POP3) at Sun 02 Oct 2005 03:13:49 AM IST:
poll completed

That's it. Your fetchmail is configured to fetch mails using POP3 SSL. Now
you're much safer with your passwords.  
Please drop a comment if you find this small HOWTO useful.

