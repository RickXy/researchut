{
    "title": "Device Mapper Multipath status in Debian",
    "date": "2015-12-29T08:28:41-05:00",
    "lastmod": "2015-12-29T08:28:41-05:00",
    "draft": "false",
    "tags": [
        "iscsi",
        "multipath",
        "SAN",
        "systemd"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/san-setup-debian-next"
}

For Debian Jessie, the multipath support relied on sysvinit scripts. So, if
you were using systemd, the level of testing would have been minimal.

At DebConf15, I got to meet many people whom I'd worked with, over emails,
over the years. With every person, my ask was to use the SAN Storage stack in
a test environement, and report bugs early. Not after the next release. This
applies also to the usual downstream distribution projects.

That said, today, I spent time building a Root File System on SAN setup using
the following stack, of the versions that'd be part of the next stable
release:

  * Linux
  * Open-iSCSI Initiator
  * Device Mapper Multipath
  * LIO Target

I'm pretty happy that nothing much has changed in terms of setup, from what
has already been documented in README.Debian files. The systemd integration
has been very transparent.

But that is my first hand experience. I'm request all users of the above
mentioned stack to build the setup and report issues, if any. Please do not
wait for the last minute of the release/freeze.

    
    
    root@debian-sanboot:~# systemctl status -l multipath-tools
    
    ● multipathd.service - Device-Mapper Multipath Device Controller
    
       Loaded: loaded (/lib/systemd/system/multipathd.service; enabled; vendor preset: enabled)
    
       Active: active (running) since Tue 2015-12-29 18:38:58 IST; 1min 23s ago
    
      Process: 246 ExecStartPre=/sbin/modprobe dm-multipath (code=exited, status=0/SUCCESS)
    
     Main PID: 260 (multipathd)
    
       Status: "running"
    
       CGroup: /system.slice/multipathd.service
    
               └─260 /sbin/multipathd -d -s
    
    
    Dec 29 18:39:04 debian-sanboot multipathd[260]: sdb [8:16]: path added to devmap sanroot
    
    Dec 29 18:39:04 debian-sanboot multipathd[260]: sdc: add path (uevent)
    
    Dec 29 18:39:04 debian-sanboot multipathd[260]: sanroot: load table [0 16777216 multipath 0 0 3 1 service-time 0 1 1 8:16 1 service-time 0 1 1 8:0 1 service-time 0 1 1 8:32 1]
    
    Dec 29 18:39:04 debian-sanboot multipathd[260]: sdc [8:32]: path added to devmap sanroot
    
    Dec 29 18:39:04 debian-sanboot multipathd[260]: sdd: add path (uevent)
    
    Dec 29 18:39:04 debian-sanboot multipathd[260]: sanroot: load table [0 16777216 multipath 0 0 4 1 service-time 0 1 1 8:16 1 service-time 0 1 1 8:32 1 service-time 0 1 1 8:48 1 service-time 0 1 1 8:0 1]
    
    Dec 29 18:39:04 debian-sanboot multipathd[260]: sdd [8:48]: path added to devmap sanroot
    
    Dec 29 18:39:13 debian-sanboot multipathd[260]: sanroot: sda - directio checker reports path is up
    
    Dec 29 18:39:13 debian-sanboot multipathd[260]: 8:0: reinstated
    
    Dec 29 18:39:13 debian-sanboot multipathd[260]: sanroot: remaining active paths: 4



    
    
    root@debian-sanboot:~# multipath -ll
    
    sanroot (36001405ead943c8222140268e019ba49) dm-0 LIO-ORG,IBLOCK
    
    size=8.0G features='0' hwhandler='0' wp=rw
    
    |-+- policy='service-time 0' prio=1 status=active
    
    | `- 4:0:0:0 sdb 8:16 active ready running
    
    |-+- policy='service-time 0' prio=1 status=enabled
    
    | `- 3:0:0:0 sdc 8:32 active ready running
    
    |-+- policy='service-time 0' prio=1 status=enabled
    
    | `- 5:0:0:0 sdd 8:48 active ready running
    
    `-+- policy='service-time 0' prio=1 status=enabled
    
      `- 2:0:0:0 sda 8:0  active ready running
    
    



    
    
    root@debian-sanboot:~# iscsiadm -m session
    
    tcp: [1] 172.16.20.40:3260,1 iqn.2003-01.org.linux-iscsi.debian.sanboot (non-flash)
    
    tcp: [2] 172.16.20.41:3260,1 iqn.2003-01.org.linux-iscsi.debian.sanboot (non-flash)
    
    tcp: [3] 172.16.20.42:3260,1 iqn.2003-01.org.linux-iscsi.debian.sanboot (non-flash)
    
    tcp: [4] 172.16.20.43:3260,1 iqn.2003-01.org.linux-iscsi.debian.sanboot (non-flash)



    
    
    root@debian-sanboot:~# mount | grep sanroot
    
    /dev/mapper/sanroot on / type ext4 (rw,relatime,errors=remount-ro,stripe=8191,data=ordered)



