{
    "title": "New SSH Session in Konsole",
    "date": "2005-10-02T09:32:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools",
        "Computing"
    ]
}

Some quick steps to create an Option for "New SSH Session" under Konsole which
includes keychain and ssh-agent for password-less ssh logins.

![](http://www.researchut.com/blog/images/konsole-ssh-session_copy.jpg)

I'm using a Debian system but majority of the steps should be common across
all distributions.

1) Install keychain and openssl-client programs (ssh-agent is part of openssl-
client package under Debian)  
**rrs@laptop:~ $ apt-get install keychain openssl-client**

2) Add the following lines to your .bashrc file  

    
    
    # For SSH

  

    
    
    # For now I'm gambling the linux session for ssh-agent.

  

    
    
    # I'll have to find a better way in the future. 30/09/2005

  

    
    
    if [ $TERM == linux ]; then

  

    
    
      keychain ~/.ssh/id_dsa ~/.ssh/id_rsa --timeout 30

  

    
    
      [ -f $HOME/.keychain/$HOSTNAME-sh ] && source $HOME/.keychain/$HOSTNAME-sh

  

    
    
    fi

Yes, the comments are right. Right now there's a flaw in using the $TERM
variable because the default shell uses this variable. But that's a limitation
with konsole which you'll come to know after a couple of steps more. I've
filed a bug (# 113589) with the maintainer and hoping that he confirms the
wishlist. If you have a suggestion, please do put a comment or mail me.

3) Create the following file with the following contents

rrs@laptop:~ $ cat ~/.kde/share/apps/konsole/SSH.desktop  
[Desktop Entry]  
Comment=New SSH Console  
Cwd=  
Encoding=UTF-8  
Exec=bash  
Font=6  
Icon=openterm  
KeyTab=  
Name=SSH Console  
Schema=Linux.schema  
Term=linux  
Type=KonsoleApplication

That's it. Start a new Konsole and you should see the "New SSH Session" option
under the "Session" menu.

Coming back to the $TERM variable flaw. Konsole presently allows only one
variable to be exported from its "Session" configuration tab (Settings =>
Configure Konsole => Session). Since we can't have the contents in .bashrc
specific to ssh-agent and keychain executed by every session type and every
shell, it'd be dangerous to put them without a conditional check. But
presently, AFAIK, the only way to distinguish our sessiontype "New SSH
Session" from other sessions is by exporting the variable.  
So if the maintainer confirms the wishlist bug, we could very easily put a
check like:  
if [ $MY_VAR == NEW_SSH_SESSION_TYPE_KONSOLE]; then  
…execute ssh-agent && keychain specific commands  
fi

This way the SSH specific commands will be executed only within the "SSH
Session" type.

Please drop your comments if you have any suggestions.

