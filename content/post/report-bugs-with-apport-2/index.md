{
    "title": "Reporting bugs with Apport - II",
    "date": "2012-07-17T09:11:51-04:00",
    "lastmod": "2014-11-12T05:48:08-05:00",
    "draft": "false",
    "tags": [
        "apport",
        "debian"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/report-bugs-with-apport-2"
}

This is a follow up to the previous post regarding [Reporting bugs with
Apport](http://www.researchut.com/site/blog/report-bugs-with-apport)

Apport, version 2.2.3-2, has been pushed to experimental. There were some nice
feedback that led to some more changes that I will talk here.

 **Opt out:  **As a developer, if you see the volume of reports to be
annoying, you have the option to opt out of apport reports. To do this, you
should specify the **XBS-Apport: No** field in your package 's control file.
When your application crashes, apport first checks for that field in your
package's description and acts based on what you chose.

Following image is that the user sees for packages where the developer has
opted out.![opt out](/sites/default/files/apport%203.png)

 **Repetition:  **Repetition of the same crash could lead to multiple reports
on the same buggy behavior. For this, apport, if senses that you have already
filed a bug, it provides you with the option to further ignore all crashes for
that particular type.

Following image should explain it better.

![ignore report](/sites/default/files/apport%201.png)



My initial thought regarding defaults was to make apport a default opt out
tool for the entire archive and only file reports for packages where the
developer has manually enabled to opt-in for apport reports. But that'd have
been very slow in terms of adoption, hence, for now, instead, the version in
experimental does it the other way around. It will file bug reports for all
packages. If you need to opt out, you will have to add the above expalined
control field.

That's it for this post. Please provide feedback once you spend some time with
apport.

PS: Please take conscious steps when filing bug reports with apport. I think
it is a great tool. It just needs great users. :-)

