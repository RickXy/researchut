{
    "title": "An Eyeful a Day Keeps the Doctor Away",
    "date": "2005-09-05T05:28:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Fun"
    ]
}

Now I understand, why even though staring carelessly for the past 5 years, my
health has not gone bad.
![;-\)](http://www.researchut.com/blog/pivotx/includes/emoticons/trillian/e_121.gif)

**Staring at women 's breasts is good for men's health and makes them live
longer, a new survey reveals.**

![](http://www.researchut.com/blog/images/an_eyeful_a_day_keeps_the_doctor_away.jpg)

Staring at women's breasts is good for men's health and makes them live
longer, a new survey reveals. Researchers have discovered that a 10-minute
ogle at women's breasts is as healthy as half-an-hour in the gym. A five-year
study of 200 men found that those who enjoyed a longing look at busty beauties
had lower blood pressure, less heart disease and slower pulse rates compared
to those who did not get their daily eyeful.

Dr Karen Weatherby, who carried out the German study, wrote in The New England
Journal of Medicine: "Just 10 minutes of staring at the charms of a well-
endowed female is roughly equivalent to a 30-minute aerobics workout. Sexual
excitement gets the heart pumping and improves blood circulation. There is no
question that gazing at breasts makes men healthier. Our study indicates that
engaging in this activity a few minutes daily cuts the risk of a stroke and
heart attack in half. We believe that by doing so consistently, the average
man can extend his life 4 to 5 years."

She added that sexy stars like Dolly Parton, Heather Locklear, Anna Nicole
Smith and Demi Moore had proved to be especially good for the men's health.

