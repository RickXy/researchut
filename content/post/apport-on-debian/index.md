{
    "title": "Apport for Debian",
    "date": "2012-04-07T12:47:05-04:00",
    "lastmod": "2014-11-12T06:02:01-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "apport"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/apport-on-debian"
}

apport just cleared the new queue and is now available in
[experimental](http://packages.qa.debian.org/a/apport.html). For those who do
not know what apport is:

>  
>

> Debugging program crashes without any automated tools has been pretty time
consuming and hard for both developers and users. Many program crashes remain
unreported or unfixed because:

>

>   * Many crashes are not easily reproducible.

>   * End users do not know how to prepare a report that is really useful for
developers, like building a package with debug symbols, operating `gdb`, etc.

>

>   * A considerable part of bug triage is spent with collecting relevant
information about the crash itself, package versions, hardware architecture,
operating system version, etc.

>   * There is no easy frontend which allow users to submit detailed problem
reports.

>   * Existing solutions like bug-buddy or krash are specific to a particular
desktop environment, are nontrivial to adapt to the needs of a distribution
developer, do not work for crashes of background servers (like a database or
an email server), and do not integrate well with existing debug packages that
a distribution might provide.

>

>

> Apport is a system which

>

>   * intercepts crashes right when they happen the first time,

>   * gathers potentially useful information about the crash and the OS
environment,

>   * can be automatically invoked for unhandled exceptions in other
programming languages (e. g. in Ubuntu this is done for Python),

>   * can be automatically invoked for other problems that can be
automatically detected (e. g. Ubuntu automatically detects and reports package
installation/upgrade failures from update-manager),

>   * presents a UI that informs the user about the crash and instructs them
on how to proceed,

>   * and is able to file non-crash bug reports about software, so that
developers still get information about package versions, OS version etc.

>

At this moment, it is quite broken. The crashes are not intercepted by update-
notifier on my box. With it now in experimental, my intent is to slowly
integrate it well with all dependent tools for Debian. It won't be ready for
the Wheezy release, but hopefully for the one after that.

On the Ubuntu side, Canonical hosts a retracing service that takes user
reported core dumps and generates a usable backtrace. For Debian, my plan is
to have a chroot kind interface, where in the user could opt-in to download
all debug packages to generate a valid backtrace. This could go as the debian
backend for apport in the future. On the bug reporting side, afaik, we do not
have a web interface to lodge bugs. To report bugs (minus the core dumps), we
will want a backend to submit it over email. The current apport report
gathering tool is pretty good (if compared to other tools like reportbug), so
here the low hanging fruit would be to just take the report and feed in email
to our BTS server.



For the eyes:

![Apport on Debian](/sites/default/files/apport-debian.png)

