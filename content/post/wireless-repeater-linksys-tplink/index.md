{
    "title": "Unbricking my Linksys",
    "date": "2016-02-23T08:54:34-05:00",
    "lastmod": "2016-02-23T08:54:34-05:00",
    "draft": "false",
    "tags": [
        "linksys",
        "tplink",
        "repeater",
        "wifi",
        "ddwrt",
        "dd-wrt",
        "bridge"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/wireless-repeater-linksys-tplink"
}

In trying to improve the WiFi service in my house, I recently dug out my old
Linksys WRT54G Router. It is a 802.11 bg router, which can do 54 MBPS.
Currently, I use a TPLink based router, with a DD-WRT community firmware,
which claims is 802.11 bgn and can do 300 MBPS.  The overall wifi signal isn't
adequate, so I thought of using the old router as a repeater.

The default Linksys VXWorks firmware does not have the bridging capabilities,
so it was time to flashback, again, the dd-wrt image onto it (The last time I
did, I eventually had to revert back to the stock firmware because my ISP
claimed that the router was giving too many packet drops). Being an old
device, and me being lazy, I did not pay much attention to the exact steps;
and ended up bricking my router, wherein it wouldn't give me the **Management
Console** to flash a new firmware; nor would it give me the old **VXWorks**
firmware interface. All it gave was a White Screen of Death and an active IP
connection.

Turns out I'm not the only one bricking routers, so some good soul
[documented](http://www.makeuseof.com/tag/unbrick-trashed-linksys-router/) the
process on unbricking the router. I was in a slightly more better position
than the author, cause I did have the network available. My initial hope was
to just push the firmware image over tftp, and that it would work. But that
didn't happen.

So, I was forced to follow the steps in the article, i.e. to use the Windows
Tool mentioned in it (Infact that is what triggered me to write the article).
Not having Windows available, my choice was to try it out over Crossover/Wine.
It got the job done; so Thank You.

BTW, does anyone know what goodie does the Windows Tool do that plain tftp did
not ? The Windows Tool too pushed the image over tftp, so it must be doing
something within the tftp spec to trigger the firmware upload and flash.

![](/sites/default/files/Screenshot%20from%202016-02-23%2014-17-00.png)





That done, getting the **Repeater Bridge** wasn't that painful. If you build a
Repeater Bridge setup, just follow dd-wrt instructions. The only extra I'd add
is that **You should keep your Virtual AP's credentials the same as that of
your Primary Router's AP's.** I'm not sure why that is needed but that ate my
good 2-3 hrs.

