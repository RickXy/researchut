{
    "title": "Debian GNU/Hurd on VirtualBox",
    "date": "2015-02-01T05:51:20-05:00",
    "lastmod": "2015-02-01T05:51:20-05:00",
    "draft": "false",
    "tags": [
        "Hurd",
        "debian",
        "Mach",
        "VirtualBox",
        "VBox"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Technology",
        "Virtualization"
    ],
    "url": "/blog/debian-hurd-on-virtualbox"
}

One of the great things about Debian is the wide range of kernels it
~~supports~~ can run. This gives the user the flexibility to not spend time on
the common userland stuff. For example, most apps, package management and
system admin tasks are common across all Debian platforms.

These platforms may not be optimal at par to Linux, but still, **choice is
good**.

For long, I had used Debian GNU/Hurd, only on a KVM hypervisor. Recently being
involved in VirtualBox maintenance, I've almost beeing using VirtualBox for
all my virtualization tasks. So it was time to try out Hurd on VirtualBox.

All praises to the work the Debian Hurd team has put, it just works
wonderfully.

To try out Hurd on VirtualBox:

  1. You could download ready to use VirtualBox images from [here](https://people.debian.org/~sthibault/hurd-i386/).
  2. These are raw images. For VirtualBox, I'd recommend you convert the image to VDI format.
    1. 
        rrs@learner:~/VirtualBox VMs$ VBoxManage convertdd debian-hurd-20150105.img debian-hurd-20150105.vdi --format VDI
        Converting from raw image file="debian-hurd-20150105.img" to file="debian-hurd-20150105.vdi"...
        Creating dynamic image with size 3146776576 bytes (3001MB)...

    2. The VDI format may help in with some of the goodies VirtualBox has to offer, like efficient snapshots.
  3. The rest remains the same. You need to configure the VM just as else. Currently supported arch is x86 only, so make the selections accordingly.
  4. The final VM config may look something like the following screenshot.
    1. ![](/sites/default/files/hurd-vbox.jpg)
  5. And here's the result in a [video](http://youtu.be/K5RmNAGmucI) captured on VirtualBox running Debian GNU/Hurd.



