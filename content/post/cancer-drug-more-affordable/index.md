{
    "title": "Cancer cure drugs now more affordable",
    "date": "2012-05-05T15:32:05-04:00",
    "lastmod": "2013-01-31T13:45:09-05:00",
    "draft": "false",
    "tags": [
        "cancer",
        "patents"
    ],
    "categories": [
        "Debian-Blog",
        "Jurisprudence"
    ],
    "url": "/blog/cancer-drug-more-affordable"
}

Now these are the kind of moves that needs to happen more often. Whether this
will cause a negative impact on the overall market, and the further invention
of drugs ([including patent control](patents-and-pharmaceutical-industries)),
but the affordability of the medicatoin to an average citizen is a great move.

The typical Chemotherapy can be on an average of 22 times. When summed up with
the dosage (somewhere around 250 mg IIRC), the cost comes to approx: (22/4) *
15000 = 82k, which now, will be affordable at 27k.

I guess the price slash is only for India and am not sure what the impact to
the global market will look like.

Quoting the article:



<http://www.moneycontrol.com/news/business/cipla-drug-price-cut-not-to-hurt-
revenue-much-say-analysts_700380.html>



> Cipla cut price of its kidney cancer drug Sorafenib, which is sold under
brand name Nexavar by multi-national Bayer to Rs 6,840 for a month's supply,
from around Rs 28,000 earlier. Its lung cancer drug Gestinib, which is sold
under brand name Iressa by AstraZeneca will cost Rs 4,250, versus Rs 10,200
earlier, and price of Temozolamide used to treat brain tumour, has been
reduced by Rs 15,000 to Rs 5,000.

>

> India's Patent Office recently issued a compulsory licence allowing Natco to
make a generic copy of Sorafenib, on the payment of a royalty to Bayer, which
sells the drug at around Rs 2 lakh.

>

> Domestic sales account for 46-47% of Cipla's total sales and of that the
cancer drugs portfolio is a very small portion, so these price cuts are
unlikely to have any major impact on its revenue, Hitesh Mahida of Fortune
Equity Brokers told moneycontrol.com

>

> "Cipla's idea seems to be to create disruption in the market, increase its
market share..." the analyst says.

>

> Swiss pharma major Roche had earlier this year signed a manufacturing deal
with India's Emcure Pharma so that its anti-cancer drugs Herceptin and
MabThera could be made in India at affordable prices. Analysts say Cipla's
move to slash prices could in future deter some MNCs from launching their
drugs in India at all, but some may also look at doing deals like the one
struck by Roche.

>

> Meanwhile, shares of pharma major Cipla surged over 3% on Friday after
brokerage CLSA upgraded the stock to "outperform" from "underperform," saying,
Cipla would be strongest beneficiaries of a weakening rupee.

>

> The rupee has been sliding sharply against the US dollar in recent days and
hit over four month low of around Rs 53.78 earlier in trade.

>

> "We expect improving margins over the coming quarters on back of a weak
rupee and a low base. We expect strong operating profit growth over coming
quarters led by margin expansion and high margin product supplies," CLSA's
Hemant Bakhru said.

>

> The US Food and Drugs Administration has approved Meda's drug Dymista for
allergic rhinitis and the product is widely expected to reach USD 300-500
million in annual sales over the coming years. The analyst says Cipla being a
partner, will benefit through product supplies over a longer term.

>

> "Apart from approval (outside North America) related milestone payment
(US$5m), we expect gradual increase in Cipla's sales from product related
supplies to Meda. Assuming Cipla supplies product at 10-15% of sales, it could
earn US$50-75m at peak sales," Bakhru said.

>

> Additionally, a low base in domestic formulations could result in reasonable
India growth, he adds.

>

> Cipla shares were up 2.8% at Rs 326.60 on NSE in noon trade.

