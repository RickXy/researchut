{
    "title": "Go for Geek lovers",
    "date": "2011-03-12T03:02:28-05:00",
    "lastmod": "2013-01-31T13:35:42-05:00",
    "draft": "false",
    "tags": [
        "Geek",
        "Nerd"
    ],
    "categories": [
        "Fun",
        "Psyche"
    ],
    "url": "/articles/go-for-geek"
}

The following document was taken from [Machtelt
Garrel's](mailto:tille@soti.org) Site.

## /* Begins

## Go for nerds!

The information below was gladly sponsored by European tax payers. The money
went to a Dutch company for doing social research among developers. People
doing this kind of research deserve a financial compensation, of course.

The start point of the research was a questionnaire for developers, asking
about demographical information, orientation, motivation, earning and
employment.

## The typical nerd

The survey started with a description of the typical geek or nerd that can be
expected in a developer environment:

  * The subject is male.
  * If not home-sticking, then at least computer-sticking.
  * Only interested in computers.
  * While some will say otherwise, it is generally believed they earn relatively high incomes.
  * The friends of a nerd are other nerds.
  * Nerds generally know their friends only by E-mail or IRC since they never go out.
  * The working day of a nerd usually starts when yours end. They live on cafeine, nicotine and other -ines.
  * They are single. Since they never go out.
  * A large part are still students.

Or, as the dictionary puts it:  _geek: a carnival performer often billed as a
wild man whose act usually includes biting the head off a live chicken or
snake_ , which translates as "not the sociable kind".

## Yes, it's true!

The actual survey was about checking if the above is all true. Most of it is.

And so much for the better. Geeks are typically at the perfect age for
reproduction and fun. Only one fifth is student, but all of them are 98% male.
But don't we all love a typical man with just a slight touch of the female
nature? Macho's with a soft spot?

Another advantage is that only one fifth of the geeks are married. That's
probably not the same fifth that are students, but that still makes over half
of them possible dates. Except if you love the risk of going for young boys or
married men, in that case you still have the full choice.

My general impression is that women like to have a partner they can respect,
somebody intelligent, and that women will give up an attractive partner for an
intelligent one. Now, your average nerd is not very attractive, but they do
have the brains, all consolidated in PhD's, university or highschool degrees
and various certificates. So I'm quite sure that geeks or nerds are OK, guru's
on the other hand are not good candidates in my opinion. It seems that you
have to really spend an awful lot of time behind your computer in order to
achieve guru status, while you can be a nerd by more or less keeping up
appearances. This can be done, specifically in the case of Open Source
activists, by spending a couple of hours a week on your projects. Most people
really don't do more than that, so there's plenty of time for building
relationships.

Also on the financial plan you're quite safe with a nerd: unemployed nerds
virtually don't exist. Except the students, but if you go for the young boys
option, you should be prepared to grab your wallet from time to time. It's a
well-known fact they don't have anything at all. The average geek on the other
hand makes a fair and reliable monthly income, enough to sustain wife and
children and a nice house in a nice neighbourhood.

## Free/Open Source developers vs. closed source developers

The above statements should of course be revised if we are to compare Open
Source developers with closed source developers. There's an equal amount of
both, but I would advise to try an Open Source developer if you have the
choice. Open Source developers are generally more satisfied with their work,
so consequently they will be more agreeable human beings. As an extra, they
suffer less from time pressure, so they have less stress, and a longer life.
About that last statement we don't know anything yet, because geeks are
commonly too young a group for producing this kind of statistical material,
but it is my guess this will become clear in a couple of decades.

Since Open Source developers clearly seem to enjoy what they're doing, they
don't usually think that money is all-important. They usually have a healthy
attitude towards balancing earning and spending money. Expect your presents in
hardware, of course. But hey, if he buys your laptop, all the more for you to
spend on make-up, new clothes or whatever you like!

Other nice side effect of Open Source is the motivation and energy these
developers have. And not only about their software. They will also be very
enthousiastic about you, guaranteed! And they want to make things better -
almost sounds like Philips, what more can you want? Plus they are innovative
and funny and generally not the kind of guy that wants to stay in his ivory
tower.

To a certain (large) class of Open Source developers, programming is an art.
They have an eye for aesthetics. They will tell you when you look too fat in
your dress, but when asked to observe, they will notice you changed your hair
colour.

Open Source people are also the kind of guys that usually have elaborate ideas
about politics and freedom, and are generally peace-loving. Not the kind of
husband that beats up his wife - maybe because they usually don't have the
physics for it to start with, but even if they would, you're safe.

## Conclusions

You might not have thought of it, but your true Jacob might be a geek...

More information can be found at
[infonomics.nl](http://www.infonomics.nl/FLOSS/report/). The survey was
published in the Autumn of 2002 and apparantly was also referenced in
[SlashDot](http://slashdot.org/).

## Ends. */

