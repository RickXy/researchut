{
    "title": "rm \\- \"-\" ??",
    "date": "2007-07-01T19:17:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools"
    ]
}

Does anyone know when really did coreutils get patched for this behavior ?  
  

> rrs@learner:~$ rm -foo  
>  rm: invalid option -- o  
>  **Try `rm ./-foo' to remove the file `-foo'.**  
>  Try `rm --help' for more information.  
>

This feature really wasn't earlier and if you ever ended up with files
starting with characters like  "-", it was not a straight task to remove it.  
  
Earlier (when I wasn't aware of this patch), the workaround was to write a
small 1 liner and not use getopt at all. Something like:  
  

> rrs@learner:~$ cat rm.py  
>  #!/usr/bin/env python  
>  
>  import os, sys  
>  
>  sys.stdout.write("Removing file %s.\n" % (sys.argv[1]) )  
>  os.unlink(sys.argv[1])  
>

I'm really happy to see this bug fixed.

