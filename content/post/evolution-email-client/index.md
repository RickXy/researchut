{
    "title": "State of Email Clients on Linux Based Platforms",
    "date": "2016-02-01T13:10:07-05:00",
    "lastmod": "2016-02-01T13:10:07-05:00",
    "draft": "false",
    "tags": [
        "gnome",
        "evolution",
        "Email"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/evolution-email-client"
}

I've been trying to catch up on my reading list (Thanks to rss2email, now I
can hold the list longer, than just marking all as read). And one item from
last year's end worth spending time was Thunderbird.

Thunderbird has been the email client of choice for many users. The main
reason for it being popular has been, in my opinion, it being cross platform.
Because that allows users an easy migration path across platforms. It also
bring persistence, in terms of features and workflows, to the end users.
Perhaps that must have been an important reason for many distributions
(Ubuntu) and service providers to promote it as the default email client. A
Windows/Mac user migrating to Ubuntu will have a lot better experience if they
see familiar tools, and their data and workflows being intact.

Mozilla must have executed its plan pretty well, to have been able to get it
rolling so far. Because other attempts elsewhere (KDE4 Windows) weren't so
easy. Part of the reason maybe that any time a new disruptive update is rolled
on (KDE4, GNOME3), a lot many frustrated users are born. It is not that people
don't want change. Its just that no one likes to see things break. But
unfortunately, in Free Software / Open Source world, that is taken lightly.

That's one reason why it takes Mozilla so so so long to implement Maildir in
TB, when others (Evolution) have had it for so long.

So, recently, Mozilla [announced](http://lwn.net/Articles/666295/) its plans
to drop Thunderbird development. It is not something new. Anyone using TB
knows how long it has been in Maintenance/ESR mode.

What was interesting on LWN was the comments. People talked a lot about DE
Native Email clients - Kmail, Sylpheed. TUI Clients and these days Browser
based clients. Surprisingly, not much was talked about Evolution.

My recent move to GNOME has made me look into letting go of old
tools/workflows, and try to embrace newer ones. Of them has been GNOME itself.
Changing workflows for email was difficult and frustrating. But knowing that
TB doesn't have a bright future, it was important to look for alternatives.
Just having waited for Maildir and GTK3 port of TB for so long, was enough.

On GNOME, Evolution, may give an initial impression of being in Maintenance
mode. Especially given that most GNOME apps are now moving to the new UI,
which is more touch friendly. And also, because there were other efforts to
have another email client on GNOME, I think it is Yorba.

But even in its current form, Evolution is a pretty impressive ~~email
client~~ **Personal Information Management** tool. It already is ported to
GTK3. Which implies it is capable of responding to Touch events. It sure could
have a revised Touch UI, like what is happening with other GNOME Apps. But I
'm happy that it has been defered for now. Revising Evolution won't be an easy
task, and knowing that GNOME too is understaffed, breaking a perfectly working
tool won't be a good idea.

My intent with this blog post is to give credit to my favorite GNOME
application, i.e. Evolution. So next time you are looking for an email client
alternative, give Evolution a try.

Today, it already does:

  * Touch UI
  * Maildir
  * Microsoft Exchange
  * GTK3
  * Addressbook, Notes, Tasks, Calendar - Most Standards Based and Google Services compatible
  * RSS Feed Manager
  * And many more that I may not have been using

The only missing piece is being cross-platform. But given the trend, and
available resources, I think that path is not worthy of trying.

Keep It Simple. Support one platform and support it well.

