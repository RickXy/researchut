{
    "title": "SSH Password-less login",
    "date": "2006-08-12T09:15:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

First generate a public key

    
    
    		1.rrs@geeKISSexy:~$ ssh-keygen -t dsa  
                    2.Generating public/private dsa key pair.  
                    3.Enter file in which to save the key (/home/rrs/.ssh/id_dsa):  
                    4.Created directory '/home/rrs/.ssh'.  
                    5.Enter passphrase (empty for no passphrase):  
                    6.Enter same passphrase again:  
                    7.Your identification has been saved in /home/rrs/.ssh/id_dsa.  
                    8.Your public key has been saved in /home/rrs/.ssh/id_dsa.pub.  
              
    Enter the command "ssh-keygen -t dsa" (line 1.).  
    When prompted on file where key is saved just hit "Enter" (line 3.).  
    When prompted for a passphrase just hit "Enter" (line 5.).  
    When prompted to confirm passphrase hit "Enter" a second time (line 6.).   
    Once you have created your public key you can copy it to the server.   
              
      
                    1.rrs@geeKISSexy:~$ ssh-copy-id -i ~/.ssh/id_dsa.pub rrs@learner  
                    25  
                    3.The authenticity of host 'learner (192.168.1.1)' can't be established.  
                    4.RSA key fingerprint is 8a:8a:53:b3:1c:d6:72:b5:e9:e8:7d:c7:81:8c:6f:0b.  
                    5.Are you sure you want to continue connecting (yes/no)? yes  
                    6.Warning: Permanently added 'learner' (RSA) to the list of known hosts.  
                    7.rrs@learner's password:  
                    8.Now try logging into the machine, with "ssh 'rrs@learner'", and check in:  
              
    Enter the command "ssh-copy-id -i [location of key] [username@server IP] (line 1.).  
    You may get some complaints due to defaults but when prompted to continue enter "yes" (line 5.).    
    You will next be prompted to enter the user's password (line 7.). Remember the user password must be the same on the the client and server  
    Finally you will be asked to see if you can log in via ssh on the server (line 8.).   
    If everything is correct you should now be able to log in on the server in a your user account without having to enter a password.   
              
      
                    rrs@geeKISSexy:~$ ssh rrs@learner  
                    Last login: Sat Aug 12 11:21:54 2006

