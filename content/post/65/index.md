{
    "title": "Networking in KVM/QEMU",
    "date": "2007-08-04T09:24:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Virtualization"
    ]
}

In my previous blog entry, I mentioned about not being able to simply
configured networking for the Guest VMs. I feel that area is still
unimplemented properly by the GUI wrappers available for KVM/QEMU.

There is a good utility VDE2, which can work great for all your networking
needs for the Guest VMs. But unfortunately none of the GUI wrappers
(Qemulator, QtEmu, Qlauncher) are handling it.

The simplicity of vde is amazing. Here's what all you need to do:

> iface tap0eth2 inet static  
>          address 172.16.1.1  
>          netmask 255.255.0.0  
>          network 172.16.1.0  
>          broadcast 172.16.1.255  
>          vde2-switch -  
>          #tunctl_user uml-net  
>          pre-up /sbin/iptables -t nat -A POSTROUTING -s 172.16.1.1/24 -o
eth2 -j MASQUERADE;  
>          pre-down /sbin/iptables -t nat -D POSTROUTING -s 172.16.1.1/24 -o
eth2 -j MASQUERADE;  
>          up echo 1 > /proc/sys/net/ipv4/ip_forward  
>          down echo 0 > /proc/sys/net/ipv4/ip_forward  
>  
>

>

> And this is how you'd execute each VM (be it from a wrapper of from the
command-line)

>

> rrs@learner:~/bin$ cat virtual-hurd  
>  #!/bin/sh  
>  
>  vdeq kvm -M pc -hda /media/VMIMAGES/Debian_GNU_Hurd.img -m 128 -usb \  
>  -net nic,vlan=1,macaddr=52:54:00:12:02:00 -net
vde,vlan=1,sock=/var/run/vde2/tap0eth2.ctl -fda \  
>  /media/VMIMAGES/hurd-floppy.img -boot a  
>

>

> rrs@learner:~/bin$ cat virtual-kfreeBSD  
>  #!/bin/sh  
>  
>  vdeq kvm -M pc -hda /media/VMIMAGES/Debian_GNU_kFreeBSD -m 256 -usb -net \  
>  nic,vlan=1,macaddr=52:54:00:12:01:00 -net
vde,vlan=1,sock=/var/run/vde2/tap0eth2.ctl -boot c  
>

>

> rrs@learner:~/bin$ cat virtual-PCBSD  
>  #!/bin/sh  
>  
>  vdeq kvm -M pc -hda /media/VMIMAGES/PC-BSD.img -m 512 -usb -net \  
>  nic,vlan=1,macaddr=52:54:00:12:03:00 -net
vde,vlan=1,sock=/var/run/vde2/tap0eth2.ctl -boot c  
>

Interesting things to notice here are the  **vde2-switch -** option in the
network configuration section and **macaddr=52:54:00:12:03:00** being used
with each instance of KVM/QEMU. The macaddr needs to be unique for all the VMs
or otherwise you'll have network lost in all VMs except the one that gets
executed last.

As for the tap0eth2 interface, it is very simple. All you'll need to do is to
NAT tap to the interface (eth2 in my case) which talks to the external
network.

That's it. Enjoy the network access in the Guest VMs.

PS: Network access has worked for me in Guest VMs on Hurd, FreeBSD, PCBSD,
Minix. These are the only ones I've tried till now.

