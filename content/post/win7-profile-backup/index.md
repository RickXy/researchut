{
    "title": "Windows 7 Profile Synchronization",
    "date": "2013-12-18T12:23:53-05:00",
    "lastmod": "2014-11-12T03:32:04-05:00",
    "draft": "false",
    "tags": [
        "Windows 7",
        "VirtualBox",
        "Roaming Profiles",
        "backups",
        "SyncToy"
    ],
    "categories": [
        "General",
        "Fun",
        "Computing",
        "Virtualization"
    ],
    "url": "/blog/win7-profile-backup"
}

Lately, for my day job (to be more efficient for the time I spent), I am
required to use Windows, back again. :-)

It is great to be back. By back, I mean using Windows for some of the
workflows. All these years, I've been using Linux based tools (Debian, Kontact
| IceDove, Konq | Chromium | IceWeasel etc) to get my job done. It is great to
try back Windows for some of the workflows.

So my Guest VM is a **Windows 7** Client. It is connected to my corporate
domain. But interestingly, we do not use **Roaming Profiles**. That means, if
I corrupt my Windows VM, I lose all my local changes. PS: And it did happen
twice. That 's why you are reading this blog entry. So don't procrastinate.

Back when interactinge with Win2K Client, IIRC, there was an option to specify
_the location of your profile_. I am not sure if that option really was
present or was it just my memory.

Anyways, my intent was to look for something which would allow me to sync my
profile to a network location, which, I could map to a **VirtualBox** map,
pointing to a _Linux File System location_. That 'd allow me to have a copy of
the Windows profile handy, in case the Guest VM ever got lost.

So what do we have???? Microsoft provides the **Sync Center** tool, which is
intended to synchronize and make available your network files / folders.   **
_BUT NOT vice versa_**. :-( I though MS was good at providing choice but they
seem to have been choosing the Apple route.

With Sync Center gone, what other choices do we have??? _My intent is to run
something from within the Guest VM, and not externally (like samba tools,
rsync etc). I also desire to run something native and not a 3rd party, for
obvious reasons._

So that brings me to **SyncToy**. This tool is something I had never heard of
before, but then, I never ran into a requirement like this....... Since the
very first run, I have been happy and impressed with this tool. Below I 'll
give you the reasons why....

  *  _ **Simplicity**_ \- Yeah!!! I like the simplicity they 've provided. The screenshot should say it all..![](/sites/default/files/sync%20toy%20configuration%20summary.jpg)



  *  **Operating Modes** \- The tool has 3 opeating modes. The one I use is _Echo_ , which as the name suggests, will echo from LHS to RHS![](/sites/default/files/sync%20toy%20synchronization%20mode.jpg)



  *  **Regex Filter** \- So in within the **Profile Folder** that I backup, I do not want to backup the **Outlook** Folder. That data is anyways frequently backed up by my **MS Outlook** client to the **Exchange** server. And also, that folder cache is bloatedly big. SyncToy made me happy here too.  ![](/sites/default/files/folder%20exclusion.jpg)



  *  ** ~~Execution~~ Summary** \- With all the settings in place, just initiate the run and you see the following. ![](/sites/default/files/Sync%20Toy%20Summary.jpg) Hmmmmm!!!! But run it always, _manually_. Don 't I have a better (cron) job????



  *  **Windows Cron** \- Here you go.... I desired to have it run every 12 hrs. But the interface is shy to show. And I 'm lazy to poke..![](/sites/default/files/sync%20toy%20task%20scheduler.jpg)

