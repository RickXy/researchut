{
    "title": "Touchpad Gestures",
    "date": "2015-12-07T12:33:54-05:00",
    "lastmod": "2015-12-07T12:33:54-05:00",
    "draft": "false",
    "tags": [
        "libinput",
        "multitouch",
        "gesture",
        "Xorg",
        "X",
        "Wayland"
    ],
    "categories": [
        "Debian-Blog"
    ],
    "url": "/blog/multitouch-gestures-libinput"
}

If you have a modern laptop, you must be having a touchpad with multitouch
capability, in hardware. But on the software front, it must be crap.

And if you've always craved for having support for those multitouch gestures,
you must have been following the X/Wayland development.

From what I've known so far, **libinput** is the successor to all older input
support in _**X/Wayland**_. But it does have some conditions at this time.
Like: Multitouch Gesture is only available in Wayland, not in X. Similarly, if
you use _**GNOME/Wayland**_ , not all _**RandR**_ functionalities are
available in Wayland, that are there in X.

So if you'll still be using X for some time, and want Touchpad Gestures, you
may consider looking at: https://github.com/bulletmark/libinput-gestures

For Debian, [this](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=806985)
still needs to fixed before you can use. But if you read till here, you know
your way.

