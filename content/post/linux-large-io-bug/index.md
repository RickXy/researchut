{
    "title": "Linux IO + Memory + CPU Contention",
    "date": "2016-02-26T12:23:33-05:00",
    "lastmod": "2016-02-26T12:23:33-05:00",
    "draft": "false",
    "tags": [
        "linux",
        "memory",
        "block",
        "cpu",
        "contention"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Technology"
    ],
    "url": "/blog/linux-large-io-bug"
}

I very recently met someone, and we had a good productive discussion on the
features and (long standing) bugs of the Linux kernel. No doubt, Linux is the
most featureful kernel in the market. Is also a lot appealing given its
breadth of platform support.

Of that discussion we had, it led about Linux's behavior in tighter stressed
scenarios where there is a lot of contention among the core subsystems. From
the conversation, I got the feedback that perhaps the
[issue](https://bugzilla.kernel.org/show_bug.cgi?id=12309) is no more valid.
My conclusion was I must have missed out on the fix because I haven't really
spent any Engineering Lab time in recent past.

But a picture says a thousand words. And no, the issue is NOT fixed. And it is
still simple to reproduce the bug. And it is just that we have faster
advancements in underlying hardware, that we may eventually find it much
harder to reproduce these bugs.



![](/sites/default/files/IMG_20160226_223108.jpg)

The kernel spitting the screen is Linux 4.4 Stock Debian.

