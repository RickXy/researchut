{
    "title": "Offline Package Manager - apt-offline",
    "date": "2011-08-19T06:23:13-04:00",
    "lastmod": "2016-10-21T02:29:15-04:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "apt-offline-gui"
    ],
    "categories": [
        "Debian-Pages",
        "Computing",
        "Tools"
    ],
    "url": "/tools/apt-offline"
}



A copy of this article is also available at:

<http://www.debian-
administration.org/article/648/Offline_Package_Management_for_APT>

This article is about Offline Package Management in Debian. Debian is a pretty
well known project. One of the things that makes Debian very popular is APT
(a.k.a Advanced Packaging Tool) which allows remote package downloads,
upgrades and dependency resolution. Unfortunately it does require a network
connection - unless you use apt-offline.

In Debian, when you need to install a package, you usually would fire up the
apt-get command and the software would just install without any hand holding.

While APT is really very cool one of the main reasons for its success is the
Debian Policy. The Debian Policy is like the brain of the project that
controls the entire project ensuring that all the bits and pieces fit well
together upto the Debian Standards. APT is just a result of the fantastic
Debian Policy work.

In Debian, every package is very well self-contained and is tightly related to
each other using APT. APT does a very good job of integrating and resolving
dependencies for Package Management and takes off all the Dependency Hell
problems from the user.

This is where the problem starts - for a machine which has network access it
works very well because APT generates the list of packages and their
dependencies and is able to download and install them successfully.

But when it comes to downloading a package individually on a different
machine, along wih resolving any dependencies this can be a big problem.

Consider this real world example: I have a Debian box at home. At home, I have
no (or very slow/expensive dial-up) internet connection. At work, I (or my
friend) do have a very fast connection but (as part of IT policy) am required
to use Windows.

I would still like to be able to painlessly update/upgrade my Debian box at
home, with all the power and flexibility of APT.

This is where apt-offline is useful. [apt-offline](http://apt-
offline.alioth.debian.org/) is an  _offline_  APT Package Manager.

Using apt-offline:

  * You generate a signature on your Debian box at home and carry the signature file on a removable medium (Probably a USB Stick).(e.g. "apt-offline set /tmp/apt-offline.txt")
  * Now you take the USB Stick (with the apt-offline.txt signature file) to the office machine which could be running any linux version, or as I mentioned above, even Windows.
  * There, you could run apt-offline giving it the signature file. (e.g. "apt-offline get C:\apt-offline.txt")
  * apt-offline would generate you an archive file or a folder with all the data. That data can be copied on a removable media. The removable media can be attached back to the disconnected Debian box at home and installed. (e.g. "apt-offline install /tmp/apt-offline.zip")

**Let's start with a 3 step example**

**Step 1**

Generate a signature file on the Disconnected Debian box at home

    
    
    apt-offline set /tmp/apt-offline.sig
    

The above command will generate all information required from apt about
updating its database.

By default, with no additional arguments passed, apt-offline will extract
information about  **APT Package Database Update i.e. the  \--update option**
as well as the list of  **Packages to be upgraded i.e. the  \--upgrade
option**.

These options can also be individually passed if you want only one of those.

**Step 2**

Download data based on the signature file generated earlier

    
    
    apt-offline get C:\apt-offline.sig --threads 5
    

The above command will download data as mentioned in the signature file. To
speed up downloads (that can be from multiple apt repositories), in this
example we spawn 5 download threads.

**Note:**  It would be good to also download the bug reports for the packages
that you are downloading. So that example now becomes:

    
    
    apt-offline get C:\apt-offline.sig --bug-reports --threads 5
    

There are many more options that you can pass to apt-offline, like the
\--bundle option which would generate for you, an archive file with all the
data.

Once completed, you could just copy the data (an archive file, if you used the
\--bundle option) back to the removable medium and copy it back onto your
offline host.

**Step 3**

Once you're back upon the home Debian machine, you feed the data from the
removable medium to apt-offline:

    
    
    apt-offline install /media/USB/apt-offline.zip
    

This will update the APT database on your disconnected machine seamlessly.

If there were packages that needed to be upgraded, now they would all be
available (with dependencies) in the APT database. So if you do an apt-get
upgrade now, APT won't prompt you mentioning even a single bye download. APT
would find that all required packages are already present in the APT cache.

If you had used the \--bug-reports switch that I mentioned earlier, during
install apt-offline would prompt you with the list of bug reports related to
the packages on your machine that need be upgraded/installed - not just the
list but the full bug report will be available for you to look at and evaluate
the severity involved.

As you can see from the article above, apt-offline helps you achieve the power
of APT, in just 3 steps. apt-offline is part of Debian and is [developed at
Alioth](http://apt-offline.alioth.debian.org/) and
[Github](https://github.com/rickysarraf/apt-offline).



![](https://camo.githubusercontent.com/867002ddf0a84bc99625d4221eb1f7f6779020e9/68747470733a2f2f6c68362e676f6f676c6575736572636f6e74656e742e636f6d2f5f5f6574717a2d79655034732f545a3778705772717965492f41414141414141414255552f66705a565f316f496e37342f733634302f6170742d6f66666c696e652d616476616e6365642d6f7074696f6e732e706e67)

