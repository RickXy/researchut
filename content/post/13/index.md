{
    "title": "tomoyo for debian",
    "date": "2010-05-11T03:50:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "Mandatory Access Controls",
        "security",
        "mac"
    ],
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

Just uploaded tomoyo-tools and is waiting in the NEW queue.

Thanks to Moritz Muehlenhoff, tomoyo kernel support should be available in
Debian with kernel 2.6.32-13 and above.

What is Tomoyo ?

Description: Lightweight and easy-use Mandatory Access Control for Linux

 TOMOYO Linux is Lightweight and Usable Mandatory Access Control with

  \- "automatic policy configuring" feature by "LEARNING mode"

  \- administrators friendly policy language

  \- no need libselinux nor userland program modifications

 .

 TOMOYO Linux consists of patches to Linux kernel and administrative

 utilities, and this package contains its audit daemon and tools.

Description: Lightweight and easy-use Mandatory Access Control for Linux

TOMOYO Linux is Lightweight and Usable Mandatory Access Control with

\- "automatic policy configuring" feature by "LEARNING mode"

\- administrators friendly policy language  

\- no need libselinux nor userland program modifications .

TOMOYO Linux consists of patches to Linux kernel and administrative utilities,
and this package contains its audit daemon and tools.

