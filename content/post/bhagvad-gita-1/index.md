{
    "title": "Song of God – The Bhagavad Gita (Part – I)",
    "date": "2016-01-10T11:58:54-05:00",
    "lastmod": "2016-01-10T11:58:54-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism",
        "bhagvad gita",
        "religion"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/bhagvad-gita-1"
}

![](/sites/default/files/bh.jpg)

The armies of the Pandavas, and the Kauravas stood facing each other on the
battlefield. Then suddenly, a chariot drew away from the Pandava side and came
between the two armies. A banner displaying the image of a monkey fluttered
above it. It was Arjuna.

Arjuna looked at the army before him. Then he looked at the army behind him.
Brothers, uncles and nephews, ready to fight and kill one another - for what?
A piece of land? "I cannot do this", he said. "This cannot be dharma!"

To the surprise of all assembled warriors, he lowered his bow.

"Don't be such a weakling, Arjuna. Face the situation like a man!" shouted
Lord Krishna.

"I cannot", moaned Arjuna, his shoulders dropping.

'It's your duty as a Kshatriya", said Lord Krishna, trying to reason with him.

"I cannot", said Arjuna.

"They abused your wife. They encroached upon your kingdom. Fight for justice,
Arjuna", pleaded Lord Krishna.

Arjuna replied - "I see no sense in killing brothers and uncles and friends.
This is cruelty, not nobility. I would rather have peace than vengeance."

Lord Krishna said - "Noble thoughts indeed. But where does this nobility comes
from? Generosity or fear? Wisdom or ignorance? Suddenly, you are confronted by
the enormity of the situation - the possibility of failure, the price of
success - and you tremble. You wish it had not come to this. Rather than face
the situation, you withdraw. Your decision is based on a misreading of the
situation. If you knew the world as it truly is, you would be in bliss even at
this moment."

"I don't understand.", said Arjuna.

It was then that Lord Krishna sang his song, a song that explained to Arjuna
the true nature of the world. This was  **The Bhagavad Gita.**

"Yes, you would kill hundreds of warriors. But that would be the death only of
the flesh. Within the flesh is the immortal soul that never dies. It will be
reborn; it will wrap itself in a new body as fresh clothes after old ones are
discarded. What is a man's true identity; the temporary flesh or the permanent
soul? What do you kill, Arjuna? What can you kill?

"The flesh exists to direct you to the soul. For the flesh enables you to
experience all things temporary - your thoughts, your feelings, your emotions.
The world around it is temporary. The body itself is temporary. Eventually,
disappointed of all things temporary, you will seek permanence and eventually
discover the soul. You grieve for flesh, right now, Arjuna, without even
realizing the reason it exists."

"Of all living creatures, the human being is the most blessed. For human flesh
is blessed with intellect. Humans alone can distinguish between all that is
temporary and all that is not. Humans alone can distinguish between flesh and
soul. Arjuna, you and all those on this battlefield have spent their entire
life losing this opportunity - focusing more on mortal things than on immortal
things."

"Your flesh receives information about the external world through your five
sense organs: eye, ear, nose, tongue and skin. Your flesh engages with the
external material world through your five action organs: hands, feet, face,
anus and genitals. Between the stimulus and the response, a whole series of
processes take place in your mind. These processes construct your
understanding of the material world. What you, Arjuna, consider as the
battlefield is but a perception of your mind. And like all perceptions it is
not real."

"Your intellect is not aware of your soul. It seeks meaning and validation.
Why does it exist? It seeks answers in the material world and finds that
everything in the material world is mortal, nothing is immortal. Awareness of
death generates fear. It makes the intellect feel invalidated and worthless.
From fear is born the ego. The ego contaminates the mind to comfort the
intellect. It focuses on events, memories and desires that validate its
existence and make it feel immortal and powerful. It shuns all that that makes
it feel worthless and mortal. Right now, your ego controls your mind, Arjuna.
It gives greater value to the finite experience of your flesh and distracts
you from the infinite experiences of your soul. Hence, your anxiety, fear and
delusion."

"Your mind retains memory of all past simulations - those that evoke fear and
those that generate comfort. Your mind also imagines situations that frighten
you and comfort you. Goaded by your ego, you suppress memories that cause pain
and prefer memories that bring pleasure. Goaded by your ego, you imagine
situations that the ego seeks and shuns. Right now, Arjuna. on this
battlefield, nothing has happened. But a lot is occurring in your mind -
memories resurface as ghosts and imagination descends like a demon. That is
why you suffer."

"Your ego constructs a measuring scale to evaluate a situation. This measuring
scale determines your notions of fearful or comforting, painful or
pleasurable, right or wrong, appropriate or inappropriate, good or bad. It is
informed by the values of the world you live in, but is always filtered by the
ego before being accepted. Right now, Arjuna, what you consider right is based
on your measuring scale. What Duryodhana considers right is based on his
measuring scale. Which measuring scale is appropriate? Is there one free of
bias?"

"The world that you perceive is actually a delusion based on your chosen
measuring scale. New memories and new imaginations can change this measuring
scale, hence your perception of the world. Only the truly enlightened know the
world as it truly is; the rest construct a reality that comforts the ego. The
enlightened are therefore always at peace while the rest are constantly
restless and insecure. If you were enlightened, Arjuna, you could have been in
this battlefield, bow in hand, but still in peace. If you were enlightened,
Arjuna, you would have fought without anger, killed without hate."

"Your ego clings to things that grant it maximum comfort. The purpose of life
then becomes the pursuit of comfort - generating states, the shunning of fear
- generating states. Attainment of desirable states brings joy, failure to do
so becomes sorrow. The ego clings tenaciously to things and ideas that
validate its existence. The ego does everything in its power to establish and
retain a permanent territorial hold over all external states that give it joy.
Do you realize, Arjuna, all you want is to reclaim or recreate situations that
give you joy? You have attached your emotions to external events. Separate
them."



 **Copyright (C) Vibhor Mahajan**

