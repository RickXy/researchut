{
    "title": "Linux Virtualization Richness",
    "date": "2007-07-29T03:57:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Virtualization"
    ]
}

So Yesterday, I was finally confident enough to uninstall
[**VMWare**](http://www.vmware.com) from my laptop.

Thanks to the richness of virtualization technologies which are present in
Linux (2.6.22) now.

I've always liked to explore different operating systems (not distributions
really) to see what interesting features they implement differently. BSDs
being one, Solaris being the other. But sometimes it really was difficult for
me to try them out.

Reason:

  * The current machine that I have apart from my laptop is a very old box with 64Mb of RAM and a 750 Mhz Intel Processor
  * This box is primarily used for building my cusotmized kernels to run on my laptop.
  * This box serves as a file server.
  * This box doesn't have a head. 

I also own a Dell XPS M1210 laptop with a pretty good configuration.

  * Intel Core Duo 2500 2.0 Ghz Processor
  * 2 GB of RAM
  * 60 GB 7200 RPM SATA Hard Disk Drive 
  * 120 GB 5400 RPM External USB Hard Disk Drive

But wasn't really maxing out the power of this machine. Thanks to
virtualiazation technologies in Linux - Qemu, KVM, UML, It has been pretty
good now enjoying the benefits of the machine without really messing the base
OS on which I rely for all of my work.

Now coming back to the point of VMWare. VMWare WS is a good software. It has a
very usable and nice GUI interface.

So what really kept me away from using Linux Virtualization implementation
extensively for long was really the GUI. I mean I have been a Linux console
user for 8 years and am very comfortable with it, but today I rather prefer
using a GUI to do most of my tasks. Maybe blame KDE for that - It's so elegant
that it spoilt my console habits.

So what really helped me replace VMWare from my box is a good GUI wrapper for
Qemu, [**Qemulator**](http://qemulator.createweb.de/)

This app really helped me move away from VMWare and enjoy the native
virtulization implementation of Linux.

In my exploration for a GUI wrapper for Qemu, I finally had considered 3 apps.

  1. [Qemulator](http://qemulator.createweb.de/)
  2. [QtEmu](http://qtemu.org)
  3. [OLPC Qemu Admin](http://people.redhat.com/berrange/olpc/sdk/olpc-qemu-admin-demo.html)

Unfortunately, I wasn't able to try out **OLPC Qemu Admin** , which looks to
be a product of RedHat, because I couldn't find a downloable link on their
site. Probably it is supposed to be shipped only with Fedora.

Among Qemulator and QtEmu I finally made up my mind with Qemulator even though
QtEmu is a good contendor. But it lacks some basic features which Qemulator
has covered properly. Probably it is just a matter of time by when those
features will be implemented/fixed.

One of the good features of Qemulator that I liked (which isn't available in
QtEmu) is the ability to add your custom virtualization app (KVM in my case)
on a image-by-image basis.

Here are some screenshots which could explain some of the points that I've
tried to explain:

![](http://www.researchut.com/blog/images/qemulator_qtemu_main_window.png)

This is the main window interface for both the apps, Qemulator and QtEmu.

![](http://www.researchut.com/blog/images/qemulator_qtemu_emulator_type.png)

Here if you notice, you can assign a different emulator type for each image in
Qemulator. You can also add additional emulator types, like KVM. This is
currently one big limitation in QtEmu which I think should be very minor
problem to solve.



![](http://www.researchut.com/blog/images/qemulator_settings_emulator.png)

This is Qemulator's Settings window with a lot of possibilities for
customization

![](http://www.researchut.com/blog/images/qemulator_screenshot_capture.png)

Another greate feature of Qemulator is Screenshot Capture. You can capture
screenshots of your VM as you want. Really wonderful. There's also Wave
capture which I haven't tried yet.

![](http://www.researchut.com/blog/images/qemulator_pcbsd.png)

And finally, here's PCBSD running smoothly in KVM.

The virtualization technologies in Linux along with proper GUI apps have
become a real good solution to use. I'm now eagerly looking forward to 2.6.23
and ahead where I'd be able to explore more option with lguest and later
Paravirtualization from within KVM.

On another note, there's also another virtualization beast in Linux which
works prefect. UML, or User Mode Linux, if that sounds better is another great
technology if you're only exploring different Linux versions. If you are a
distro fanatic and like to run multiple Linux distributions, UML would be a
very good option for your requirements. And the good part about UML is that it
runs as a normal user process which when gone bad can be killed very easily.
There's also an additional patch maintained by BlaisorBlade, SKAS (Separate
Kernel Address Space) which dramatically improves the performance of UML.For
years, it has been maintained externally and I doubt if it is ever going to be
merged mainline.  But it would have been a lot better if SKAS was available as
a Kernel Module. That'd avoid a requirement to recompile the whole kernel for
a general user.

