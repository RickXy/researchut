{
    "title": "pypt-offline 0.6.Beta Released",
    "date": "2006-07-09T20:19:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools"
    ]
}

I'm happy that a new release has been possible.

This new release is almost a complete re-write and has many fixes and
features.

For details, visit:

<http://pypt-offline.sf.net>

There's also a small HOWTO about pypt-offline

Cheers!

Ritesh

pypt-offline -- An Offline Package Manager  
(C) 2005, 2006 Ritesh Raj Sarraf <rrs@researchut.com>  
  
  
  
# INTRODUCTION  
  
So you've decided to give this small piece of work a try.  
Good ! Let's get it done faster.

pypt-offline is an offline package management tool written in the Python
Programming Language. This program, as of now, is intended for people using
Debian (And Debian based) systems.

This program allows leveraging the power of Debian (more precisely apt-get)
onto a completely disconnected machine. Most of the people with slow or no
internet connection (most of those in India/Nepal/Pakistan and nearby
countries) have not considered using Debian (or Debian derived distributions)
because Debian's real taste is experienced when it is connected to the
internet.

This utility is an attempt in making that problem eradicate. I hope this
utility comes to use to you.  
I'd be eager to hear your comments/suggestions. Feel free to drop an email at
rrs _AT_ researchut |DOT| com

#########################################################################

Let us assume you have a machine at home ( Hereby called Machine-A) with no or
very expensive internet connection on which you've installed Debian (Hereby
all Debian and Debian based systems will be called as Debian).

You or your friend works at a local city office where they have High Speed
Internet Connection. The machine used there is Linux/Windows/Mac box. We'll
call this Machine-B henceforth.

pypt-offline allows you to synchronize Machine-A's apt database with the
Debian archives.  
It does so by  
* Extracting the details of Machine-A's apt's database which needs to updated  
* Fetches the required database on Machine-B  
* Synchronizes the fetched database back to Machine-A

With these 3 steps you can keep a disconnected Debian machine up-to-date on a
daily basis.

The rest of the document will describe the details along with the commands.

STEP - 1  

On Machine-A:

pypt-offline --set-update /tmp/update-uris

With this command, pypt-offline extracts the details from apt's database for
the files that are required to update apt's package database. The extracted
information is stored in /tmp/update-uris

pypt-offline --set-upgrade /tmp/upgrade-uris --upgrade-type dselect-upgrade

With this command, pypt-offline extracts the details from apt's database for
the packages that are required to be upgraded. The extraced information is
stored in /tmp/upgrade-uris  
There are 3 types of "upgrade" type. "upgrade", "dist-upgrade" and "dselect-
upgrade".  
You can pass any one of them.  
Note: --set-upgrade and --upgrade-type are mutually inclusive. You cannot use
one option without the other

pypt-offline --set-install /tmp/install-uris --set-install-packages package1
package2 packageN

With this command, pypt-offline extracts the details from apt's database for
the packages (and its dependent packages) that are required to be installed.
The extracted information is stored in /tmp/install-uris  
Note: --set-install and --set-install-packages are mutually inclusive. You
cannot use one option without the other

The above mentioned options are executed on Machine-A. They extract the
details from apt's database.

Now the user needs to copy the extracted data file onto a removable media and
take it to Machine-B.

STEP - 2

On Machine-B:

With the extracted data file in hand, on Machine-B, execute the following
commands:

pypt-offline --fetch-update /tmp/update-uris [ -d /tmp/updates/ -s
/tmp/repository/ --disable-md5check --zip --zip-update-file /tmp/updates.zip
--zip-upgrade-file /tmp/upgrades.zip]

With this command, pypt-offline fetches the required data from the internet as
directed by Machine-A  
The options in square bracket are optional

/tm/updates/ - With this option, the directory where the downloaded data will
be saved is set. If the option is not given, a folder named pypt-offline-
downloads is created under the current working directory.

-s /tmp/repository/ - With this option, the directory where the previously downloaded data was saved is searched. This is used so that you don't download the same file again and again. If the option is not given, the current working directory is used as the repository and all files and folders under it are recursively searched.  
Also the freshly downloaded files are copied to this folder so that you don't
have to download it again the next time.  
Note: This option is effective only for packages which are downloaded and
*NOT* for update files. Update files change almost daily in the Debian
repositories, hence keeping a local cache is useless.

\--disable-md5check - It is *highly discouraged* to use this option. By
default, pypt-offline compares the md5checksum for every package it downloads.
If the md5checksum don't match, the file is deleted. This is necessary to make
sure that you don't end up having tampered packages.  
Note: This option is effective only for packages which are downloaded and
*NOT* for update files. Update files currently don't provide md5checksum
details.

\--zip - It is *highly encouraged* to use this option. Though disabled by
default, if enabled, pypt-offline creates a single zip file of the downloaded
files. This way it is easier to track the downloaded files.

\--zip-update-files - This option sets the zip file name. If this option is
not set, pypt-offline uses the default file name, pypt-offline-update.zip

\--zip-upgrade-files - This option sets the zip file name. If this option is
not set, pypt-offline uses the default file name, pypt-offline-upgrade.zip

If you don't use the --zip option, the downloaded files are stored in the
folder you mentioned with the -d option. You'll need to copy the files from
the folder.

With this, once all the data has been downloaded, copy it to your removable
storage device and take it back to Machine-A.

STEP - 3

On Machine-A:

Once you come back to Machine-A:  

pypt-offline --install-update /tmp/updates.zip [ /tmp/]

With this command, pypt-offline syncs the update files from updates.zip. If a
folder name is given as an argument to \--install-update, it searches for the
required files and syncs them to apt's package database.

pypt-offline --install-upgrade /tmp/upgrade.zip [/tmp/]

With this command, pypt-offline syncs the package from updates.zip. If a
folder name is given as an argument to --install-update, it searches for the
requires files and syncs them to apt's package database.  
Note: Please keep in mind that this doesn't actually install/upgrade the
packages. It just makes the "To Be Downloaded" package available. With it apt
will not require to download any additional packages and hence will do the
installation as if it was downloaded from the internet.

NOTE: If you use apt-listbugs to track package bugs while you install, it'll
fail because apt-listbugs will try to connect to the internet.

  
  
That's all. Please send your comments and suggestions to me at rrs _AT_
researchut |DOT| com  
If you come across any bug [misbehavior, feature request], file them at the
Bug tracker.  
There's also a mailing list available at
https://lists.sourceforge.net/lists/listinfo/pypt-offline-general  
  
  
Thanks,  
Ritesh Raj Sarraf

