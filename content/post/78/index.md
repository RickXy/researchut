{
    "title": "luvcview Debian Package",
    "date": "2006-11-22T21:25:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages",
        "Tools"
    ]
}

luvcview is a camera viewer for UVC driver based webcams

I've packaged a .deb for it which can be fetched from:

http://mentors.debian.net/cgi-bin/sponsor-
pkglist?action=details;package=luvcview

