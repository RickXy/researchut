{
    "title": "apt-offline - 1.3",
    "date": "2012-12-31T12:16:07-05:00",
    "lastmod": "2014-11-12T05:44:30-05:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "apt-offline GUI"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Programming"
    ],
    "url": "/blog/apt-offline-1.3"
}

It is still 2012 in this part of the world and the world is still intact.
Since nothing major happened, I thought of spending the new gifted time to add
a long pending item to apt-offline. As shown in the screen shots, apt-
offline's GUI now has support to detect and display the downloaded offline bug
reports. ![](/sites/default/files/bug%20report1.jpeg)  
  
  
![](/sites/default/files/bug%20report2.jpeg)  
  
  
This is part of the just released, **version 1.3**.

