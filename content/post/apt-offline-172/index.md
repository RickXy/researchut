{
    "title": "apt-offline 1.7.2 released",
    "date": "2016-11-10T12:01:26-05:00",
    "lastmod": "2016-11-10T12:01:26-05:00",
    "draft": "false",
    "tags": [
        "apt",
        "apt-offline",
        "apt-offline-gui"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/apt-offline-172"
}

I am happy to announce the release of [apt-offline
1.7.2](https://github.com/rickysarraf/apt-offline/releases/tag/v1.7.2). This
has turned out in time for the next release of Debian, i.e. Debian Stretch.

A long standing cosmetic issue in CLI of the progress bar total item count has
been fixed. There are also a bunch of other bug fixes, for which the specifics
are present in the git logs.

Also, in this release, we've tried to catch-up on the Graphical Interface,
adding the GUI equivalent of the features, that were added to apt-offline in
the recent past.



In 1.7.1, we added the changelog option, and in this release, we've added the
GUI equivalent of it.

![](/sites/default/files/Screenshot%20from%202016-11-10%2022-10-37.png)



The 'set' command had had many new options. So in this release the 'set'
command's GUI has those options added.

![](/sites/default/files/Screenshot%20from%202016-11-10%2022-09-57.png)



I hope you like this release. Please test and file bug reports in time so that
we have a bug free version for Debian Stretch.

Future tasks now will focus on porting the GUI to PyQt5 first and then apt-
offline to Python3. Any help is welcome.

You can download apt-offline from the [github](https://github.com/rickysarraf
/apt-offline) page or the [alioth](http://apt-offline.alioth.debian.org) page.
For Debian users, the package will show up in the repository soon.

PS: What is apt-offline ?

    
    
    Description: offline APT package manager
     apt-offline is an Offline APT Package Manager.
     .
     apt-offline can fully update and upgrade an APT based distribution without
     connecting to the network, all of it transparent to APT.
     .
     apt-offline can be used to generate a signature on a machine (with no network).
     This signature contains all download information required for the APT database
     system. This signature file can be used on another machine connected to the
     internet (which need not be a Debian box and can even be running windows) to
     download the updates.
     The downloaded data will contain all updates in a format understood by APT and
     this data can be used by apt-offline to update the non-networked machine.
     .
     apt-offline can also fetch bug reports and make them available offline.
    

