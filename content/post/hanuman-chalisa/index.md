{
    "title": "Shri Hanuman Chalisa – Meaning",
    "date": "2016-01-13T05:52:11-05:00",
    "lastmod": "2016-01-13T05:52:11-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "RAM",
        "hanuman"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/hanuman-chalisa"
}

**[![lord_hanuman](/sites/default/files/lord_hanuman.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/lord_hanuman.jpg)**

**|| श्री हनुमान चालीसा ||**

 **दोहा (Doha)**

 **श्री गुरु चरन सरोज रज निजमनु मुकुरु सुधारि ।  
बरनऊ रघुबर बिमल जसु जो दायकु फल चारि ॥**

 **Shri guru charan saroj raj, Nij manu mukur sudhare |  
Barnau raghubar bimal jasu, Jo dhayak phal chare ||**

"Before I narrate the divine glory of Sri Ramachandra, the most supreme in the
Raghu dynasty, the giver of four fruits of mankind, let me first clean the
mirror of my mind with the dust of Shri Guru's lotus feet"

 **बुद्धिहीन ननु जानिके सुमिरौ पवन कुमार ।  
बल बुद्धि विद्या देहु मोहि हरहु कलेस बिकार् ॥**

 **Budhihien tanu jaanke, Sumerao pavan-kumar |  
Bal budhi vidhya dehu mohe, Harhu kales bikar ||**

"O Hanuman, O Lord, as I am ignorant and lack of wisdom, I pray to you to take
away all my woes and worries and shower on me wisdom, strength and knowledge."

[![rama_sita_hanuman](/sites/default/files/rama_sita_hanuman.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/rama_sita_hanuman.jpg)

चौपाई (Chopai)

 **जय हनुमान ज्ञान गुण सागर ।  
जय कपीश तिहु लोक उजागर ॥ 1 ॥**

 **Jai hanuman gyan gun sagar |  
Jai kapise tehu lok ujagar || 1 ||**

"Hail, O Lord of the monkeys, who is well known in all the three worlds and is
a vast sea of wisdom and virtue."

 **रामदूत अतुलित बलधामा ।  
अञ्जनि पुत्र पवनसुत नामा ॥ 2 ॥**

 **Ram dut atulit bal dhama |  
Anjani putra pavan sut nama || 2 ||**

"Victory to you, Lord Rama's messenger, who embodies strength beyond
comparison and who is also known as Pavanputra and Anjaniputra."

 **महावीर विक्रम बजरङ्गी ।  
कुमति निवार सुमति के सङ्गी ॥3 ॥**

 **Mahabir bikram bajragee |  
Kumati nivas sumati ke sangi || 3 ||**

"Banish all evil and wrong thoughts O Mahaveer, you who is like the lightening
and thunder, strong and powerful and ushers wisdom."

 **कञ्चन वरण विराज सुवेशा ।  
कानन कुण्डल कुञ्चित केशा ॥ 4 ॥**

 **Kanchan baran biraj subesa |  
Kann kundal kunchit kesa || 4 ||**

"Your skin is like gold and you look beautiful with your curly hair and ear-
rings."

 **हाथवज्र औ ध्वजा विराजै ।  
कान्थे मूञ्ज जनेऊ साजै ॥ 5॥**

 **Haat vajar ao dheja biraje |  
Kandhe muj janeu sajee || 5 ||**

"You have a flag in one hand and a thunderbolt mace in the other and your
shoulders are endowed with the sacred thread of munja."

 **शङ्कर सुवन केसरी नन्दन ।  
तेज प्रताप महाजग वन्दन ॥ 6 ॥**

 **Sankar suvan kesrinandan |  
Tej pratap maha jag bandhan || 6 ||**

"You are Lord Shiva's descendant, the son of Kesari. and the whole world
worships you."

 **विद्यावान गुणी अति चातुर ।  
राम काज करिवे को आतुर ॥ 7 ॥**

 **Vidhyavan gune aati chatur |  
Ram kaaj karive ko aatur || 7 ||**

"Forever at Lord Rama's feet ready to serve him, you are full of virtue and
wisdom, and the master of all knowledge."

 **प्रभु चरित्र सुनिवे को रसिया ।  
रामलखन सीता मन बसिया ॥ 8॥**

 **Prabhu charit sunibe ko rasiya |  
Ram lakhan sita maan basiya || 8 ||**

"Dwelling in your heart are Lord Rama, Laxman and Sita and you are devoted,
listening to hymns on the lord."

 **सूक्ष्म रूपधरि सियहिं दिखावा ।  
विकट रूपधरि लङ्क जरावा ॥ 9 ॥**

 **Susham roop dhari siyahi dhikhava |  
Bikat roop dhari lank jarava || 9 ||**

"You became demonic and demolished the city of Lanka, but at the same time you
revealed your meek side to Sita."

 **भीम रूपधरि असुर संहारे ।  
रामचन्द्र के काज संवारे ॥ 10 ॥**

 **Bhim roop dhari asur sahare |  
Ramchandra ke kaaj savare || 10 ||**

"You embellished Lord Rama's missions and destroyed the demons."

 **लाय सञ्जीवन लखन जियाये ।  
श्री रघुवीर हरषि उर लाये ॥ 11 ॥**

 **Laye sanjeevan lakhan jiyaye |  
Shriraghuvir harsha ure laye || 11 ||**

"Sri Ramachandra was very pleased and embraced you with utmost joy when you
revived Laxman with Sanjivini."

 **रघुपति कीन्ही बहुत बडाई ।  
तुम मम प्रिय भरतहि सम भाई ॥ 12 ॥**

 **Raghupati kinhe bahut badai |  
Tum mam preye bharathisam bhai || 12 ||**

"He also praised you, giving you honour and considers you as dear as Bharat,
his own brother."

 **सहस वदन तुम्हरो जास गावै ।  
अस कहि श्रीपति कण्ठ लगावै ॥ 13 ॥**

 **Sahas badana tumarho jas gavey |  
Aas kahi shripati kant lagavey || 13 ||**

"Sri ram embraced you saying that Shesha Naga sings of his glory."

 **सनकादिक ब्रह्मादि मुनीशा ।  
नारद शारद सहित अहीशा ॥ 14 ॥**

 **Sanakadika bhramadhi munisa |  
Narad sarad sahit ahisa || 14 ||**

"Other Gods like Sanaka, Brahma and other sages like Narad, Sharada forever
sing your praises eternally."

 **जम(यम) कुबेर दिगपाल जहां ते ।  
कवि कोविद कहि सके कहां ते ॥ 15 ॥**

 **Jam kuber digpal jaha the |  
Kavi kovid kahi sake kaha the || 15 ||**

"Words fail Yama, Kuber, Digpal in praise of your glory."

 **तुम उपकार सुग्रीवहि कीन्हा ।  
राम मिलाय राजपद दीन्हा ॥ 16 ॥**

 **Tum upkar sugrivahim kehina |  
Ram milaye raj pad denha || 16 ||**

"With Lord Rama's blessings you did a big favour for Sugriva by regaining his
crown."

 **तुम्हरो मन्त्र विभीषण माना ।  
लङ्केश्वर भए सब जग जाना ॥ 17 ॥**

 **Tumraho mantra vibhishana mana |  
Lankeshvar bhaye sab jag jaan || 17 ||**

"Vibhishana became Lanka's king after accepting your council and this is known
across the world."

 **युग सहस्र योजन पर भानू ।  
लील्यो ताहि मधुर फल जानू ॥ 18 ॥**

 **Jug sahes jojan per bhanu |  
Linyo tahi madhur phal janu || 18 ||**

"Thinking it was a sweet fruit you swallowed the sun which is millions of
miles away."

 **प्रभु मुद्रिका मेलि मुख माही ।  
जलधि लाङ्घि गये अचरज नाही ॥ 19 ॥**

 **Prabhu mudrika meli mukh mahi |  
Jaldhi ladhi gaye acraj nahi || 19 ||**

"In search of Sita you swiftly crossed mighty oceans with the Lord's ring in
your mouth."

 **दुर्गम काज जगत के जेते ।  
सुगम अनुग्रह तुम्हरे तेते ॥ 20 ॥**

 **Durgam kaaj jagat ke jete |  
Sugam anugrah tumre tete || 20 ||**

"All the problems in this world can be solved by your power and grace."

 **राम दुआरे तुम रखवारे ।  
होत न आज्ञा बिनु पैसारे ॥ 21 ॥**

 **Ram duaare tum rakhvare |  
Hoot na aagya binu pasare || 21 ||**

"Without your mandate no one is allowed to enter Lord Rama's humble adobe."

 **सब सुख लहै तुम्हारी शरणा ।  
तुम रक्षक काहू को डर ना ॥ 22 ॥**

 **Sab sukh lahai tumhre sarna |  
Tum rakshak kahu ko daarna || 22 ||**

"Under your protecting hands, nobody needs to fear anything and each and
everyone can be happy."

 **आपन तेज तुम्हारो आपै ।  
तीनों लोक हाङ्क ते काम्पै ॥ 23 ॥**

 **Aapan tej samharo aape |  
Teno lok hakte kape || 23 ||**

"You alone can deliver from your might and when you call out loud the three
worlds tremble and shiver."

 **भूत पिशाच निकट नहि आवै ।  
महवीर जब नाम सुनावै ॥ 24 ॥**

 **Bhut pesach nikat nahi aaveh |  
Mahavir jab naam sunaveh || 24 ||**

"Whoever chants your name, evil spirits will not go anywhere near them, your
devotees."

 **नासै रोग हरै सब पीरा ।  
जपत निरन्तर हनुमत वीरा ॥ 25 ॥**

 **Nase rog hare sab peera |  
Japat nirantar hanumat bal bira || 25 ||**

"All diseases and illnesses can be cured if one chants your name in prayer."

 **सङ्कट तें(सें) हनुमान छुडावै ।  
मन क्रम वचन ध्यान जो लावै ॥ 26 ॥**

 **Sankat se hanuman chudave |  
Maan kram bachan dayan jo lavey || 26 ||**

"Those who keep Lord Hanuman in their hearts, in their deeds, in their words,
in their meditations will be kept safe from all distress and troubles."

 **सब पर राम तपस्वी राजा ।  
तिनके काज सकल तुम साजा ॥ 27 ॥**

 **Sab per ram tapasvi raja |  
Tin ke kaaj sakal tum saja || 27 ||**

"You accomplished all the missions of Lord Rama, the ruler of all."

 **और मनोरध जो कोइ लावै ।  
सोई अमित जीवन फल पावै ॥ 28 ॥**

 **Aur manorat jo kayi lave |  
Tasuye amit jeevan phal pavey || 28 ||**

"All wishes and desires will be granted to those who come to you and they will
be faithful n life."

 **चारो युग परिताप तुम्हारा ।  
है परसिद्ध जगत उजियारा ॥ 29 ॥**

 **Charo yuj pratap tumarah |  
Hai prasidh jagat ujiyara || 29 ||**

"Your radiance is wide spread all over the cosmos and acclamation of your
glory is done in all the four ages."

 **साधु सन्त के तुम रखवारे ।  
असुर निकन्दन राम दुलारे ॥ 30 ॥**

 **Sadhu sant ke tum rakhvare |  
Asur nikandan ram dulare || 30 ||**

"O Mahaveer, the slayer of evil spirits and protector of saints and sages,
Lord Rama has great affection for you."

 **अष्ठसिद्धि नौ(नव) निधि के दाता ।  
अस वर दीन्ह जानकी माता ॥ 31 ॥**

 **Ashat sidhi navnidhi ke data |  
As var deen jaanki mata || 31 ||**

"You have the power to grant anyone, one of the eight sidhis or of the nine
sidhis by the blessings of Mother Sita."

 **राम रसायन तुम्हारे पासा ।  
साद रहो रघुपति के दासा ॥ 32 ॥**

 **Ram rasayan tumhre pasa |  
Sada raho raghupati ke dasa || 32 ||**

"You are always ready to serve Lord Raghupati"

 **तुम्हरे भजन रामको पावै ।  
जनम जनम के दुख बिसरावै ॥ 33 ॥**

 **Tumreh bhajan ram ko bhavey |  
Janam janam ke dukh bisravey || 33 ||**

"Anyone can become free from the sorrows of many lives and reach Lord Rama by
praying to you."

 **अन्त काल रघुवर पुरजाई ।  
जहां जन्म हरिभक्त कहाई ॥ 34 ॥**

 **Ant kaal raghubar pur jaie |  
Jaha janam hari bhagat kahaei || 34 ||**

"By your grace one will enter the immortal adobe of Lord Rama after death and
remain devoted to him."

 **और देवता चित्त न धरई ।  
हनुमत सेइ सर्व सुख करई ॥ 35 ॥**

 **Aur devta chitna dhareyo |  
Hanumat seye sarav sukh karaei || 35 ||**

"Those who serve you will rejoice and enjoy all pleasures and won't need to
seek any other Gods in their hearts."

 **सङ्कट कटै मिटै सब पीरा ।  
जो सुमिरै हनुमत बल वीरा ॥ 36 ॥**

 **Sankat kate mite sab pera |  
Jo sumere hanumat balbira || 36 ||**

"Cast away all difficulties and pains of those who revere you, Lord."

 **जै जै जै हनुमान गोसाई ।  
कृपा करो गुरुदेव की नाई ॥ 37 ॥**

 **Jai jai jai hanuman gusai |  
Kripa karo guru dev ke nai || 37 ||**

"Praises and glory to you O mighty Lord, and be like my Guru, compassionate
and kind."

 **जो शत वार पाठ कर कोई ।  
छूटहि बन्दि महा सुख होई ॥ 38 ॥**

 **Jo sat bar pat kar koi |  
Chutehi bandhi maha sukh hoi || 38 ||**

"Whoever chants this prayer a 100 times will attain freedom from wordly
sorrows and his happiness sublimes."

 **जो यह पडै हनुमान चालीसा ।  
होय सिद्धि साखी गौरीशा ॥ 39 ॥**

 **Jo yahe pade hanuman chalisa |  
Hoye sidhi sa ke gauisa || 39 ||**

"Those who recite these 40 verses will be free from all problems."

 **तुलसीदास सदा हरि चेरा ।  
कीजै नाथ हृदय मह डेरा ॥ 40 ॥**

 **Tulsidas sada hari chera |  
Kijeye nath hridaye maha dera || 40 ||**

"Tulsidas will be forever a faithful devotee to Lord Hari. O Hanuman, do stay
in my heart always'."

[![anjaneya](/sites/default/files/anjaneya.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/anjaneya.jpg)

 **दोहा (Doha)**

 **पवन तनय सङ्कट हरण - मङ्गल मूरति रूप् ।  
राम लखन सीता सहित - हृदय बसहु सुरभूप् ॥**

 **Pavantnaye sankat haran, Mangal murti roop |  
Ram lakhan sita sahet, Hridaye basau sur bhup ||**

"O conqueror of the Wind, destroyer of all miseries, embodiment of
auspiciousness, along with Sri Rama, Laxman and Sita say in my heart."

[![new
2](/sites/default/files/new-2.gif)](https://vibhormahajan.files.wordpress.com/2012/06/new-2.gif)



 **Copyright (C) Vibhor Mahajan**

