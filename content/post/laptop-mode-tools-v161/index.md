{
    "title": "Laptop Mode Tools - 1.61",
    "date": "2012-05-17T08:56:29-04:00",
    "lastmod": "2013-01-31T13:44:53-05:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-v161"
}

Laptop Mode Tools, version 1.61, has been released and will land up soon for
Debian. This is the version that would be targetting Wheezy.  
This release includes many bug fixes and should make power savings much better
on your machines.

This is mainly a bug fix release. Some parallel module execution approach has
been used which could show runtime improvements.

Changelog:

  
 **1.61** \- Thu May 17 17:44:26 IST 2012  
    * Handle devices with persistent device naming. This fixes the issues where  
      you don't have a disk referenced by a block name, the commit= value was  
      completely skipped  
    * Fix issue where hdparm skips SSDs for power management  
    * Add parallel execution for the modules. In theory this should speeden up the  
      execution. See git commit log comments for details  
    * Add support for non-deafult customized settings  
    * calculate design_capacity_warning on machines/arches where it is not readily  
      available  
  
We have switched the SCM to git. The current code repository is  
available at [1] along with the changelog.  
  
The tarball is available here [2].  
The md5 checksum for the tarball is 6685af5dbb34c3d51ca27933b58f484e  
  
[1] <https://github.com/rickysarraf/laptop-mode-tools>  
[2]<http://samwel.tk/laptop_mode/tools/downloads/laptop-mode-
tools_1.61.tar.gz>

