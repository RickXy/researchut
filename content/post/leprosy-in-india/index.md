{
    "title": "Leprosy in India",
    "date": "2016-07-10T14:06:17-04:00",
    "lastmod": "2016-07-10T19:48:54-04:00",
    "draft": "false",
    "tags": [
        "41000 feet",
        "Qatar",
        "Debconf",
        "Leprosy"
    ],
    "categories": [
        "Debian-Blog",
        "General"
    ],
    "url": "/blog/leprosy-in-india"
}

During my recent travel, I had quite a long layover at the Doha International
Airport in Qatar. While killing time, I watched an interesting programme on
the Al Jazeera network.

The program aired on Al Jazeera is
[**Lifelines**](http://www.aljazeera.com/programmes/lifelines/). This special
episode I watched, covered about "Leprosy in India". After having watched the
entire programme, I felt the urge to blog about it.

First of all, it was quite depressing to me, to know through the programme,
that the Govt. of India had officially marked "Leprosy" eradicated from India
in 2005. As per [Wikipedia](https://en.wikipedia.org/wiki/Leprosy), "Globally
in 2012, the number of chronic cases of leprosy was 189,000, down from some
5.2 million in the 1980s. The number of new cases was 230,000. Most new cases
occur in 16 countries, with India accounting for more than half."

Of the data presented on Lifelines, they just covered a couple of districts
from 2 States (of the 28+ States) of India. So, with many states remaining,
and unserveyed, and uncounted, we are far away from making such statements.

Given that the Govt., on paper, has marked Leprosy eradicated; so does WHO.
Which means that there is no more funding to help people suffering from the
disease. And with no Govt. (or International) funding, it is a rather
disappointing situation.

I come from a small town on the Indo-Nepal intl. border, named Raxaul. And
here, I grew up seeing many people who suffered from Leprosy. As a child, I
never much understood the disease, because as is mentioned in the programme, I
was told it was a communicable disease. Those suffering, were (and are) tagged
as Untouchables (Hindi:अछूत). Of my small town, there was and still is a sub-
town, named Sunderpur. This town houses patients suffering from Leprosy, from
around multiple districts closeby. I've never been there, but have been told
that it is run by an NGO, and helps patients fight the disease.

  
Lifelines also reported that fresh surveys done by the Lepra society, just a 3
day campaign, resulted in 3808 new cases of people suffering from Leprosy.
That is a big number, given accessibility to small towns only happens once a
week on market day. And in 3 days the team hardly covered a couple of district
towns.

  
My plea to the Media Houses of India. Please spend some time to look beyond
the phony stuff that you mostly present. There are real issues that could be
brought to a wider audience. As for the government, it is just depressing to
know how rogue some/most of your statements are.

