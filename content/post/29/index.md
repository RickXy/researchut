{
    "title": "Motorola S9 Bluetooth Headset",
    "date": "2009-02-12T17:21:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "bluetooth"
    ],
    "categories": [
        "Debian-Pages",
        "Technology"
    ]
}

So my friend game me a cute little birthday present, a Motorola S9 Bluetooth
Headset.

Making it work under Linux was not very difficult but still has to catch-up in
terms of utils.

To make the headset work, I just had to run a scan on the Linux host, and get
the hw address and specify it in .asoundrc.

> pcm.bluetooth {  
>      type bluetooth  {  
>          address = xx.xx.xx.xx.xx  
>          description "Motorola S9 Bluetooth Headset"  
>      }  
>  }  
>

 And then to play a file, like in mplayer, you just specify the alsa device.

>  mplayer -ao alsa:device=bluetooth foo.ogg

>  

KDE's Phonon currently doesn't show the Bluetooth Headset even if it is
paired.

PulseAudio doesn't work good. (I couldn't make it work in my limited attempt)

