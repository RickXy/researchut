{
    "title": "Laptop Mode Tools - 1.60",
    "date": "2011-10-14T04:36:59-04:00",
    "lastmod": "2013-01-31T13:47:13-05:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ]
}

Hello World,  
  
I am very pleased to announce the release of Laptop Mode Tools, version 1.60.  
This release includes lots of bug fixes and should make power savings much
better on your machines.The battery polling daemon now, more reliably, gets
triggered and killed based on power states.



  
This release also includes 2 helper scripts to trigger suspend and hibernate,
in case anyone is interested.

Given the advancements of Linux PM ( **All thanks to Rafael J. Wysocki** ) in
recent years, the freezer/thawfunctionality really does a very good job of
handling suspend/hibernate, there is no need of a hacky
suspend/resumemechanism. Thus, you'll notice the helper scripts just do a mere
echo into sysfs.  
  
I would also like to thank the Chromium project that has found and fixed many
bugs and added many enhancements to Laptop Mode Tools.



  
Changelog:  
  

1.60 \-- Fri Oct 14 13:08:09 IST 2011

  * Use proper device reference for iwconfig (Debian BTS: #639388)
  * Check for block device's existence. Thanks to Simon Que
  * Add suspend/resume helper tools: pm-helper, pm-suspend, pm-hibernate
  * What laptop-mode-tools is stopped from init, also kill polling daemon
  * Reliable and much better locking mechanics
  * Make polling dameon lock safe
  * Make lmt-udev distro neutral. Thanks to Simon Que
  * Change Intel HDA Audio's default power save timeout to 2 seconds

  
  
We have switched the SCM to git. The current code repository is  
available at [1] along with the changelog.  
  
  
The tarball is available here [2].  
The md5 checksum for the tarball is 22bcc955c4e5d28e2f3a992b0efb50b4  
  
[1] <https://github.com/rickysarraf/laptop-mode-tools>  
[2]<http://samwel.tk/laptop_mode/tools/downloads/laptop-mode-
tools_1.60.tar.gz>

