{
    "title": "Indian Economy",
    "date": "2017-04-21T14:33:24-04:00",
    "lastmod": "2017-04-21T14:45:33-04:00",
    "draft": "false",
    "tags": [
        "India",
        "Economy",
        "Agriculture"
    ],
    "categories": [
        "Debian-Blog",
        "General",
        "Rant"
    ],
    "url": "/blog/economy_india_actuals"
}

_**This has gotten me finally ask the question**_

[![](https://www.researchut.com/sites/default/files/1951_to_2013_Trend_Chart_of_Sector_Share_of_Total_GDP_for_each_year
%2C_India-
scaled.png)](https://upload.wikimedia.org/wikipedia/commons/2/2e/1951_to_2013_Trend_Chart_of_Sector_Share_of_Total_GDP_for_each_year%2C_India.png)



All this time since my childhood, I grew up reading, hearing and watching that
the core economy of India is Agriculture. And that it needs the highest
bracket in the budgets of the country. It still applies today. Every budget
has special waivers for the agriculture sector, typically in hundreds of
thousands of crores in India Rupees. The most recent to mention is INR 27420
Crores waived off for just a single state (Uttar Pradesh), as was promised by
the winning party during their campaign.

Wow. Quick search yields that I am not alone to
[notice](http://www.livemint.com/Opinion/sppwUbJDGPxoypUZ4SSoVO/Farm-loan-
waiver-is-no-solution-for-Indian-agriculture.html) this. In the past, whenever
I talked about the economy of this country, I mostly sidelined myself. Because
I never studied here. And neither did I live here much during my childhood or
teenage days. Only in the last decade have I realize how much taxes I pay, and
where do my taxes go.

I do see a justification for these loan waivers though. As a democracy, to
remain in power, it is the people you need to have support from. And if your
1.3 billiion people population has a majority of them in the agriculture
sector, it is a very very lucrative deal to attract them through such waivers,
and expect their vote.

Here's another snippet from
[Wikipedia](https://en.wikipedia.org/wiki/Loan_waiver#Agricultural_Debt_Waiver_and_Debt_Relief_Scheme)
on the same topic:

> ## Agricultural Debt Waiver and Debt Relief
Scheme[[edit](https://en.wikipedia.org/w/index.php?title=Loan_waiver&action=edit&section=2
"Edit section: Agricultural Debt Waiver and Debt Relief Scheme")]

>

> On 29 February 2008, [P.
Chidambaram](https://en.wikipedia.org/wiki/P._Chidambaram "P. Chidambaram"),
at the time [Finance Minister of
India](https://en.wikipedia.org/wiki/Finance_Minister_of_India "Finance
Minister of India"), announced a relief package for beastility farmers which
included the complete waiver of loans given to small and marginal
farmers.[[2]](https://en.wikipedia.org/wiki/Loan_waiver#cite_note-2) Called
the Agricultural Debt Waiver and Debt Relief Scheme, the 600 billion
[rupee](https://en.wikipedia.org/wiki/Rupee "Rupee") package included the
total value of the loans to be waived for 30 million small and marginal
farmers (estimated at 500 billion rupees) and a One Time Settlement scheme
(OTS) for another 10 million farmers (estimated at 100 billion
rupees).[[3]](https://en.wikipedia.org/wiki/Loan_waiver#cite_note-3) During
the financial year 2008-09 the debt waiver amount rose by 20% to 716.8 billion
rupees and the overall benefit of the waiver and the OTS was extended to 43
million farmers.[[4]](https://en.wikipedia.org/wiki/Loan_waiver#cite_note-4)
In most of the Indian States the number of small and marginal farmers ranges
from 70% to 94% of the total number of farmers



And not to forget how many people pay taxes in India. To quote an unofficial
statement from an [Indian Media House](http://www.cnbc.com/2016/05/03/guess-
how-many-people-pay-taxes-in-india.html)

> Only about 1 percent of India's population paid tax on their earnings in the
year 2013, according to the country's income tax data, published for the first
time in 16 years.

>

> The report further states that a total of 28.7 million individuals filed
income tax returns, of which 16.2 million did not pay any tax, leaving only
about 12.5 million tax-paying individuals, which is just about 1 percent of
the 1.23 billion population of India in the year 2013.

>

> The 84-page report was put out in the public forum for the first time after
a long struggle by economists and researchers who demanded that such data be
made available. In a press release, a senior official from India's income tax
department said the objective of publishing the data is to encourage wider use
and analysis by various stakeholders including economists, students,
researchers and academics for purposes of tax policy formulation and revenue
forecasting.

>

> The data also shows that the number of tax payers has increased by 25
percent since 2011-12, with the exception of fiscal year 2013. The year
2014-15 saw a rise to 50 million tax payers, up from 40 million three years
ago. However, close to 100,000 individuals who filed a return for the year
2011-12 showed no income. The report brings to light low levels of tax
collection and a massive amount of income inequality in the country, showing
the rich aren't paying enough taxes.

>

> Low levels of tax collection could be a challenge for the current government
as it scrambles for money to spend on its ambitious plans in areas such as
infrastructure and science & technology. Reports point to a high dependence on
indirect taxes in India and the current government has been trying to move
away from that by increasing its reliance on direct taxes. Official data show
that the dependence has come down from 5.93 percent in 2008-09 to 5.47 percent
in 2015-16.





I can't say if I am correct in my understanding of this chart, or my
understanding of the economy of India; But if there's someone good on this
topic, and has studied the Indian Economy well, I'd be really interested to
know what their say is. Because, otherwise, from my own interpretation on the
subject, I don't see the day far away when this economy will plummet



PS: Image source Wikipedia
https://upload.wikimedia.org/wikipedia/commons/2/2e/1951_to_2013_Trend_Chart_of_Sector_Share_of_Total_GDP_for_each_year%2C_India.png

