{
    "title": "ctime, atime and mtime",
    "date": "2006-10-12T23:22:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Computing",
        "Technology"
    ]
}

**ctime, atime and mtime**

It is important to distinguish between a file or directory 's change time
(ctime), access time (atime), and modify time (mtime).

 **ctime** \-- In UNIX, it is not possible to tell the actual creation time of
a file. The ctime--change time--is the time when changes were made to the file
or directory 's inode (owner, permissions, etc.). It is needed by the dump
command to determine if the file needs to be backed up. You can view the ctime
with the ls -lc command.

 **atime** \-- The atime--access time--is the time when the data of a file was
last accessed. Displaying the contents of a file or executing a shell script
will update a file 's atime, for example. You can view the atime with the ls
-lu command.

 **mtime** \-- The mtime--modify time--is the time when the actual contents of
a file was last modified. This is the time displayed in a long directoring
listing (ls -l).

In Linux, the stat command will show these three times.

This text was taken from
[bhutch's](http://www.brandonhutchinson.com/ctime_atime_mtime.html) site.

