{
    "title": "User Mode Linux",
    "date": "2016-07-31T13:15:28-04:00",
    "lastmod": "2016-08-01T06:39:33-04:00",
    "draft": "false",
    "tags": [
        "UML",
        "User Mode Linux",
        "debian"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools",
        "Virtualization"
    ],
    "url": "/blog/user-mode-linux-debian"
}

Recently, we had the User-Mode Linux suite out of Debian, which included user-
mode-linux, user-mode-linux-doc and uml-utilities package. We are happy that
we were able to bring it back into the archvie quick, and hope to maintain it
active.

For many who may not know about UML, here's a discription from its
[website](http://user-mode-linux.sourceforge.net/old/):

    
    
    User-Mode Linux is a safe, secure way of running Linux versions and Linux processes. Run buggy software, experiment with new Linux kernels or distributions, and poke around in the internals of Linux, all without risking your main Linux setup.
    
    User-Mode Linux gives you a virtual machine that may have more hardware and software virtual resources than your actual, physical computer. Disk storage for the virtual machine is entirely contained inside a single file on your physical machine. You can assign your virtual machine only the hardware access you want it to have. With properly limited access, nothing you do on the virtual machine can change or damage your real computer, or its software.



Most of the use cases mentioned here are achievable with Containers today. The
big difference UML provides in, is with a separate kernel. UML is an
implementation of Linux as an architecture of Linux itself. It supports x86
and x86_64 architecture. And given that it is the port of the kernel, you can
do many of the tests and experiments of the regular kernel, safely inside a
confined UML environment. As with other virtualization implementations, the
limitation comes in if you are working on physical hardware.



With its re-entry in Debian, I wanted to revive my local setup. First is the
packaging structure and the second is its integration with current standard
tools

  * **Packaging** : For packaging UML in Debian, we rely on the packaged linux-source package provided by the kernel team. 
    * **linux-source package** : We build UML from the linux sources that are provided by the debian kernel team. This works fine for now. Whenever there's a kernel vulnerability, there'll be an updated source package, to which we'll rebuild the UML package.
    * **Merge with debian-kernel** : In the longer run, we'd like to push UML package into the debian kernel team. UML is a component of the Linux kernel, and that is where it should be built from. 
  * **Integration** : It works fairly well right now. On modern systems with systemd, where (nspawn) containers can easily have a network interface bound to it, UML lacks a bit behind. It'd be nice if we could see some UML integration with systemd. 
    * Networking under systemd: Setting up networking, for UML, under systemd is fairly straight. In fact, with systemd, it is much simpler. Below is the host network (tuntap) setup, to which UML can bind for all its network needs.

    
    
    rrs@learner:~/tidBits (master)$ cat /etc/systemd/network/tap.netdev 
    [NetDev]
    Name=tap0
    Kind=tap
    
    [TAP]
    Group=uml-net
    User=uml-net
    2016-08-01 / 15:41:40 ♒♒♒  ☺  
    rrs@learner:~/tidBits (master)$ cat /etc/systemd/network/tap.network 
    [Match]
    Name=tap0
    
    [Network]
    DHCPServer=yes
    IPForward=yes
    IPMasquerade=yes
    Address=172.16.20.2
    LLMNR=yes
    MulticastDNS=yes
    DNS=172.16.20.1
    2016-08-01 / 15:41:43 ♒♒♒  ☺  
    

systemd allows for defining user/group ownership in its file.  With this
setup, and uml-utilities running, one can simply fire a UML instance as below:

    
    
    rrs@learner:~/rrs-home/Community/Packaging/user-mode-linux (master)$ linux ubd0=~/rrs-home/NoTrack/uml.img eth0=daemon mem=1024M rw  
    Core dump limits :
        soft - 0
        hard - NONE
    Checking that ptrace can change system call numbers...OK
    Checking syscall emulation patch for ptrace...OK
    Checking advanced syscall emulation patch for ptrace...OK
    Checking environment variables for a tempdir...none found
    Checking if /dev/shm is on tmpfs...OK
    Checking PROT_EXEC mmap in /dev/shm...OK
    Adding 23609344 bytes to physical memory to account for exec-shield gap
    Linux version 4.6.3 (root@chutzpah) (gcc version 5.4.0 20160609 (Debian 5.4.0-6) ) #2 Sat Jul 16 16:22:22 UTC 2016
    Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 263721
    Kernel command line: ubd0=/home/rrs/rrs-home/NoTrack/uml.img eth0=daemon mem=1024M rw root=98:0
    PID hash table entries: 4096 (order: 3, 32768 bytes)
    Dentry cache hash table entries: 262144 (order: 9, 2097152 bytes)
    Inode-cache hash table entries: 131072 (order: 8, 1048576 bytes)
    Memory: 1020852K/1071632K available (4803K kernel code, 1207K rwdata, 1340K rodata, 157K init, 217K bss, 50780K reserved, 0K cma-reserved)
    SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=1, Nodes=1
    NR_IRQS:15
    .....snipped.......
    
    root@uml:~# cat /proc/cpuinfo 
    processor    : 0
    vendor_id    : User Mode Linux
    model name    : UML
    mode        : skas
    host        : Linux learner 4.6.0-1-amd64 #1 SMP Debian 4.6.4-1 (2016-07-18) x86_64
    bogomips    : 6048.97
    
    root@uml:~# ping www.debian.org
    PING www.debian.org (130.89.148.14) 56(84) bytes of data.
    64 bytes from klecker4.snt.utwente.nl (130.89.148.14): icmp_seq=1 ttl=46 time=372 ms
    64 bytes from klecker4.snt.utwente.nl (130.89.148.14): icmp_seq=2 ttl=46 time=395 ms
    64 bytes from klecker4.snt.utwente.nl (130.89.148.14): icmp_seq=3 ttl=46 time=315 ms
    ^C
    --- www.debian.org ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2002ms
    rtt min/avg/max/mdev = 315.550/361.064/395.440/33.556 ms
    



And here are some (incomplete and non-conclusive) performance numbers

    
    
    root@uml:~# dd if=/dev/zero of=foo.img bs=1M count=2500 conv=fsync
    2500+0 records in
    2500+0 records out
    2621440000 bytes (2.6 GB, 2.4 GiB) copied, 39.4876 s, 66.4 MB/s
    
    vs
    
    rrs@learner:/var/tmp/Debian-Build/Result$ dd if=/dev/zero of=foo.img bs=1M count=2500 conv=fsync
    2500+0 records in
    2500+0 records out
    2621440000 bytes (2.6 GB, 2.4 GiB) copied, 41.2126 s, 63.6 MB/s
    2016-08-01 / 15:59:15 ♒♒♒  ☺  
    

