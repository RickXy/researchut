{
    "title": "KMail HAM Handling",
    "date": "2008-05-26T17:03:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "kmail"
    ],
    "categories": [
        "KDE"
    ]
}

I have been a long time KMail user and love this email client. It has most of
the features that I care about, like Message Threading, Spam Filtering,
Disconnected IMAP et cetera.

KMail has good integration with SPAM Handling softwares like **spamassassin**
and **bogofilter**. The KMail wizard will autoconfigure spamassassin,
bogofilter and others (as detected from your installation). The default
settings were a little annoying because messages detected as SPAM were re-
written with the SPAM Heading. The problem was when spamassassin mis-judged a
genuine message as SPAM. Well, we could mark it as HAM, but the added heading
never went off.

To revert back the message to its original state, do the following:

  1. Select the **Classify as Not Spam filter**  
  2. Add a new filter action

  * **Pipe Through**
  * Add the following command in the text box, **spamassassin -d**

That's it. Now when you click on **Not SPAM** , the database gets updated and
the email message is restored back to its original format.

![](http://www.researchut.com/blog/images/ham1.png)

