{
    "title": "Why Debian",
    "date": "2005-06-24T18:45:00-04:00",
    "lastmod": "2011-02-15T15:42:56-05:00",
    "draft": "false",
    "tags": [
        "debian",
        "why debian"
    ],
    "categories": [
        "Debian-Pages"
    ]
}

A small [talk](/news/why_debian.html) I gave about Debian and its merits at
the ILGUD on 18th of April, 2004.

