{
    "title": "Linux SCSI Persistent Device Detection",
    "date": "2008-01-09T07:23:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools",
        "Computing"
    ]
}

Many of us might be aware of the **block level encryption features (LUKS for
example)** provided in Linux.

Most of the articles that I referred to over the internet mention the usage of
the traditional scsi block device when creating it as an ecrypted device.
Eg. sudo cryptsetup luksFormat /dev/sdb1

This actually poses a problem. The problem is with the way Linux detects SCSI
devices. While many distributions have shifted to using ID/LABEL based access
to devices, users still use the old traditional way of using a device. This
turns out to be a problem in cases like suspend/resume where devices are
rediscovered. Technically there is no guarantee that the device you see
currently as sda will be named sda again on reboot/resume. While we don't see
issues like this with attached disks, the issue can be easily reproduced in a
SAN Enviornment or in cases where users use external hard drives using USB.

I faced the same issue with my external USB Hard Drive which I use for
backups. This has an encrypted device to which I backup using a software like
**keep.** To rectify the proble, all I had to do to recreate the encrypted
device was to refer the SCSI device by its device ID.

sudo cryptsetup luksFormat /dev/disk/by-id/usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0-part2

This would now work across reboots/resumes because the device ID for the
device will not change and will remain persistent.

 Here's how the devices look in /dev:

rrs@learner:~$ ls -l /dev/disk/by-id/  
total 0  
lrwxrwxrwx 1 root root  9 2008-01-08 13:37 ata-Hitachi_HTS72106-MPCC14Y3GUX6LE
-> ../../sda  
lrwxrwxrwx 1 root root 10 2008-01-08 13:37 ata-Hitachi_HTS72106
-MPCC14Y3GUX6LE-part1 -> ../../sda1  
lrwxrwxrwx 1 root root 10 2008-01-08 13:37 ata-Hitachi_HTS72106
-MPCC14Y3GUX6LE-part2 -> ../../sda2  
lrwxrwxrwx 1 root root 10 2008-01-08 13:37 ata-Hitachi_HTS72106
-MPCC14Y3GUX6LE-part3 -> ../../sda3  
lrwxrwxrwx 1 root root  9 2008-01-08 13:37 scsi-S_MPCC14Y3GUX6LE -> ../../sda  
lrwxrwxrwx 1 root root 10 2008-01-08 13:37 scsi-S_MPCC14Y3GUX6LE-part1 ->
../../sda1  
lrwxrwxrwx 1 root root 10 2008-01-08 13:37 scsi-S_MPCC14Y3GUX6LE-part2 ->
../../sda2  
lrwxrwxrwx 1 root root 10 2008-01-08 13:37 scsi-S_MPCC14Y3GUX6LE-part3 ->
../../sda3  
lrwxrwxrwx 1 root root  9 2008-01-09 00:05 usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0 -> ../../sdc  
lrwxrwxrwx 1 root root 10 2008-01-09 00:05 usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0-part1 -> ../../sdc1  
lrwxrwxrwx 1 root root 10 2008-01-09 00:05 usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0-part2 -> ../../sdc2  
lrwxrwxrwx 1 root root 10 2008-01-09 00:05 usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0-part3 -> ../../sdc3  
lrwxrwxrwx 1 root root 10 2008-01-09 00:05 usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0-part4 -> ../../sdc4  
lrwxrwxrwx 1 root root 10 2008-01-09 00:05 usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0-part5 -> ../../sdc5  
lrwxrwxrwx 1 root root 10 2008-01-09 00:05 usb-WDC_WD12_00BEVE-
11UYT0_ST_Killer-0:0-part6 -> ../../sdc6

As you can notice, currently my **usb disk** is pointing to **sdc** where as
on a reboot it will return back to the proper order and would be listed as
**sdb**.

