{
    "title": "Samsung Galaxy S and Samsung Kies",
    "date": "2010-10-21T06:57:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "Kies",
        "Samsung Galaxy S"
    ],
    "categories": [
        "Tools"
    ]
}

Well most must be knowing by now that Samsung **is** rolling out the **Android
2.2 FROYO** update in phases. And it would be available through Kies.

There have been numerous reports of Kies not detecting the Samsung Galaxy S
phone.I think I have a pattern and know what has been causing the trouble.

**Samsung Kies** , by default, is set to auto-start and reside in the System
Tray. The Kies application auto-detects the phone when plugged-in and starts
up the applications. I guess this is where the problem lies.

In Windows (at least on my XP), when a PnP device is connected, Windows:

  * Detects the device
  * Loads driver and initiazlies the device
  * Executes appropriate action related to the device (Like open a folder if it is a flash stick)

The phones, when connected, are also detected by Windows as storage devices.
So, if we can make Windows have 1st access to the Samsung Galaxy S phone upon
connect, it can follow the above steps and detect the device.

This is where the catch is. On typical installations of Kies, it is already
running. So as soon as the phone gets plugged in, Kies tries to access the
phone first. I have no idea how bloated Kies is. Does it also hinder the
driver initialization of the phone ? From the looks of when Kies is running
and phone is plugged, yes.

So, simple steps to get your Samsung Galaxy S detected on Kies

  * Kill Kies
  * No Kies instance in System Tray
  * Connect Phone and select "Samsung Kies"
  * Let Windows detect the phone and initialize the device
  * Let Windows pop-up an action Window
  * After all this is done, now start Kies.

Leave a comment on the blog is this works for you.

