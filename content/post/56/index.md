{
    "title": "KDE4",
    "date": "2008-01-12T20:23:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "KDE"
    ]
}





![](http://static.kdenews.org/jr/kde-4.0-banner.png)



[**KDE 4.0 is out**](http://dot.kde.org/1200050369/)





Despite all the FUD that KDE4 won't stay up to the expectations of the users,
I must say KDE4 has impressed me a lot. It is definitely a lot more **faster**
than KDE 3.

The shiny new desktop with all the new core technologies of KDE 4 and the new
visual effects (kwin, plasma, oxygen), all make KDE 4 the best.

Go ahead and install. It is wonderful.





PS: The KDE 4 Debian packages are available in experimental. The [Debian KDE
Team](http://pkg-kde.alioth.debian.org) has done a fabulous job in bringing
them in time with great quality. Thank you.

