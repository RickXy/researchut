{
    "title": "Laptop Mode Tools 1.64",
    "date": "2013-09-01T04:16:01-04:00",
    "lastmod": "2014-11-12T03:33:44-05:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-1-64"
}

I just released **Laptop Mode Tools** @ version **1.64**. And am pleased to
introduce the new graphical utility to toggle individual power saving modules
in the package.



![](/sites/default/files/LMT_GUI.jpg)



The GUI is written using the **PyQT** Toolkit and the options in the GUI are
generated at runtime, based on the list of available power saving modules.



Apart from the GUI configuration tool, this release also includes some bug
fixes:

  * Don't touch USB Controller power settings. The individual devices, when plugged in, while on battery, inherit the power settings from the USB controller
  * start-stop-programs: add support for systemd. Thanks to Alexander Mezin
  * Replace hardcoded path to udevadm with "which udevadm". Thanks to Alexander Mezin
  * Honor .conf files only. Thanks to Sven Köhler
  * Make '/usr/lib' path configurable. This is especially useful for systems that use /usr/lib64, or /lib64 directly. Thanks to Nicolas Braud-Santoni
  * Don't call killall with the -g argument. Thanks to Murray Campbell
  * Fix RPM Spec file build errors

The Debian package will follow soon. I don't intend to introduce a new package
for the GUI tool because the source is hardly 200 lines. So the dependencies
(pyqt packages) will go as **Recommeds** or **Suggests**

