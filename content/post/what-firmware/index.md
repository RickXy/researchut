{
    "title": "What firmware",
    "date": "2015-01-12T14:04:57-05:00",
    "lastmod": "2015-01-12T14:04:57-05:00",
    "draft": "false",
    "tags": [
        "firmware",
        "battery"
    ],
    "categories": [
        "Debian-Blog"
    ],
    "url": "/blog/what-firmware"
}

Dear Lazy Web,

I have  an [HP Envy J104TS](http://www8.hp.com/us/en/ads/envy-
touchsmart-15/specs.html) laptop. Recently I saw an interesting message in the
kernel log.

[99360.969652] [Firmware Bug]: battery: (dis)charge rate invalid.

Does anybody know what firmware is it referring to here ? I don't think the
current set of firmwares shiped by linux are involved in battery related
information. Is it the BIOS ?

    
    
    [95474.561491] IPv6: ADDRCONF(NETDEV_UP): wlan0: link is not ready
    [95474.803578] r8169 0000:0f:00.0 eth0: link down
    [95474.803627] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
    [95474.933797] IPv6: ADDRCONF(NETDEV_UP): wlan0: link is not ready
    [95477.111599] wlan0: authenticate with 00:03:7f:fe:00:02
    [95477.127389] wlan0: send auth to 00:03:7f:fe:00:02 (try 1/3)
    [95477.129315] wlan0: authenticated
    [95477.131352] wlan0: associate with 00:03:7f:fe:00:02 (try 1/3)
    [95477.133605] wlan0: RX AssocResp from 00:03:7f:fe:00:02 (capab=0x411 status=0 aid=3)
    [95477.133686] wlan0: associated
    [95477.133703] IPv6: ADDRCONF(NETDEV_CHANGE): wlan0: link becomes ready
    [99125.123138] device vethNLAABF entered promiscuous mode
    [99125.123377] IPv6: ADDRCONF(NETDEV_UP): vethNLAABF: link is not ready
    [99125.188568] IPv6: ADDRCONF(NETDEV_CHANGE): vethNLAABF: link becomes ready
    [99125.188615] lxcbr0: port 1(vethNLAABF) entered forwarding state
    [99125.188631] lxcbr0: port 1(vethNLAABF) entered forwarding state
    [99140.239301] lxcbr0: port 1(vethNLAABF) entered forwarding state
    **[99360.969652] [Firmware Bug]: battery: (dis)charge rate invalid.**
    [99361.729291] NMI watchdog: enabled on all CPUs, permanently consumes one hw-PMU counter.
    [99361.864205] EXT4-fs (dm-2): re-mounted. Opts: errors=remount-ro,data=writeback,commit=0
    [99361.905210] EXT4-fs (sda6): re-mounted. Opts: data=ordered,commit=0
    [102236.267648] lxcbr0: port 1(vethNLAABF) entered disabled state
    [102236.556452] lxcbr0: port 1(vethNLAABF) entered disabled state
    [102236.557476] device vethNLAABF left promiscuous mode
    [102236.557483] lxcbr0: port 1(vethNLAABF) entered disabled state
    

