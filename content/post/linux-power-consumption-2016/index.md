{
    "title": "Linux Power Savings 2016",
    "date": "2016-03-01T10:31:45-05:00",
    "lastmod": "2016-03-01T10:31:45-05:00",
    "draft": "false",
    "tags": [
        "power saving",
        "laptop-mode-tools",
        "laptop",
        "battery"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Technology",
        "Tools"
    ],
    "url": "/blog/linux-power-consumption-2016"
}

Having moved to a new place, now at times, I also have to deal with power
outages. As heat increases, the power outages will be much longer and more
frequent. So much, that UPS and Power Inverters run out. Such are ideal times
to measure idle power consumption for my laptop.

Here's what my default (and idle) OS looks like. It should be standard to most
"typical" users. Some minor odds could be apport, dnsmasq, and maybe, tor.
Also, thanks to systemd, there are some native services which are now
converted to socket activation based ones. There's also Laptop Mode Tools for
userspace power savings. But huge thanks to Intel's work in making the kernel
more power efficient. With things like **Runtime PM** and **Intel P-State** ,
we can now define, default and aggressive power-savings modes,  helping
conserve more power.

We still have challenges with things like binary-only drivers; and also stock
GPL drivers but with an external firmware blob. There have been many instances
when my previous machines (which had both) ran into severe power drain. The
challenge with power savings is that it is not just the job of the kernel. All
components, kernel and userspace, need to be made aware of, and work in
unison. Otherwise, one ill behaving web browser tab can be the cause of power
drain. P-State driver seems to already be doing some scale down of CPU
resources. And with CGroups maturing moe I think we'll eventually have all
Desktop Environments making use of it.

    
    
    systemd-+-ModemManager-+-{gdbus}
            |              `-{gmain}
            |-NetworkManager-+-{gdbus}
            |                `-{gmain}
            |-accounts-daemon-+-{gdbus}
            |                 `-{gmain}
            |-agetty
            |-apport-notifyd
            |-atd
            |-avahi-daemon---avahi-daemon
            |-bluetoothd
            |-colord-+-{gdbus}
            |        `-{gmain}
            |-cron
            |-dbus-daemon
            |-dnsmasq
            |-evolution-+-{book-client-dbu}
            |           |-{dconf worker}
            |           |-7*[{evolution}]
            |           |-{gdbus}
            |           `-{gmain}
            |-gdm3-+-gdm-session-wor-+-gdm-x-session-+-Xorg
            |      |                 |               |-gnome-session-b-+-gnome-settings--+-{dconf worker}
            |      |                 |               |                 |                 |-{gdbus}
            |      |                 |               |                 |                 |-{gmain}
            |      |                 |               |                 |                 |-{pool}
            |      |                 |               |                 |                 `-{threaded-ml}
            |      |                 |               |                 |-gnome-shell-+-ibus-daemon-+-ibus-dconf-+-{dconf worker}
            |      |                 |               |                 |             |             |            |-{gdbus}
            |      |                 |               |                 |             |             |            `-{gmain}
            |      |                 |               |                 |             |             |-ibus-engine-sim-+-{gdbus}
            |      |                 |               |                 |             |             |                 `-{gmain}
            |      |                 |               |                 |             |             |-{gdbus}
            |      |                 |               |                 |             |             `-{gmain}
            |      |                 |               |                 |             |-{JS GC Helper}
            |      |                 |               |                 |             |-{JS Sour~ Thread}
            |      |                 |               |                 |             |-{dconf worker}
            |      |                 |               |                 |             |-{gdbus}
            |      |                 |               |                 |             |-{gmain}
            |      |                 |               |                 |             `-{threaded-ml}
            |      |                 |               |                 |-{dconf worker}
            |      |                 |               |                 |-{gdbus}
            |      |                 |               |                 `-{gmain}
            |      |                 |               |-{gdbus}
            |      |                 |               `-{gmain}
            |      |                 |-{gdbus}
            |      |                 `-{gmain}
            |      |-gdm-session-wor-+-gdm-x-session-+-Xorg
            |      |                 |               |-gnome-session-b-+-evolution-alarm-+-{cal-client-dbus}
            |      |                 |               |                 |                 |-{dconf worker}
            |      |                 |               |                 |                 |-{evolution-alarm}
            |      |                 |               |                 |                 |-{gdbus}
            |      |                 |               |                 |                 `-{gmain}
            |      |                 |               |                 |-gnome-settings--+-{dconf worker}
            |      |                 |               |                 |                 |-{gdbus}
            |      |                 |               |                 |                 |-{gmain}
            |      |                 |               |                 |                 |-{pool}
            |      |                 |               |                 |                 `-{threaded-ml}
            |      |                 |               |                 |-gnome-shell-+-2*[redshift-+-{gdbus}]
            |      |                 |               |                 |             |             `-{gmain}]
            |      |                 |               |                 |             |-{JS GC Helper}
            |      |                 |               |                 |             |-{JS Sour~ Thread}
            |      |                 |               |                 |             |-{dconf worker}
            |      |                 |               |                 |             |-{gdbus}
            |      |                 |               |                 |             |-{gmain}
            |      |                 |               |                 |             `-{threaded-ml}
            |      |                 |               |                 |-parcimonie
            |      |                 |               |                 |-parcimonie-appl-+-{gdbus}
            |      |                 |               |                 |                 `-{gmain}
            |      |                 |               |                 |-python3---libinput-debug-
            |      |                 |               |                 |-ssh-agent
            |      |                 |               |                 |-tracker-extract-+-{dconf worker}
            |      |                 |               |                 |                 |-{gdbus}
            |      |                 |               |                 |                 |-{gmain}
            |      |                 |               |                 |                 `-10*[{pool}]
            |      |                 |               |                 |-tracker-miner-a-+-{gdbus}
            |      |                 |               |                 |                 `-{gmain}
            |      |                 |               |                 |-tracker-miner-f-+-{dconf worker}
            |      |                 |               |                 |                 |-{gdbus}
            |      |                 |               |                 |                 `-{gmain}
            |      |                 |               |                 |-tracker-miner-u-+-{gdbus}
            |      |                 |               |                 |                 `-{gmain}
            |      |                 |               |                 |-{dconf worker}
            |      |                 |               |                 |-{gdbus}
            |      |                 |               |                 `-{gmain}
            |      |                 |               |-{gdbus}
            |      |                 |               `-{gmain}
            |      |                 |-{gdbus}
            |      |                 `-{gmain}
            |      |-{gdbus}
            |      `-{gmain}
            |-geoclue-+-{gdbus}
            |         `-{gmain}
            |-gnome-keyring-d-+-{gdbus}
            |                 |-{gmain}
            |                 `-{timer}
            |-gpg-agent
            |-gsd-printer-+-{gdbus}
            |             `-{gmain}
            |-ibus-daemon-+-ibus-dconf-+-{dconf worker}
            |             |            |-{gdbus}
            |             |            `-{gmain}
            |             |-ibus-engine-sim-+-{gdbus}
            |             |                 `-{gmain}
            |             |-{gdbus}
            |             `-{gmain}
            |-ibus-x11-+-{gdbus}
            |          `-{gmain}
            |-iio-sensor-prox-+-{gdbus}
            |                 `-{gmain}
            |-inetd---leafnode
            |-libvirtd---15*[{libvirtd}]
            |-lvmetad
            |-master-+-pickup
            |        |-qmgr
            |        `-tlsmgr
            |-mcelog
            |-minissdpd
            |-mount.ntfs
            |-onboard-+-{dconf worker}
            |         |-{gdbus}
            |         `-{gmain}
            |-packagekitd-+-{gdbus}
            |             `-{gmain}
            |-polkitd-+-{gdbus}
            |         `-{gmain}
            |-2*[pulseaudio-+-gconf-helper]
            |               |-{alsa-sink-ALC23}]
            |               |-{alsa-sink-HDMI }]
            |               |-{alsa-sink-pcspe}]
            |               `-{alsa-source-ALC}]
            |-rtkit-daemon---2*[{rtkit-daemon}]
            |-systemd-+-(sd-pam)
            |         |-at-spi-bus-laun-+-dbus-daemon
            |         |                 |-{dconf worker}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-at-spi2-registr-+-{gdbus}
            |         |                 `-{gmain}
            |         |-dbus-daemon
            |         |-dconf-service-+-{gdbus}
            |         |               `-{gmain}
            |         |-goa-daemon-+-{dconf worker}
            |         |            |-{gdbus}
            |         |            `-{gmain}
            |         |-goa-identity-se-+-{gdbus}
            |         |                 |-{gmain}
            |         |                 `-{pool}
            |         |-gvfs-afc-volume-+-{gdbus}
            |         |                 |-{gmain}
            |         |                 `-{gvfs-afc-volume}
            |         |-gvfs-goa-volume-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfs-gphoto2-vo-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfs-mtp-volume-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfs-udisks2-vo-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfsd-+-{gdbus}
            |         |       `-{gmain}
            |         |-gvfsd-fuse-+-{gdbus}
            |         |            |-{gmain}
            |         |            |-{gvfs-fuse-sub}
            |         |            `-2*[{gvfsd-fuse}]
            |         |-gvfsd-metadata-+-{gdbus}
            |         |                `-{gmain}
            |         `-mission-control-+-{dconf worker}
            |                           |-{gdbus}
            |                           `-{gmain}
            |-systemd-+-(sd-pam)
            |         |-at-spi-bus-laun-+-dbus-daemon
            |         |                 |-{dconf worker}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-at-spi2-registr-+-{gdbus}
            |         |                 `-{gmain}
            |         |-dbus-daemon
            |         `-gconfd-2
            |-systemd-+-(sd-pam)
            |         |-at-spi-bus-laun-+-dbus-daemon
            |         |                 |-{dconf worker}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-at-spi2-registr-+-{gdbus}
            |         |                 `-{gmain}
            |         |-dbus-daemon
            |         |-dconf-service-+-{gdbus}
            |         |               `-{gmain}
            |         |-evolution-addre-+-2*[evolution-addre-+-{dconf worker}]
            |         |                 |                    |-{evolution-addre}]
            |         |                 |                    |-{gdbus}]
            |         |                 |                    `-{gmain}]
            |         |                 |-{dconf worker}
            |         |                 |-{evolution-addre}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-evolution-calen-+-evolution-calen-+-{dconf worker}
            |         |                 |                 |-{evolution-calen}
            |         |                 |                 |-{gdbus}
            |         |                 |                 |-{gmain}
            |         |                 |                 `-6*[{pool}]
            |         |                 |-evolution-calen-+-{book-client-dbu}
            |         |                 |                 |-{dconf worker}
            |         |                 |                 |-{evolution-calen}
            |         |                 |                 |-{gdbus}
            |         |                 |                 |-{gmain}
            |         |                 |                 `-{pool}
            |         |                 |-2*[evolution-calen-+-{dconf worker}]
            |         |                 |                    |-{evolution-calen}]
            |         |                 |                    |-{gdbus}]
            |         |                 |                    `-{gmain}]
            |         |                 |-{dconf worker}
            |         |                 |-{evolution-calen}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-evolution-sourc-+-{dconf worker}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-gconfd-2
            |         |-gnome-shell-cal-+-{cal-client-dbus}
            |         |                 |-{dconf worker}
            |         |                 |-{gdbus}
            |         |                 |-{gmain}
            |         |                 `-{gnome-shell-cal}
            |         |-gnome-terminal--+-bash---pstree
            |         |                 |-bash
            |         |                 |-{dconf worker}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-goa-daemon-+-{dconf worker}
            |         |            |-{gdbus}
            |         |            `-{gmain}
            |         |-goa-identity-se-+-{gdbus}
            |         |                 |-{gmain}
            |         |                 `-{pool}
            |         |-gpaste-daemon-+-{dconf worker}
            |         |               |-{gdbus}
            |         |               `-{gmain}
            |         |-gvfs-afc-volume-+-{gdbus}
            |         |                 |-{gmain}
            |         |                 `-{gvfs-afc-volume}
            |         |-gvfs-goa-volume-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfs-gphoto2-vo-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfs-mtp-volume-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfs-udisks2-vo-+-{gdbus}
            |         |                 `-{gmain}
            |         |-gvfsd-+-{gdbus}
            |         |       `-{gmain}
            |         |-gvfsd-burn-+-{gdbus}
            |         |            `-{gmain}
            |         |-gvfsd-dnssd-+-{gdbus}
            |         |             `-{gmain}
            |         |-gvfsd-fuse-+-{gdbus}
            |         |            |-{gmain}
            |         |            |-{gvfs-fuse-sub}
            |         |            `-2*[{gvfsd-fuse}]
            |         |-gvfsd-metadata-+-{gdbus}
            |         |                `-{gmain}
            |         |-gvfsd-network-+-{dconf worker}
            |         |               |-{gdbus}
            |         |               `-{gmain}
            |         |-gvfsd-trash-+-{gdbus}
            |         |             `-{gmain}
            |         |-mission-control-+-{dconf worker}
            |         |                 |-{gdbus}
            |         |                 `-{gmain}
            |         |-nautilus-+-gedit
            |         |          |-{dconf worker}
            |         |          |-{gdbus}
            |         |          `-{gmain}
            |         `-tracker-store-+-{dconf worker}
            |                         |-{gdbus}
            |                         |-{gmain}
            |                         `-4*[{pool}]
            |-systemd-journal
            |-systemd-logind
            |-systemd-network
            |-systemd-timesyn---{sd-resolve}
            |-systemd-udevd
            |-thermald---{thermald}
            |-tor
            |-udisksd-+-{cleanup}
            |         |-{gdbus}
            |         |-{gmain}
            |         `-{probing-thread}
            |-upowerd-+-{gdbus}
            |         `-{gmain}
            `-wpa_supplicant
    
    

So I used the standard power measurement tool, powertop. Below is a screenshot
of PowerTop claiming 9 - 10 hours of possible battery backup. This is idle
state. "Idle" would mean when the user is not interacting with the machine.
When under basic usage (email, web), the actual battery backup I've sensed is
somewhere around 6-7 hrs, which is still good from what we got a couple years
ago.

There's one oddity in the screenshot though. The estimated power consumption
reported is 4.55W, where as, the power consumption of just the wifi card is
reported 6.17W. That too, when the wifi was disconnected.

This reminded me of the [bug report](https://bugs.debian.org/cgi-
bin/bugreport.cgi?bug=719860) against PowerTop, which is now more than 2 years
old. We've still not concluded in that bug report, so if anyone can shed some
light on PowerTop's reporting, please do share it in the bug report.

![](/sites/default/files/powertop-2016.png)

Note: Please ignore the terminal's title in the screenshot. That is wrong and
I've not bothered to figure out why it is displaying the title name
constructed from an outdated session, which is already terminated.

