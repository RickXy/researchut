{
    "title": "Fully SSL for my website",
    "date": "2016-07-15T06:24:09-04:00",
    "lastmod": "2016-07-15T06:24:09-04:00",
    "draft": "false",
    "tags": [
        "SSL",
        "https",
        "let's encrypt",
        "eff"
    ],
    "categories": [
        "Debian-Blog",
        "General",
        "Computing"
    ],
    "url": "/blog/lets-encrypt-researchut"
}

I finally made full switch to SSL for my website. Thanks to
[this](https://hblok.net/blog/posts/2016/02/24/lets-encrypt-tls-certificate-
setup-for-apache-on-debian-7/) simple howto on Let's Encrypt. I had to use the
upstream git repo though. The Debian packaged tool, letsencrypt.sh, did not
have enough documentation/pointers in place. And finally, thanks to the Let's
Encrypt project as a whole.

PS: http is now redirected to https. I hope nothing really breaks externally.

