{
    "title": "Laptop - Microsoft Windows - Installer - License",
    "date": "2008-12-11T16:52:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Tools",
        "Technology"
    ]
}

Most people who've bought laptops, would see a pre-installed version of
Windows, for which they'd have already paid. Recently things have changed but
still for the majority of the laptop models available, Windows is the most
commonly used option.

I own a Dell XPS M1210 which too came pre-installed with Microsoft Windows XP.
Since the machine was low on hard-drive space, I had to eventually knock-off
all Non-Linux partitions to make more space. Recently I was able to swap my
Seagate SATA Laptop Drive (160 GB) with the one that was shipped with the
Laptop. This made me rich in disk space. So, being a small PC gamer, I decided
that I do want to have a copy of Windows XP installed on my machine. And why
not, I already have paid for it and own a license.

Here's where the fun started. Over the years, my laptop's CD/DVD drive had
been mal-functioning. I seldom used it. For installing Linux, I never had
issue because most Linux Distributions support installation off the USB Stick.
But to install Windows XP using the USB Stick was a very tough time. I had to
dig up a lot of docs on the internet, most of which were very particular (to
EEEPC) and never worked. The only article that I came across, that worked, was
[this.](http://www.vandomburg.net/installing-windows-xp-from-usb/)

After having the USB Stick WIndows XP Bootable, I ran into a new problem. The
copy of Windows XP Installer didn't accept my License Key. This started a
whole new set of problems. It led me to suspect that something must be flawed
in the USB Installation method, especially that it uses the ramdisk image from
WIndows Server 2003.

Left with not much choices, I was disappointed. I wanted to install XP on my
laptop but was not able to. Last night I tried a **Desi** thing. I wiped the
CD/DVD Drive's lense with my shirt and gave the Windows XP Installer CD
another try. And Bingo, It booted off it. Sometimes Lord Mahadev just want the
**Sweet Time** be taken. Now with the Installer CD working, I hoped things
would work. But it came to the same License key problem. My license key didn't
work. I had to struggle a lot in getting it to work.

Two things here:

  * WIndows licenses for the installation media are lot based. Keys from one lot will not work with the other lot.
  * The article about [Windows XP Installation from USB Stick](http://www.vandomburg.net/installing-windows-xp-from-usb/) works. 

Overall now I'm done with the Windows XP re-incarnation on my laptop.

There's just one glitch remaining to solve. My HDD's partition layout is this:

> rrs@learner:~$ sudo fdisk -l  
>  [sudo] password for rrs:  
>  
>  Disk /dev/sda: 160.0 GB, 160041885696 bytes  
>  255 heads, 63 sectors/track, 19457 cylinders  
>  Units = cylinders of 16065 * 512 = 8225280 bytes  
>  Disk identifier: 0x1d05272a  
>  
>     Device Boot      Start         End      Blocks   Id  System  
>  /dev/sda1               1         255     2048256   83  Linux  
>  /dev/sda2   *         256        4143    31230360    7  HPFS/NTFS  
>  /dev/sda3            4144        5359     9767520   83  Linux  
>  /dev/sda4            5360       19457   113242185   8e  Linux LVM

sda4 is the LVM partiton where my root and swap partitions reside. Under
Windows XP, sda4 is listed as a " **Windows XP Fault Tolerant Partition** ".
I'm not touching it at all when in Windows, but its mere presence makes my
mood go bad. I'd rather preferred if Windows XP was completely unknown about
this partition type ( **8e** )

