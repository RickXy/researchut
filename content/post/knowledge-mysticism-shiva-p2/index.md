{
    "title": "Knowledge Of The Mysticism Of Lord Shiva – Part II",
    "date": "2016-01-11T05:21:32-05:00",
    "lastmod": "2016-01-11T05:21:58-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "shiv"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/knowledge-mysticism-shiva-p2"
}

[![lord-shiva](/sites/default/files/lord-
shiva1.jpg)](https://vibhormahajan.files.wordpress.com/2012/07/lord-
shiva1.jpg)

**Birth Of Lord Brahma**

As Lord Narayana slept in water, by the will of Lord Shiva, a lotus stalk
sprouted out of the navel of sleeping Vishnu Narayana. It gave him another
name  _' Kamalnabha'_.

The stalk continued to grow upwards like a thick column of smoke. Then, high
up at the top of the stalk a huge lotus flower blossomed. It was a magnificent
sight. Later it was learnt that, Lord Shiva produced Lord Brahma from the
lower part of his body and put him on the lotus. He had four faces but was
bereft of any knowledge about his father, his purpose, surroundings, location,
situation and aim. That divine lotus was the only thing he saw and knew about.
Hoping to find his creator at the base or root of the flower, Lord Brahma
descended into the stalk that was like a vertical tunnel from inside. For
hundreds of celestial years he kept sliding down without reaching the bottom.
Frustrated at endless travel down he started upward climbing to reach the top.
Again even after hundreds of years of ascent he could not reach the corona to
his dismay.

Then, Lord Shiva willed a prophecy for Lord Brahma's benefit. It asked him to
make a long penance. He made a hard penance for twelve years and Lord Vishnu
materialised before him, by grace of Lord Shiva. Magnificent was Lord Vishnu,
incredibly handsome, wearing a crown and a lot of ornamental things, draped in
yellow apparel, four armed, holding conch sell, chakra, lotus and mace in each
hand. Both of them were under the spell of Maya of Lord Shiva.

 **Pillar Of Luminosity**

Lord Vishnu claimed, he was Lord Brahma's father and the one who lay at the
root of the lotus stalk he manifested on. But Lord Brahma thought he had come
into existence on his own through his latent divine power as he had not found
Lord Vishnu during his exploration down. So, he would not accept his seniority
claim and superior position. They argued on and on and even quarrelled. To
bring peace Lord Shiva manifested between them in the form of luminous pillar.
The spell of Maya broke when they failed to find either end of it as they
competed to explore. Lord Vishnu went for down end and Lord Brahma for upper
end. Defeated they prayed to the pillar - _  "O divine mystery! It is beyond
our scope to understand what or who you are. Totally mystified we are, please
manifest in your true form and enlighten us. Reveal your truth."_

There was no response from the shaft of light which in fact was the Lingam
symbol of Lord Shiva. For one hundred years they prayed in many ways without
success. Then, a humming sound issued from the pillar. It felt like echoing
_' OM'_. It was followed by other sounds echoing from various sides with
formation of corresponding syllables like 'aa', 'oo', 'um'. A divine happiness
was being radiated by the luminous pillar, Lord Vishnu and Lord Brahma were
confused.

Then, a shadow of a sage figure took shape. It revealed that Maya of the Force
Supreme was at work, and it was Lord Shiva. In phonetics and syllables he was
manifesting in the cryptic forms being yet beyond mind and speech. He was the
causer of all. (AUM) manifested his syllable form that addressed a common
gender. Various sounds and syllables appearing were representatives of the
divinities to manifest, as willed by Lord Shiva. From the core echo syllable
'A-U-M' the three principal deities emerged in the form of Brahma, Vishnu and
Mahesh (Manifested Lord Shiva) to carry on the exercises of creation,
sustenance and destruction respectively. The syllable also stood for male,
female and zone aspects (aum). The hum of the echo (Nada) kept them linked
together. In other sense, Lord Shiva was the seeder, Lord Brahma seed and Lord
Vishnu seeded (the later two being male and female aspect). The seeded seed
expanded to become cosmic egg (Brahmanda). The divine egg remained in water
for many celestial years.

Thousands of years later, Lord Shiva split the egg in two parts. The upper
part became celestial world and lower half, the earth and five elements. From
that egg also emerged the four armed creator.

Upon learning the supreme divinity of the luminescent pillar as being
_Shivalingam_ , Lord Vishnu began to sing prayers of the divinity supreme.
Then, they saw a divine person with five faces, ten arms, having camphor white
complexion, glow and ornamental embellishments materialise. Extremely generous
and virile he looked. Every part of that divinity reflected a syllable and all
the syllables were representing various parts. He appeared to be the
embodiment of hymns, odes, prayers, holy mantras, spiritual wisdoms,
enlightenments, mystical knowledge's, volumes of  un-worded scriptures, Vedas,
visualisation of future epics, tenets, code books and alphabetical heap
waiting to become meaningful words. It was syllabic manifestation of deity
supreme, Lord Shiva. Lord Vishnu sang odes to the  _Shabda Parbrahm_ , the
supreme power of un-manifested words.

He, the all prevailing and all-embracing Shiva Supreme breathed the divine
knowledge of Vedas, five holiest mantras and Om into Lord Brahma and Lord
Vishnu. Pleased with there prayers, Lord Shiva materialised before them in his
deity supreme form along with his consort, Uma to bless them. They made
obeisance to them devotedly.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

