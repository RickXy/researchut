{
    "title": "Hospital && Medical && Doctors",
    "date": "2008-09-13T23:27:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "General",
        "Psyche"
    ]
}

I've always been very skeptical about Doctors. Given the kind of news we've
heard about Doctor's (one who used to take kidneys out of healthy people in
the name of treatment), it was tough believing in this profession.

But I think I've been very wrong. One example cannot represent a community. My
father has been going through serious medical treatments and I admitted him to
the Manipal Hospital at Bangalore, India. Different people have had varying
opinions about Manipal Hospital. I though it is important for me to opine what
I went through with his treatment.

I've found Manipal Hospital to be a very good hospital. The service, care, and
hospitalizaiton - all are good. But most importantly, I was very very happy
and satisfied with the doctor who treated my father, Dr. Satish R. It was Dr.
Satish's confidence that made me take all decisions for the treatment.

