{
    "title": "Citizen",
    "date": "2011-02-18T02:05:16-05:00",
    "lastmod": "2013-01-31T13:37:30-05:00",
    "draft": "false",
    "tags": [
        "Traffic Rules",
        "Good Citizenship",
        "You Suck"
    ],
    "categories": [
        "Psyche",
        "Rant"
    ],
    "url": "/blog/you-suck"
}

What is a Good Citizen?

These are my observations in an urban city of a developing country. One
unanimous comment that I often hear is **Traffic**. That that how bad it is,
no rules, pathetic roads and so on...... The government is to blame for this
unplanned growth that has been causing chaos. And then there 's no effective
system to raise, track and execute Tickets.

All these might be very facts based arguments if thrown out in the wild. What
I haven't found is what people think of good citizenship. This is a regular
nightmare. The citizens have vehicles but have no etiquette on how to make use
of it.

  * We have close to 50% people on high-beam
  * We have the wannabes with bright flashy lights (Ones those go terrible on your eyes)

Let's see the contributions

  * Many Taxis - Taxis would want to run on high beam so that they can get quick and make more bucks. Justified? At what cost?
  * Cabs - These are special taxis provided by the workplaces. They have deadlines. And they have incentives. Justified?
  * Good Citizens - This is the special crowd. The one that whines the most. Nearly 30% of the citizens are on high beams. And half of them don't understand if the other side is signalling inconvenience of the high beam. But still **Traffic** is bad here. And the roads suck. Right?

