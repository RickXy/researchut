{
    "title": "Laptop Mode Tools 1.68.1",
    "date": "2015-09-27T04:57:48-04:00",
    "lastmod": "2015-09-27T04:57:48-04:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-1_68_1"
}

I am please to announce the release of Laptop Mode Tools **1.68.1**.

The last release (1.68) was mostly about systemd integration, and so is this
release. There were a couple of bugs reported, and most of them fixed, with
this release. All downstreams are requested to upgrade.

For RPM packages for Fedora and OpenSUSE (Tumbleweed), please see the
[homepage.](http://samwel.tk/laptop_mode)

    
    
    1.68.1 - Sun Sep 27 14:00:13 IST 2015
    
        * Update details about runtime-pm in manpage
    
        * Revert "Drop out reload"
    
        * Log error more descriptively
    
        * Write to common stderr. Do not hardcode a specific one
    
        * Call lmt-udev in lmt-poll. Don't call the laptop_mode binary directly.
    
          Helps in a lot of housekeeping
    
        * Direct stderr/stdout to journal
    
        * Fix stdout descriptor
    
        * Install the new .timer and poll service
    
        * Use _sbindir for RPM



