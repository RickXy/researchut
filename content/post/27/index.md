{
    "title": "Debian Lenny - Reviews and Comparison",
    "date": "2009-02-18T03:38:00-05:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "categories": [
        "Debian-Pages"
    ]
}

Now that Lenny is released, we see a lot of reviews around the world. I think
most of the reviewers do an unfair comparison for Debian.

I followed Heise Online's review and see that they are comparing Lenny to
Ubuntu/Fedora/openSUSE.

IMO, the philosophy behind Debian Stable is very different than that of
Fedora/openSUSE/Ubuntu. Fedora/openSUSE are more focused to bring you the
latest and greatest. The same applies to Ubuntu (Excluding the LTS release).
These are releases where they are targetting users as Beta Testers. And that's
good. It brings a good final Stable Release product. But comparing Debian to
them is not really correct.

Debian Stable's Release Policy is very well defined. It'll be released when it
is ready. A lot has improved there. And also the "Codename and a half" are a
very good add-on to the existing process.

> Debian Stable should actually be compared against - RHEL/SLES/UBUNTU LTS  
>  Debian Testing should be compared against - Fedora/openSUSE/UBUNTU  
>

This is something reviewers should be educated about. And they should keep
this in mind during their reviews. Debian Stable is designed with stabiilty in
mind. The focus is more on the Server side.

