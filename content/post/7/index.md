{
    "title": "Old GPG key finally revoked",
    "date": "2010-09-03T00:34:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "gpg",
        "gnupg",
        "04f130bc"
    ],
    "categories": [
        "General"
    ]
}

Just revoked my old GPG key as I do not use it anymore.

    
    
    pub  1024D/[04F130BC](http://www.researchut.com/pks/lookup?op=get&search=0xE11862EA04F130BC) 2003-08-18 *** KEY REVOKED *** [not verified]
                                   [Ritesh Raj Sarraf <rrs@researchut.com>](http://www.researchut.com/pks/lookup?op=vindex&search=0xE11862EA04F130BC)
                                   Ritesh Raj Sarraf (Ricky) <rrs@researchut.com>
                                   Ritesh Raj Sarraf <ritesh@cyberspace.com.np>
                                   Ritesh Raj Sarraf <riteshsarraf@fastmail.fm>
                                   Ritesh Raj Sarraf <rrs_rhwlist@softhome.net>
                                   Ritesh Raj Sarraf (NetApp) <rsarraf@netapp.com>
                                   Ritesh Raj Sarraf <riteshsarraf@users.sourceforge.net>
                                   [user attribute packet]
                                   [user attribute packet]

