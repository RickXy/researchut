{
    "title": "Hindu Mythology",
    "date": "2016-01-10T11:51:15-05:00",
    "lastmod": "2016-01-10T11:52:41-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism",
        "religion"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology"
}

Hindusim is a religion that is commonly dominant in the Indian sub-continent.
Though personally not very religious, I am born in a Hindu family, and was
brought up following the Hindu religion. As a religion following human, I do
have my favorite god, **Lord Shiva - The god of death**.

With my website's this section, my desire is to document whatever I think is
true and legitimate. One of the reason is, India (where Hinduism is most
common) was invaded by many rulers, from different religions. That led to many
of the history not being preserved. We do have some history, that still
remains, in tid bits.

My attempt is to gather as much of content I can, and have it documented in
digital form. Almost all the content is authored by someone else, to whom I
try my best to attribute, tracing their sources.

As a country so big that India is, it is important we document things. Things
come and go, emperors come and go, opinions come and go, what remains stagnant
is the event. It is important that we document events so that the coming
generations get a better history to read up on.

