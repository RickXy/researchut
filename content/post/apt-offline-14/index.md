{
    "title": "apt-offline 1.4",
    "date": "2014-08-31T12:41:18-04:00",
    "lastmod": "2014-08-31T12:41:18-04:00",
    "draft": "false",
    "tags": [
        "apt-offline",
        "Offline APT Package Manager",
        "offline package manager"
    ],
    "categories": [
        "Debian-Blog",
        "Tools"
    ],
    "url": "/blog/apt-offline-14"
}

apt-offline 1.4 has been released [1]. This is a minor bug fix release. In
fact, one feature, offline bug reports (--bug-reports),  has been dropped for
now.

The Debian BTS interface seems to have changed over time and the older
debianbts.py module (that used the CGI interface) does not seem to work
anymore. The current debbugs.py module seems to have switched to the SOAP
interface.

There are a lot of changes going on personally, I just haven't had the time to
spend. If anyone would like to help, please reach out to me. We need to use
the new debbugs.py module. And it should be cross-platform.

Also, thanks to Hans-Christoph Steiner for providing the bash completion
script.

[1] https://alioth.debian.org/projects/apt-offline/

