{
    "title": "Kali Yuga (kalyug) – \"Age of Vice\"",
    "date": "2016-04-02T11:45:44-04:00",
    "lastmod": "2016-04-02T11:45:44-04:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "Yuga"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/kali-yuga-age-of-vice"
}

# Kali Yuga (kalyug) – "Age of Vice"

Posted by [VIBHOR
MAHAJAN](https://vibhormahajan.wordpress.com/author/vibhormahajan/) on [MAY 9,
2012](https://vibhormahajan.wordpress.com/2012/05/09/kali-yuga-kalyug-age-of-
vice/)

**“yada yada hi dharmasya  
glanir bhavati bharata  
abhyutthanam adharmasya  
tadatmanam srjamy aham”** _  –_  **(bhagavad Gita, Chapter-4)**

**_“Sri Krishna said:_**   _“Whenever and wherever there is a decline in
religious practice, O descendant of Bharata, and a predominant rise of
irreligion—at that time I descend Myself.”_

[![vishswarupa](/sites/default/files/Vibhor/vishswarupa.jpg)](https://vibhormahajan.files.wordpress.com/2012/05/vishswarupa.jpg)

Kali Yuga (Kalyug) that is “age of (the demon) Kali”, or “age of vice” is the
last of the four stages the world goes through as part of the cycle of yugas
described in the Indian scriptures. The other ages are:-

**1) Satya Yuga**  – In Hinduism, is the “Yuga (Age or Era) of Truth”, when
mankind is governed by gods, and every manifestation or work is close to the
purest ideal and mankind will allow intrinsic goodness to rule supreme. It is
sometimes referred to as the “Golden Age.”

**2)   Treta Yuga** – It is the second out of four yugas, or ages of mankind,
in the religion of Hinduism. The most famous events in this yuga were Lord
Vishnu’s fifth, sixth and seventh incarnations as Vamana,Parashurama and
Ramachandra respectively.

**3) Dvapara Yuga**  – It is the third out of four yugas, or ages, described
in the scriptures of Hinduism. According to the Puranas this yuga ended at the
moment when Krishna returned to his eternal abode of Vaikuntha.

  * **Kali Yuga According to Ramayana**

“In the Kali Yuga, the hot-bed of sin, men and women are all steeped in
unrighteousness and act contrary to the
[Vedas](http://hinduism.about.com/cs/vedasvedanta/index.htm)… every virtue had
been engulfed by the sins of Kali Yuga; all good books had disappeared;
impostors had promulgated a number of creeds, which they had invented out of
their own wit. The people had all fallen prey to delusion and all pious acts
had been swallowed by greed.”

  * **Kali Yuga According to Mahabharata**

“… The ordinances of the Vedas disappear gradually in every successive age…
the duties in the Kali age are entirely of another kind. It seems, therefore,
that duties have been laid down for the respective age according to the powers
of human beings in the respective ages.”

  * **Who is Kali?**

Kali is the reigning lord of Kali Yuga and archenemy of Kalki, the 10th and
final Avatar of the Hindu god Vishnu. According to the Vishnu Purana, he is a
negative manifestation of Vishnu, who along with his extended evil family,
perpetually operates as a cause of the destruction of this world.

  * **What happens after Kali Yuga?**

It is predicted that at the end of the Kali Yuga, Lord Shiva shall destroy the
universe and all the physical body would undergo a great transformation. After
such dissolution, Lord Brahma would recreate the universe and mankind will
become the ‘Beings of Truth’ once again.

