{
    "title": "Lord Vishnu – Dashavatharam (Part – VII)",
    "date": "2016-01-13T06:04:19-05:00",
    "lastmod": "2016-01-13T06:04:19-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "vishnu"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/dashavatharam-7"
}

[![BhagavaGita](/sites/default/files/bhagavagita.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/bhagavagita.jpg)



 _ **" yada yada hi dharmasya  
glanir bhavati bharata  
abhyutthanam adharmasya  
tadatmanam srjamy aham"** -_ (Bhagavad Gita, Chapter-4)

 **Sri Krishna said - **"Whenever and wherever there is a decline in religious
practice, O descendant of Bharata, and a predominant rise of irreligion--at
that time I descend Myself.

 ** _" Praritranaya Sadhunam_  
 _Vinashaya Cha Dushkritam_  
 _Dharamasansthapnaya_  
 _Sambhavami Yuge-Yuge. "_** _  -_ (Bhagavad Gita, Chapter-4)

 **Sri Krishna said  -** "In order to deliver the pious and to annihilate the
miscreants, as well as to re-establish the principles of religion, I advent
Myself millennium after millennium."

 **10)  Lord Kalki**

[![kalki-nm](/sites/default/files/kalki-
nm.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/kalki-nm.jpg)

It is foretold in Srimad Bhagavatam that Lord Kalki will appear at the
conjunction of the two Yugas, namely at the end of Kali-Yuga and the beginning
of Satya-Yuga. The age of Kali-Yuga lasts 432,000 years, out of which we have
passed only 5,000 years after the Battle of Kurukshetra and the end of the
regime of King Parikshit. Therefore at the end of this period, the incarnation
of Lord Kalki will take place.

Srimad Bhagavatam describes that Lord Kalki will appear in the home of the
most eminent brahmana of Shambhala village, the great soul Vishnuyasha. Lord
Kalki will grow up with lord Parshuram as his teacher, who will instruct Lord
Kalki to perform a long penance onto Lord Shiva to receive celestial weaponry.
Later he will mount his swift horse Devadatta ; sword in hand, he will travel
over the earth exhibiting his eight mystic opulence's and eight special
qualities of Godhead. Displaying his unequalled effulgence and riding with
great speed, he will kill by the millions those thieves who have dared dress
as kings. After all the imposter kings have been killed, the residents of the
cities and towns will feel the breezes carrying the most sacred fragrance of
the sandalwood paste and other decorations of Lord Kalki, and their minds will
thereby become transcendentally pure.

The incarnation of Lord Vishnu on Earth as Lord Kalki; the maintainer of
religion, will also mark the beginning of Satya-Yuga (The age of truth), and
human society will bring forth progeny in the mode of goodness.



Copyright (C) Vibhor Mahajan

