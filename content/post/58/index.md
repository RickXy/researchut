{
    "title": "autoEqualizer for Amarok",
    "date": "2007-11-28T18:12:00-05:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Tools",
        "KDE"
    ]
}

I recently came into the necessity of having something which could load
equalizer prests by reading the genre from the current running track.

For Amarok, I couldn't find anything available. So I filed a bug report. But
soon realised that my wishlist could be accomplished just bhy a small amount
of work.

I then looked at Amarok's scripting framework. It is amazing. It took me just
3 hrs to get my wishlist item done.

And here we have:

autoEqualizer

autoEqualizer is a small application which will auto load equalizer presets
based on genre of current running track in Amarok.

Go, get it. Try it.

<http://www.kde-apps.org/content/show.php/autoEqualizer?content=70509>

