{
    "title": "Bash Endless Loop",
    "date": "2006-05-19T22:11:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Programming"
    ]
}

I always keep forgetting where the colon (;) key should be placed in a Bash
endless loop so thought of jotting it down.

 **while true; do ls; done**

