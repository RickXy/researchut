{
    "title": "The Holy Tale Of Sati – Shiva Puran (Part-III)",
    "date": "2016-01-11T04:55:28-05:00",
    "lastmod": "2016-01-11T04:55:28-05:00",
    "draft": "false",
    "tags": [
        "hinduism",
        "religion",
        "sati"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/shiv-sati-puran-3"
}

[![Lord Shiva](/sites/default/files/lord-
shiva.jpg)](https://vibhormahajan.files.wordpress.com/2012/08/lord-shiva.jpg)

**Grudge of Daksha against Lord Shiva**

Once in an ancient age many sages and spiritual seers converged at Prayaga and
decided to perform a grand Yajna. It was attended by celestial lords, divine
sages, gods, deities, prajapati etc., besides earth based holy men. Lord Shiva
was also present. Sometime later, Daksha arrived. Everyone there rose up to
pay respect to Prajapati Daksha. But Lord Shiva ignored his entry and kept
sitting. Daksha took it deliberate affront of him. So, he declared that for
the misconduct Lord Shiva shall not be offered any oblations in yajnas and his
name won't be invoked in religious ceremonies in future. Lord Shiva's carrier
Nandi pleaded that Prajapati's action was improper since a Brahmin (Daksha)
could not put a curse of Lord Shiva. It further angered Daksha and he debarred
all Shiva Ganas (special guards) from attending functions conducted by divine
sages and seers. The ganas were told to parade themselves as weirdos having
ash smeared bodies and matted hair like their master.

In retaliation, Nandi shot back a volley of curses at Daksha. He bellowed -
"Be Brahmins condemned to poverty, living on charity or begging like
mendicants. Karma pedagogues they shall be, never attaining any enlightenment
or understanding the basic truth of the religion. So will Daksha also be, no
redeemer of soul or inspirer of spiritual upliftment. Merely pedantic he shall
be and a goat faced! Become demons many of the Brahmins will."

Lord Shiva admonished Nandi for crossing the limit and failing to use
restraint.

Most of the participants forgot the incident taking it as mere outburst of
frayed tempers that deserved no serious thought. But Daksha would not forget
and he carried the grudge against Lord Shiva. Under the spell of Maya he swore
to take revenge on Lord Shiva. For that very purpose he organised a great
yajna to which all were invited except Lord Shiva, his gana guards and known
faithfuls.

The venue was at a place close to Haridwar. Sage Dadheechi discovered that
Lord Shiva was not invited to the event. He asked Daksha reason for it because
the sage thought without Lord Shiva the yajna would be incomplete and would
not bear any fruit. Daksha explained that the absence of a weird one who lived
in cremation ground smeared in funeral ash, wore elephant pelt, heap of matted
hair on head with cannabis infected mind inside and snakes creeping over,
should not matter and no harm would come to yajna. All the celestial and Lord
Vishnu had already agreed to attend. "So, why should anyone worry over Lord
Shiva no coming?" Daksha asked. Not convinced Dadheechi walked away in a huff.

Meanwhile, when Sati heard about her father's yajna she expressed her wish to
take part in it and use the opportunity to meet her mother and sisters. Lord
Shiva advised her that going to the ceremony uninvited was improper. But Sati
said she wanted an explanation from her sire for not inviting them. Lord Shiva
sighed feeling bad about it.

 **Death of Sati**

Sati went to the yajna, but was not received with any respect. The family
ignored her. Her father spoke harsh words to her and made fun of Lord Shiva in
the presence of everyone. She noticed at the yajna venue that there was no
portion of oblation set aside in the name of Lord Shiva. She wanted to know
the reason. Daksha sarcastically remarked that oblations were meant for gods
or deities and not for Lord Shiva who was more like an ogre.

Sati flew in a fury and cursed all the impost ring deities present there and
the shameless sages who were conducting that yajna of no merit.

She thundered, "What yajna is this where the Deity Supreme is being
overlooked?"

Daksha countered her by using more invectives against Lord Shiva.

Engulfed in anguish and anger at Daksha's blasphemy against her Lord, Sati
could take it no more. She took some water in her hand and invoked Maheshwara
before jumping into the leaping flame of holy fire pit of yajna to the horror
of everyone there. The gods and deities trembled in fear of impending
calamity.

[![sati-commits-suicide](/sites/default/files/sati-commits-
suicide.jpg)](https://vibhormahajan.files.wordpress.com/2012/08/sati-commits-
suicide.jpg)

The body guards of (Shiva ganas) of Sati went berserk. Many killed themselves
in  grief. Many of them created mayhem around killing everyone in sight or in
their way. Sage Brig performed impromptu Havana and created some divine
warriors to take on maddened Shiva guards. The two groups clashed. Surviving
ganas fled to Kailasha to tell their Master about the ghastly happening. The
gods, deities and sages sought protection of Lord Vishnu but he was himself
confused and in fear of the wrath of Lord Shiva.

Then, a divine prophecy was heard which asked Lord Vishnu to leave that place
and leave Daksha to his fate, whose face would be burnt and his followers done
to death.

 **Manifestation of Veerbhadra**

Meanwhile, Narada had revealed the sequence of events that had taken place at
yajna venue. Then, the beaten away ganas arrived there. In furious mood Lord
Shiva pulled a plait off his matted hair nest and began to whip the mountain
with it. From the mystic rite a terrifying male guard called Veerbhadra and a
female avenger named Mahakali emerged. The male guard bowed to his master and
asked for a command. Lord Shiva told him to destroy the yajna of Daksha.

[![Veerabhadra](/sites/default/files/veerabhadra.jpg)](https://vibhormahajan.files.wordpress.com/2012/08/veerabhadra.jpg)

Like a raging storm Veerbhadra hit the venue of the yajna of Daksha. He went
on a rampage and killing spree there. Lord Vishnu told him that he was there
merely to guard the yajna as his divine duty but Lord Shiva's will overruled
everything else. He asked Veerbhadra to provide him with some screen to enable
him to get out of that place in a flash to his Vaikuntha domain. Veerbhadra
covered him with arrows and Lord Vishnu fled. In his rampage, Veerbhadra
beheaded Daksha, who was later given a goat head by Lord Shiva.

[![Virabhadra](/sites/default/files/virabhadra.jpg)](https://vibhormahajan.files.wordpress.com/2012/08/virabhadra.jpg)

After wreaking havoc at yajna venue the guard went back to Kailasha to his
Master. As a reward for carrying out his command dutifully, Lord Shiva
appointed him as chief of his guards.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

