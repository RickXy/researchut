{
    "title": "Laptop Mode Tools 1.65",
    "date": "2014-06-18T10:39:35-04:00",
    "lastmod": "2014-11-12T03:03:45-05:00",
    "draft": "false",
    "tags": [
        "laptop-mode-tools"
    ],
    "categories": [
        "Debian-Blog",
        "Technology",
        "Tools"
    ],
    "url": "/blog/laptop-mode-tools-1-65"
}

I am very pleased to announce the release of **Laptop Mode Tools** , at
version **1.65**

This release took a toll given things have been changing for me, both
personally and professionally. **1.64** was released on September 1st, 2013.
So it was a full 9 month period, of which a good 2-3 months were
procrastination. That said, this release has some pretty good bug fixes and I
urge all distribution packagers to push it to their repositories soon. While I
'd thank all contributors who have helped make this release, a special thank
you to **Stefan Huber**. Stefan found/fixed many issues, did the messy code
clean up etc.. Thank you.

Worthy changes are mentioned below. For full details, please refer to the git
commit logs.

1.65 - Wed Jun 18 19:22:35 IST 2014  
    * fix grep error on missing $device/uevent  
    * ethernet: replace sysfs/enabled by 'ip link down'  
    * wireless-iwl-power: sysfs attr enbable -> enabled  
    * wireless-iwl-power: Add iwlwifi support  
    * Use Runtime Power Managemet Framework is more robust now. _**Deprecates module  
      usb-autosuspend**_  
    * Fix multiple hibernate issue  
    * When resuming, run LMT in force initialization mode  
    * Add module for Intel PState driver  
    * GUI: Implement suspend/hibernate interface

![](/sites/default/files/LMT_CONF_TOOL.jpg)  


