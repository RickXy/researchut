{
    "title": "OpenXenManager",
    "date": "2011-10-19T08:08:46-04:00",
    "lastmod": "2014-11-12T06:06:02-05:00",
    "draft": "false",
    "tags": [
        "xen",
        "openxenmanager",
        "openxencenter"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/open-xen-manager"
}

OpenXenManager cleared the NEW queue and is now available in the archive.



What it is:

rrs@champaran:~$ apt-cache show openxenmanager Package: openxenmanagerVersion:
0.r80+dfsg-1Installed-Size: 3051Maintainer: Ritesh Raj Sarraf
<rrs@debian.org>Architecture: allDepends: pythonDescription: **full-featured
graphical management tool for xen using xenapi**  OpenXenManager is a
graphical interface to manage XenServer / Xen Cloud Platform (XCP) hosts
through the network . OpenXenManager is an open-source multiplatform clone of
XenCenter (Citrix)Homepage:
http://sourceforge.net/projects/openxenmanager/Section: adminPriority:
extraFilename: pool/main/o/openxenmanager/openxenmanager_0.r80+dfsg-
1_all.debSize: 396322MD5sum: 64df482dac6b99bc3403bcc0740f8718SHA1:
9c746f524c3c29eb88de7ae702c96cbca162f577SHA256:
454641eb26c57ba35ad573a3a8493ac6dbd82f7920c8b926049ef892724147e0



It is written purely in Python so should work at most of the places. The
current consumer of OpenXenManager are users who have a Citrix XenServer
platfrom running as a hypervisor. Soon, once XCP efforts are completed for
Debian, OpenXenManager will also be able to manage Debian Xen Hypervisor
installations using this graphical tool.



![OpenXenManager](/sites/default/files/OpenXenManager.png)

