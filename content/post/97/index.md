{
    "title": "Pythonic Addiction",
    "date": "2006-04-15T06:47:00-04:00",
    "lastmod": "2011-01-22T13:44:11-05:00",
    "draft": "false",
    "categories": [
        "Fun",
        "Programming"
    ]
}

#!/usr/bin/env python  
  
def files(root):  
    for path, folders, files in os.walk(root):  
        for file in files:  
            yield path, file  
  
  
def find_match(repository): # aka walk_tree_copy()  
    for path, file in files(repository):  
        if file.endswith ('html') or file.endswith ('htm') or file.endswith ('HTML') or file.endswith ('HTM'):  
        #if file.endswith ('html.gz') or file.endswith ('htm.gz') or file.endswith ('HTML.gz') or file.endswith ('HTM.gz'):  
            try:  
                os.environ['__TEMP__VAL'] = file  
                os.chdir(path) # We need to chdir so that gzip can see the file in the cwd  
                os.system('gzip $__TEMP__VAL')  
                sys.stdout.write("%s/%s has been gzipped\n" % (path, file))  
            except IOError:  
                sys.stdout.write("Aieeeee.... I got some error with %s!\n\n" % (file))  
            continue  
            #return True  
    return False  
  
  
def main():  
    REPOSITORY = raw_input("Please enter a path to look for the files to zip.\nHit Return Key if you want the default path i.e. \"/home/rrs/My_Documents/My Books\"")  
  
    if REPOSITORY == '':  
        REPOSITORY = "/home/rrs/My_Documents/My Books/"  
  
    find_match(REPOSITORY)  
  
if __name__ == '__main__':  
    import os, sys, shutil  
    main() 

