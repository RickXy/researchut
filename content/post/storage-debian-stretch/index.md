{
    "title": "SAN Updates for Debian Stretch",
    "date": "2016-11-24T03:51:18-05:00",
    "lastmod": "2016-11-24T07:52:30-05:00",
    "draft": "false",
    "tags": [
        "SAN",
        "Storage",
        "iscsi",
        "multipath",
        "scsi"
    ],
    "categories": [
        "Debian-Blog",
        "Computing",
        "Tools"
    ],
    "url": "/blog/storage-debian-stretch"
}

Now that we prepare for the next Debian Stable release (Stretch), it is time
to provide some updates on what the current state of some of the (storage
related) packages in Debian is. This is not an update on the complete list of
packages related to storage, but it does cover some of them.



_**REMOVALS**_

  * **iscsitarget**  \- The iscsitarget stood as a great SCSI target for the Linux kernel. It seems to have had a good user base not just in Linux but also with VMWare users. But this storage target was always out-of-tree. With LIO having gotten merged as the default in-kernel SCSI Target, development on iscsitarget seems to have stalled. In Debian, for Stretch, there will be no iscsitarget. The package is already removed from Debian Testing and Debian Unstable, and nobody has volunteered to take over it.
  * **system-storage-manager** \- This tool intended to be a simple unified storage tool, through which one could work with various storage technologies like LVM, BTRFS, cryptsetup, SCSI etc. But the upstream development hasn't really been much lately. For Debian Stable, it shouldn't be part of it, given it has some [bugs](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=845517).
  * **libstoragemgmt** \- libstoragemgmt is a universal storage client-side library to talk to remote Storage Arrays. The project is active upstream. For Debian, the package is out-of-date and, now, also needs a maintainer. Unless someone picks up this package, it will not be part of Debian Stretch.



_**UPDATES**_

  * **open-iscsi** \- This is the default iSCSI Initiator for Linux distributions. After a long slow development, upstream recently did a new release. This new release accomplished an important milestone; Hardware Offloading  for QLogic cards. A special thanks to Frank Fegert, who helped with many aspects of the new **iscsiuio** package.  And thanks to Christian Seiler, who is now co-maintaining the package, it is in great shape. We have fixed some long outstanding bugs and open-iscsi now has much much better integration with the whole system. For Jessie too, we have the up-to-date open-iscsi pacakges (including the new iscsiuio package, with iSCSI Offload) [available](https://packages.debian.org/source/jessie-backports/open-iscsi) through jessie-packports
  * **open-isns** \- iSNS is the Naming Service for Storage. This is a new package in Debian Stretch. For users on Debian Jessie, Christian's efforts have made the  open-isns package [available](https://packages.debian.org/source/jessie-backports/open-isns) in jessie-backports too.
  * **multipath-tools** \- After years of slow development, multipath-tools too saw some active development this year, thanks to Xose and Christophe. The Debian version is up-to-date with the latest upstream release. For Debian Stretch, multipath-tools should have good integration with systemd.
  * **sg3-utils** \- sg3 provides simple tools to query, using SCSI commands. The package is up-to-date and in good shape for Debian  Stretch.
  * **LIO Target** \- This is going to be the big entry for Debian  Stretch. LIO is the in-kernel SCSI Target for Linux. For various reasons, we did not have LIO in Jessie. For Stretch, thanks to Christian Seiler and Christophe Vu-Brugier, we now have the well maintained -fb fork into Debian, which will replace the initial packages from the pre-fork upstream. The -fb fork is maintained by Andy Grover, and now, seems to have users from many other distributions and the kernel community. And given that LIO -fb branch is also part of the RHEL product family, we hope to see a well maintained project and an active upstream. The older packages: targetcli, python-rtslib and python-configshell shall be removed from the archive soon.



Debian users and **derivatives** , using these storage tools, may want to
test/report now. Because once Stretch is released, getting new fixes in may
not be easy enough. So please, if you have reliance on these tools, please
test and report bugs, now.

