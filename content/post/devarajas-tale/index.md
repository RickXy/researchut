{
    "title": "Devaraja’s Tale – Shiva Puran",
    "date": "2016-01-13T05:43:21-05:00",
    "lastmod": "2016-01-13T05:43:21-05:00",
    "draft": "false",
    "tags": [
        "hindu",
        "hinduism",
        "religion",
        "shiv"
    ],
    "categories": [
        "General"
    ],
    "url": "/hindu-mythology/devarajas-tale"
}

[![user5204_pic3357_1304588591](/sites/default/files/user5204_pic3357_1304588591.jpg)](https://vibhormahajan.files.wordpress.com/2012/06/user5204_pic3357_1304588591.jpg)

Long time ago, there lived a Brahmin named Devaraja in town of Kirantanagara.
A very lax, unprincipled and degenerate life he lived. No bathing and no
praying was the order of his day. He was lecherous by nature. It appeared that
the sole object of his life was to make money and spend it on vice games to
satisfy his carnal desires. He would not mind cheating others and creating
misunderstandings between friends and relatives.

One day, he went to the pond to wash himself for a change and happened to see
there a woman of easy virtue called Shobhavati. Devaraja could not resist the
coquettish gestures of the evil woman. She took away all the money he had by
and by. He completely ignored the pleadings of his parents and the wife. To be
free to do whatever he liked, he went to live with the evil woman in her
house. When he had blown away all his money, the woman kicked him out of her
house. She had no use of him any more.

The Brahmin roamed around and reached a place called Pratishthanapura. He fell
ill there. He thought he was going to die. In a nearby Lord Shiva temple he
took shelter. On the floor he lay unable to move. There was nothing he could
do but listen to the recitation of the Shiva Puran and religious sermons being
delivered to faithful's. The sermons and recitations concluded on the day he
finally died.

The agents of the lord of death, Yama duly arrived there to take charge of the
soul of Devraja to take it to hell of after world. But the servitor agents of
Lord Shiva Intervened to stop them. The agents of death revealed the long list
of sins the dad Brahmin Devaraja had committed during his life time. The
agents of Lord Shiva reasoned that what the dead man had done or how he lived
no longer mattered, since the last few days of his life were spent in hearing
the recitations of holy Shiva Puran which had cleansed him of all his sins.
They took the soul of Devaraja to the Kailasha domain of Lord Shiva.

To his puzzled agents, Lord Yama explained, when they expressed their surprise
over incident, that great was the glory of Lord Shiva and great was his grace
bestowed upon those who listened to the Kathas of Shiva Puran.

 **" HAR HAR MAHADEV"**



 **Copyright (C) Vibhor Mahajan**

