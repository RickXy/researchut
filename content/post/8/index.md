{
    "title": "The Automatic Equalizer for Android",
    "date": "2010-09-01T04:13:00-04:00",
    "lastmod": "2011-01-22T13:44:10-05:00",
    "draft": "false",
    "tags": [
        "amarok",
        "autoEqualizer"
    ],
    "categories": [
        "Tools",
        "KDE"
    ]
}

When I wrote the [autoEqualizer ](http://kde-
apps.org/content/show.php/autoEqualizer?content=70509 "autoEqualizer")plug-in
for the [Amarok](http://amarok.kde.org) (1.x) media player, to the best of my
knowledge, there weren't any media players with this feature nor were there
any plug-ins.

Recently, I came to know that the **Samsung Galaxy S** 's media player looks
to be having the Automatic Equalizer functionality. Not sure if this player is
specific to Samsung Galaxy S or the Android Platform in general.

![](http://www.researchut.com/blog/images/galaxy-equalizer.png)

![](http://www.researchut.com/blog/images/galaxy-equalizer1.png)

