---
title: "{{ replace .Name "-" " " | title }}"
author: "Ritesh Raj Sarraf"
date: {{ .Date }}
lastmod:
draft: true
subtitle: ""
image: ""
tags: ["Linux", "Containers", "systemd", "systemd-nspawn", "Qemu", "Virtualization", "Docker"]
categories: ["Debian-Blog", "Computing", "Tools"]
url: "/blog/Article_Name_Title"
---


## Heading

### Sub Heading


This is a **Bold Text**
This is a ***Semi Bold Text***
THis is an *Italic Text*
This is a ~~Strike Through Text~~

// Here's how you add an image
// YOu upload the image to /static/iamges/....

{{< figure src="/images/virt-profile-libvirt.png" alt="virt board profile under libvirt" title="virt board profile under libvirt" >}}



// Very good highlighter
{{< highlight linux >}}

rrs@priyasi:~$ machinectl list-images
NAME                 TYPE      RO USAGE CREATED                     MODIFIED
2019                 subvolume no   n/a Mon 2019-06-10 09:18:26 IST n/a     

{{< /highlight >}}



// Programming language based highlighting
{{< highlight html >}}
<section id="main">
  <div>
    <ul>
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
    </ul>

    <ol>
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
    </ol>
  </div>
</section>
{{< /highlight >}}


// Programming language based highlighting with lineno
{{< highlight html "linenos=table,hl_lines=2 5-7,linenostart=1">}}
<section id="main">
  <div>
    <ul>
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
    </ul>

    <ol>
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
    </ol>
  </div>
</section>
{{< /highlight >}}


// You can also use Code Fences type of highlighter
```go-html-template
<section id="main">
  <div>
    <ul>
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
    </ul>

    <ol>
	<li>Coffee</li>
	<li>Tea</li>
	<li>Milk</li>
    </ol>
  </div>
</section>
```
